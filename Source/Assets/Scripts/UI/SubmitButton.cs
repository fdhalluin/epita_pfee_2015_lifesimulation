﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;
using System.Text;
using System.IO;

public class SubmitButton : MonoBehaviour {

	public Text nicknameText;

	public void Submit() {
		string nickName = nicknameText.text;
		if (nickName == "")
			return;
		uint score = MoneyManager.instance.GetMoney();

		string content = string.Format ("{{\"score\": {0}, \"nickname\": \"{1}\"}}", score, nickName);
		byte[] encoded = Encoding.UTF8.GetBytes (content);
		WebRequest wr = WebRequest.Create ("http://leo-ercolanelli.com/leaderboard/new");
		wr.ContentType = "application/json";
		wr.ContentLength = encoded.Length;
		wr.Method = "POST";
		Stream stream = wr.GetRequestStream ();
		stream.Write (encoded, 0, encoded.Length);
		stream.Close ();

		WebResponse response = wr.GetResponse ();
		stream = response.GetResponseStream ();
		StreamReader sr = new StreamReader (stream);
		string r = sr.ReadToEnd();
		print(r);
		nicknameText.text = "";
		sr.Close ();
		stream.Close ();
		response.Close ();

	}
}
