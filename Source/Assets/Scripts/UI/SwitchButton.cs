﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class SwitchButton : MonoBehaviour
{
    private static readonly Color InactiveColor = new Color(0x28 / 255f, 0x28 / 255f, 0x28 / 255f);
    private static readonly Color ActiveColor = new Color(0x4e / 255f, 0x4a / 255f, 0x43 / 255f);

    public SwitchButton[] SwitchButtons { get; set; }
    public GameObject Panel { get; set; }

    private Image _image;

    public void Awake()
    {
        _image = gameObject.GetComponent<Image>();
    }

    public void Inactivate()
    {
        _image.color = InactiveColor;
        Panel.SetActive(false);
    }

    public void Activate()
    {
        foreach (var sb in SwitchButtons.Where(sb => sb != this))
           sb.Inactivate();

        _image.color = ActiveColor;
        Panel.SetActive(true);
    }
}