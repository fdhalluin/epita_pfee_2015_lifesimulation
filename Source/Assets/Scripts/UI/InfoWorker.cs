﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InfoWorker : MonoBehaviour
{
	public Text name;
	public Text speed;
	public Text plastics;
	public Text electronics;
	public Text packaging;
    public Text efficiency;
	
	private Agent _agent;
	
	private void Update()
	{
        float[] occupations  = _agent.occupation();
		float[] tasksDone = _agent.tasksDone();

		Dictionary<TaskType, float> wTasks = Counter.tasksCount;

		name.text = "Name: " + _agent.Name;

		speed.text = "Speed: " + _agent.movingSpeedAttribute.ToString()
			+ " (" + tasksDone[0].ToString() + " / " + wTasks[TaskType.Delivery].ToString() + " : " + (occupations[0] * 100).ToString("0.00") + "% of its time)";

		plastics.text = "Plastics: " + _agent.plastics.ToString()
			+ " (" + tasksDone[2].ToString() + " / " + wTasks[TaskType.Plastic].ToString() + " : " + (occupations[2] * 100).ToString("0.00") + "% of its time)";

		electronics.text = "Electronics: " + _agent.electronics.ToString()
			+ " (" + tasksDone[1].ToString() + " / " + wTasks[TaskType.Electronic].ToString() + " : " + (occupations[1] * 100).ToString("0.00") + "% of its time)";

		packaging.text = "Packaging: " + _agent.packaging.ToString()
			+ " (" + tasksDone[3].ToString() + " / " + wTasks[TaskType.Packaging].ToString() + " : " + (occupations[3] * 100).ToString("0.00") + "% of its time)";

        efficiency.text = "Efficiency: "
            + (_agent.efficiency() * 100).ToString("0.00") + "%";
	}
	
	public void DisplayInfo(Agent a)
	{
		_agent = a;
	}
}
