﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	public GameObject BottomPanel;
	public GameObject SidePanel;
	public GameObject TasksPanel;

	public Camera MenuCamera;
	public Camera MainCamera;

	public GameObject MenuPanel;
	public GameObject AboutPanel;

	public GameObject terrain;

	public Map map;

	private void ActivateUI()
	{
		BottomPanel.SetActive(true);
		SidePanel.SetActive(true);
		TasksPanel.SetActive(true);

		MenuCamera.gameObject.SetActive(false);
		MainCamera.gameObject.SetActive(true);
	}

	public void Normal()
	{
		ActivateUI();
		map.InitMap(false);
		this.gameObject.SetActive(false);
	}

	public void Hard()
	{
		ActivateUI();
		map.InitMap(true);
		this.gameObject.SetActive(false);
	}

	public void ToggleTerrain()
	{
		bool active = terrain.gameObject.activeSelf;
		terrain.gameObject.SetActive(!active);
	}

	public void ToggleAbout()
	{
		AboutPanel.SetActive(! AboutPanel.activeSelf);
	}
}
