﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoPackage : MonoBehaviour
{
    public Text type;
	public Text plasticStock;
	public Text electronicStock;
    public Text craftedStock;
	public Text efficiency;

    private Workshop _workshop;

    public void Update()
    {
        type.text = "Type: Packaging";
		plasticStock.text = "Plastic stock: " + _workshop.PlasticComponents().ToString();
		electronicStock.text = "Electronic stock: " + _workshop.ElectronicComponents().ToString();
        craftedStock.text = "Crafted stock: " + _workshop.CraftedItems().ToString();
		efficiency.text = "Efficiency: " 
			+ (_workshop.efficiency() * 100).ToString("0.00") + "%";
    }

    public void DisplayInfo(Workshop ws)
    {
        _workshop = ws;
    }
}