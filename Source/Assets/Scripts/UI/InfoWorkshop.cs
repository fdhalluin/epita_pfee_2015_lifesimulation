﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class InfoWorkshop : MonoBehaviour
{
    public Text type;
	public Text componentsStock;
    public Text craftedStock;
    public Text efficiency;

    private Workshop _workshop;

    private void Update()
    {
        type.text = "Type: " + Enum.GetName(typeof(EType), _workshop.type);
		componentsStock.text = "Components stock: " + _workshop.BasicComponents().ToString();
        craftedStock.text = "Crafted stock: " + _workshop.CraftedItems().ToString();
        efficiency.text = "Efficiency: " 
            + (_workshop.efficiency() * 100).ToString("0.00") + "%";
    }

    public void DisplayInfo(Workshop ws)
    {
        _workshop = ws;
    }
}
