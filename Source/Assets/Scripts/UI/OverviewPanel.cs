﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OverviewPanel : MonoBehaviour
{
	public Text scoreText;
	public ProdOverview[] prods;
	
	void Update()
	{
		scoreText.text = "Final score: " + MoneyManager.instance.GetMoney() + " (Base: " + MoneyManager.instance.SavedMoney +
			" + Profit: " + ((int)MoneyManager.instance.GetMoney() - (int)MoneyManager.instance.SavedMoney).ToString() + ")";

		for (int i = 0; i < prods.Length; ++i)
		{
			prods[i].OperationText.text = Recipe.recipies[i].producted + " x " + Recipe.recipies[i].money + " $";
			prods[i].TotalText.text = (Recipe.recipies[i].producted * Recipe.recipies[i].money).ToString() + " $";
		}
	}
}
