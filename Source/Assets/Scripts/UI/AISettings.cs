﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AISettings : MonoBehaviour {

	public GameObject fullRandomButton;
	public GameObject taskButton;
	public GameObject solverButton;
	public GameObject anticipationButton;
	public Text fullRandomText;
	public Text taskText;
	public Text solverText;
	public Text anticipationText;
	public GameObject wcAssignmentSlider;
	public GameObject wcAssignLeftText;
	public GameObject wcAssignRightText;
	public Text IconText;

	public GameObject panel;

	public Color buttonColor;

	// Private
	private int selectedAI = 3;
	private int selectedWcAssign = 1;
	private bool panelToggle = true;

	private List<Image> AIImages;
	private List<Text> AIText;
	private List<Text> WCAssignText;

	// Use this for initialization
	void Start () {
		AIImages = new List<Image>();
		AIText = new List<Text>();
		WCAssignText = new List<Text>();

		panelToggle = !panel.activeSelf;

		AIImages.Add(fullRandomButton.GetComponent<Image>());
		AIImages.Add(taskButton.GetComponent<Image>());
		AIImages.Add(solverButton.GetComponent<Image>());
		AIImages.Add(anticipationButton.GetComponent<Image>());

		AIText.Add(fullRandomText);
		AIText.Add(taskText);
		AIText.Add(solverText);
		AIText.Add(anticipationText);

		WCAssignText.Add(wcAssignLeftText.GetComponent<Text>());
		WCAssignText.Add(wcAssignRightText.GetComponent<Text>());

		shiftColor();
	}
	
	public void ToggleClick()
	{
        SoundManager.instance.PlayClip(SoundEffect.click);
		panel.SetActive(panelToggle);
		panelToggle = !panelToggle;
	}

	public void FullRandomClick()
	{
        SoundManager.instance.PlayClip(SoundEffect.click);
		IconText.text = "AI Settings :\nRandom";
		selectedAI = 0;
		TaskManager.instance.IAT = IAType.Rando;
		shiftColor();
	}
	public void TaskClick()
	{
        SoundManager.instance.PlayClip(SoundEffect.click);
		IconText.text = "AI Settings :\nTasks";
		selectedAI = 1;
		TaskManager.instance.IAT = IAType.Basic;
		shiftColor();
	}
	public void SolverClick()
	{
        SoundManager.instance.PlayClip(SoundEffect.click);
		IconText.text = "AI Settings :\nSolver";
		selectedAI = 2;
		TaskManager.instance.IAT = IAType.Advanced;
		TaskManager.instance.solverAnticipation = false;
		shiftColor();
	}
	public void AnticipationClick()
	{
        SoundManager.instance.PlayClip(SoundEffect.click);
		IconText.text = "AI Settings :\nAnticipate";
		selectedAI = 3;
		TaskManager.instance.IAT = IAType.Advanced;
		TaskManager.instance.solverAnticipation = true;
		shiftColor();
	}

	public void WcAssignmentSwitch()
	{
        SoundManager.instance.PlayClip(SoundEffect.click);
		Slider s = wcAssignmentSlider.GetComponent<Slider>();
		selectedWcAssign = (int)s.value;

		switch (selectedWcAssign)
		{
		case 0:
			print ("Random Workshop assignment method");
			TaskManager.instance.workshopAssignementSelection = false;
			break;
		case 1:
			print ("Advanced Workshop assignment method");
			TaskManager.instance.workshopAssignementSelection = true;
			break;
		}

		shiftColor();
	}

	private void shiftColor()
	{
		foreach (Image im in AIImages)
			im.color = Color.white;
		foreach (Text im in AIText)
			im.color = Color.white;
		foreach (Text im in WCAssignText)
			im.color = Color.white;

		AIImages[selectedAI].color = buttonColor;
		AIText[selectedAI].color = buttonColor;
		WCAssignText[selectedWcAssign].color = buttonColor;
	}
}