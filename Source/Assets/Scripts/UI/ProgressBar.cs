﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
	public Image filler;
	public ParticleSystem fx;

	void Start()
	{
		filler.fillAmount = 0;
	}

	void FixedUpdate()
	{
		if (filler.fillAmount >= 1)
		{
			fx.Play();
			gameObject.SetActive(false);
			ResetFillAmount();
		}
	}

	public void SetFillAmount(float amount)
	{
		if (amount > 1)
			amount = 1;
		filler.fillAmount = amount;
	}

	public void ResetFillAmount()
	{
		filler.fillAmount = 0;
	}
}
