﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class EmployeeDesc : MonoBehaviour
{

	public Agent Agent { get; private set; }
	private Text nameText;
	private Text statsText;

	public void UpdateValues()
	{
		nameText.text = Agent.Name;
		statsText.text = String.Format(
			"{0}  {1,2}  {2,2}  {3,2}",
			Agent.movingSpeedAttribute,
			Agent.plastics,
			Agent.electronics,
			Agent.packaging
		);
	}

	public void Setup(Agent a)
	{
		Text[] texts = GetComponentsInChildren<Text>();
		nameText = texts[0];
		statsText = texts[1];

		Agent = a;
		UpdateValues();
	}

    public void OnClick()
    {
        UIManager.instance.DisplayAgentInfo(this.Agent);
        this.Agent.Select();
    }
}
