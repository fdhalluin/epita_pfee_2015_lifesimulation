﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class SyncTask : MonoBehaviour {

	public int current;

	private Text current_text;

	public void Start()
	{
		current = TaskManager.instance.ConcurrentTasks;

		if (current < 1)
			current = 1;

		current_text = GetComponentInChildren<Text>();
	    update_depends();
	}

	public void Inc()
	{
		++current;
		TaskManager.instance.IncreaseConcurrentTasks();
	    update_depends();
	}

	public void Dec()
	{
		if (current > 1)
		{
			--current;
			TaskManager.instance.DecreaseConcurrentTasks();
			update_depends();
		}
	}

	void update_depends()
	{
		current_text.text = current.ToString();
	}
}
