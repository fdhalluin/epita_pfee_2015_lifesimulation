﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	// Singleton
	private static UIManager _instance;
	public static UIManager instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<UIManager>();
			return _instance;
		}
	}

	public Text InfoPlaceholder;
	public InfoWorkshop WorkshopInfo;
	public InfoPackage PackageInfo;
	public InfoWorker WorkerInfo;
	public RectTransform BottomPanel;
	public RectTransform TaskPanel;
	public int PanelTransitionSpeed = 10;
	public Text BottomPanelText;
	public Text TaskPanelText;
	public GameObject LaunchButton;
	public GameObject ButtonsPanel;
	public GameObject AISettings;
	public OverviewPanel Overview;

	private bool constructionMode = false;
	private bool initialConstruction = false;
	private bool taskViewMode = false;
	private int targetYForBotPanel = -150;
	private int targetXForTaskPanel = 0;
	private Workshop selectedWorkshop = null;
	private Agent selectedAgent = null;

	public void ToggleConstructionMode()
	{
		SetConstructionMode(! constructionMode);
	}

	public void SetConstructionMode(bool b)
	{
		if (initialConstruction)
			return;

		LaunchButton.SetActive(false);
		AISettings.SetActive(false);
		ButtonsPanel.SetActive(true);
		constructionMode = b;

		if (constructionMode)
		{
			BottomPanelText.text = "Construction Mode. [SPACE] to resume production";
			targetYForBotPanel = 0;
			TimeManager.instance.Pause();
		}
		else
		{
			BottomPanelText.text = "Press [SPACE] to edit the factory";
			targetYForBotPanel = -150;
			TimeManager.instance.Play();
		}

        // Volume is lowered when in construction mode
        SoundManager.instance.SetDim(constructionMode);
	}

	public void SetInitialConstruction(bool b)
	{
		initialConstruction = b;
		if (initialConstruction)
		{
			BottomPanelText.text = "Construction Mode. Press LAUNCH when you are ready to begin production";
			TimeManager.instance.Pause();
			LaunchButton.SetActive(true);
			ButtonsPanel.SetActive(false);
			AISettings.SetActive(true);
		}
		else
		{
			SetConstructionMode(false);
		}
	}

	public void ShowOverviewPanel()
	{
		SetConstructionMode(false);
		Overview.gameObject.SetActive(true);
	}

	public void HideOverviewPanel()
	{
		Overview.gameObject.SetActive(false);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
			ToggleConstructionMode();
		else if (Input.GetKeyDown(KeyCode.T) || (Input.GetKeyDown(KeyCode.Tab)))
			ToggleTaskView();

		if (BottomPanel.position.y != targetYForBotPanel)
		{
			if (BottomPanel.position.y < targetYForBotPanel)
				BottomPanel.position = new Vector3(BottomPanel.position.x, BottomPanel.position.y + PanelTransitionSpeed, BottomPanel.position.z);
			else
				BottomPanel.position = new Vector3(BottomPanel.position.x, BottomPanel.position.y - PanelTransitionSpeed, BottomPanel.position.z);
		}

		if (TaskPanel.position.x != targetXForTaskPanel)
		{
			float dir = TaskPanel.position.x < targetXForTaskPanel ? 1 : -1;
			float x = TaskPanel.position.x + dir * PanelTransitionSpeed * 1.5f;
			float clampedX = Mathf.Clamp(x, 0, 380);

			TaskPanel.position = new Vector3(clampedX, TaskPanel.position.y, TaskPanel.position.z);
		}
	}

	public bool ConstructionMode()
	{
		return constructionMode;
	}

	public void DisplayAgentInfo(Agent a)
	{
		if (selectedWorkshop)
			selectedWorkshop.Unselect();
		if (selectedAgent)
			selectedAgent.Unselect();

		selectedWorkshop = null;
		selectedAgent = a;
		InfoPlaceholder.gameObject.SetActive(false);
		WorkshopInfo.gameObject.SetActive(false);
		PackageInfo.gameObject.SetActive(false);
		WorkerInfo.gameObject.SetActive(true);
		WorkerInfo.DisplayInfo(a);
	}

    public void DisplayInfo(Workshop w)
    {
		if (selectedWorkshop)
			selectedWorkshop.Unselect();
		if (selectedAgent)
			selectedAgent.Unselect();

		selectedAgent = null;
		selectedWorkshop = w;
        switch (w.type)
        {
           case EType.Electronic:
           case EType.Plastic:
				InfoPlaceholder.gameObject.SetActive(false);
                PackageInfo.gameObject.SetActive(false);
                WorkshopInfo.gameObject.SetActive(true);
				WorkerInfo.gameObject.SetActive(false);
                WorkshopInfo.DisplayInfo(w);
                break;

           case EType.Packaging:
				InfoPlaceholder.gameObject.SetActive(false);
                WorkshopInfo.gameObject.SetActive(false);
                PackageInfo.gameObject.SetActive(true);
				WorkerInfo.gameObject.SetActive(false);
                PackageInfo.DisplayInfo(w);
                break;
        }
    }

	public void HideInfo()
	{
		InfoPlaceholder.gameObject.SetActive(true);
		WorkshopInfo.gameObject.SetActive(false);
		PackageInfo.gameObject.SetActive(false);
		WorkerInfo.gameObject.SetActive(false);
	}

	public void OnWorkshopDestroyed(Workshop ws)
	{
		if (ws == selectedWorkshop)
		{
			HideInfo();
			selectedWorkshop = null;
		}
	}

	public void OnAgentDestroyed(Agent a)
	{
		if (a == selectedAgent)
		{
			HideInfo();
			selectedAgent = null;
		}
	}

	private void setTaskViewMode(bool b)
	{
		taskViewMode = b;

		if (taskViewMode)
		{
			TaskPanelText.text = "Press [T] to hide active tasks";
			targetXForTaskPanel = 380;
		}
		else
		{
			TaskPanelText.text = "Press [T] to show active tasks";
			targetXForTaskPanel = 0;
		}
	}
	private void ToggleTaskView()
	{
		setTaskViewMode(!taskViewMode);
	}

	public void Launch()
	{
		TaskManager.instance.InitBasicTasks();
		SetInitialConstruction(false);
	}

	public bool InitialConstructionMode()
	{
		return initialConstruction;
	}
}
