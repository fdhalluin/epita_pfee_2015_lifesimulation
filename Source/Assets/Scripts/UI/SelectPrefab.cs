﻿using System;
using UnityEngine;
using System.Collections;

public class SelectPrefab : MonoBehaviour
{
    public int Type { get; set; }
    private Map _map;

    private void Start()
    {
        _map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
    }

    public void OnClick()
    {
        _map.SetSelectedType(Type);
    }
}
