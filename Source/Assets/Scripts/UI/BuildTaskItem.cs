using System;
using UnityEngine;
using UnityEngine.UI;

public class BuildTaskItem : TaskItem
{
    private Text finishText;
    private Text workshopText;

    public BuildTaskItem(Task task, GameObject taskItem)
    {
        this.task = task;
		since = Time.time;
        go = taskItem;

        Text[] texts = taskItem.GetComponentsInChildren<Text>();
        ageText      = texts[1];
        ownerText    = texts[2];
        finishText   = texts[3];
        workshopText = texts[4];
    }

    public override void UpdateFields(float now)
    {
        BuildTask task = this.task as BuildTask;

        if (task.GetWorker() != null)
        {
            int finish = (int)(task.GetWorker().progressBar.filler.fillAmount * 100);
            finishText.text = string.Format("Completion: {0}%", finish);
        }
        else
            finishText.text = "Completion: 0%";

        workshopText.text = string.Format(
            "Workshop: ({0},{1})",
            task.GetArrival().X,
            task.GetArrival().Y
        );

        base.UpdateFields(now);
    }
}
