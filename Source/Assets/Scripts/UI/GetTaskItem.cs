using System;
using UnityEngine;
using UnityEngine.UI;

public class GetTaskItem : TaskItem
{
    private Text fromText;
    private Text toText;

    public GetTaskItem(Task task, GameObject taskItem)
    {
        this.task = task;
        since = Time.time;
        go = taskItem;

        Text[] texts = taskItem.GetComponentsInChildren<Text>();
        ageText   = texts[1];
        ownerText = texts[2];
        fromText  = texts[3];
        toText    = texts[4];
    }

    public override void UpdateFields(float now)
    {
        GetTask task = this.task as GetTask;

        fromText.text = string.Format(
            "From: ({0},{1})",
            task.GetDepartTile().X,
            task.GetDepartTile().Y
        );

        toText.text = string.Format(
            "To: ({0},{1})",
            task.GetArrival().X,
            task.GetArrival().Y
        );

        base.UpdateFields(now);
    }
}
