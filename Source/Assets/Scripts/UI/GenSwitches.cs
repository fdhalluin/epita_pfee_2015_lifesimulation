﻿using System;
using System.Xml;

using UnityEngine;
using UnityEngine.UI;

public class GenSwitches : MonoBehaviour
{
    public RectTransform FatherTransform;
    public Button SwitchPrefab;
    public GameObject PrefabGroup;
    public GameObject Prefab;

    private const string Conf = @"
<categories>
  <category name=""Layout"">
    <prefab name=""Destroy        (5$ for wall)"" type=""0"" price=""Destroy""></prefab>
    <prefab name=""Wall"" type=""1"" price=""Wall""></prefab>
  </category>
  <category name=""Workshops"">
    <prefab name=""Plastics"" type=""3"" price=""Plastic""></prefab>
    <prefab name=""Electronics"" type=""4"" price=""Elec""></prefab>
    <prefab name=""Packaging"" type=""5"" price=""Pack""></prefab>
  </category>
  <category name=""Personnel"">
    <prefab name=""Mixed Agent"" type=""2"" price=""Agent""></prefab>
	<prefab name=""Transport Specialist"" type=""8"" price=""SpeedSpecialist""></prefab>
	<prefab name=""Plastic Specialist"" type=""9"" price=""PlasticSpecialist""></prefab>
	<prefab name=""Elec Specialist"" type=""10"" price=""ElecSpecialist""></prefab>
	<prefab name=""Packaging Specialist"" type=""11"" price=""PackagingSpecialist""></prefab>
  </category>
</categories>
    ";

    private void Start()
    {
        var categories = GetCategories();
        foreach (XmlNode category in categories)
            GenSwitch(category);

        var children = gameObject.GetComponentsInChildren<SwitchButton>();
        foreach (var child in children)
            child.SwitchButtons = children;
        children[0].Activate();
    }

    private void GenSwitch(XmlNode category)
    {
        var sw = (Button) Instantiate(SwitchPrefab);
        sw.GetComponentInChildren<Text>().text = category.Attributes["name"].Value;
        sw.GetComponent<RectTransform>().SetParent(transform, false);
        sw.GetComponent<SwitchButton>().Panel = GenPanel(category);
    }

    private static XmlNodeList GetCategories()
    {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(Conf);
        return xmlDoc.SelectNodes("//category");
    }

    private GameObject GenPanel(XmlNode category)
    {
        var group = (GameObject) Instantiate(PrefabGroup);
        group.GetComponent<RectTransform>().SetParent(FatherTransform, false);

        var prefabs = category.SelectNodes("./prefab");
        foreach (XmlNode prefab in prefabs)
        {
            GameObject go = (GameObject)Instantiate(Prefab);

			string name = prefab.Attributes["name"].Value;
			Text[] texts = go.GetComponentsInChildren<Text>();

			foreach (Text t in texts)
			{
				if (t.name == "Name")
					t.text = name;
				else
				{
					t.text =  MoneyManager.instance.StringToPrice(prefab.Attributes["price"].Value).ToString() + " $";
				}
			}

            int type = int.Parse(prefab.Attributes["type"].Value);
            go.GetComponentInChildren<SelectPrefab>().Type = type;

            Texture2D texture = TextureStore.Instance.Textures[type];
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            go.GetComponentInChildren<Image>().sprite = sprite;

            go.GetComponent<RectTransform>().SetParent(group.transform, false);
        }

        return group;
    }
}
