﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class EmployeesView : MonoBehaviour
{
	public GameObject descPrefab;

	private LinkedList<EmployeeDesc> descs;
	private static EmployeesView instance;

	public void Awake()
	{
		descs = new LinkedList<EmployeeDesc>();
		instance = this;
	}

	public static EmployeesView GetInstance()
	{
		return instance;
	}

	public void RegisterAgent(Agent a)
	{
		var descGO = Instantiate(descPrefab) as GameObject;
		descGO.GetComponent<RectTransform>().SetParent(transform, false);

		var desc = descGO.GetComponent<EmployeeDesc>();
        desc.Setup(a);

		descs.AddLast(desc);
	}

	public void UnregisterAgent(Agent a)
	{
		var n = descs.First;
		for (; n != null && n.Value.Agent != a; n = n.Next)
			continue;

		if (n != null)
		{
			DestroyImmediate(n.Value.gameObject);
			descs.Remove(n);
		}
	}
}
