﻿using System.Collections.Generic;
using System;

public class Counter
{
	public static int workersTasks { get; private set; }
    public static Dictionary<TaskType, float> tasksCount { get; private set; }
	public static Dictionary<EType, int> workshopsTasks { get; private set; }
	
	public static void incrementWorkers(TaskType type)
	{
		workersTasks += 1;
        tasksCount[type] += 1;
	}
	
	public static void incrementWorkshops(EType type)
	{
		workshopsTasks[type] += 1;
	}
	
	static Counter()
	{
		workersTasks = 0;
		workshopsTasks = new Dictionary<EType, int>();
        tasksCount = new Dictionary<TaskType, float>();
		
		foreach (EType type in Enum.GetValues(typeof(EType)))
			workshopsTasks[type] = 0;

        foreach (TaskType type in Enum.GetValues(typeof(TaskType)))
            tasksCount[type] = 0;
	}
}
