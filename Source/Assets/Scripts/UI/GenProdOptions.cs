﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Attached to the Production choice menu, generates the buttons
/// with the different recipies
/// </summary>
public class GenProdOptions : MonoBehaviour {

	public GameObject OptionPrefab;

	void Start () {
		foreach (Recipe r in Recipe.recipies)
		{
			GameObject o = Instantiate(OptionPrefab);
			o.transform.SetParent(transform);

			Image i = o.GetComponent<Image>();
			Texture2D t = TextureStore.Instance.Textures[r.textureId]; 
			i.sprite = Sprite.Create(t, new Rect(0,0,t.width,t.height)
			                          , new Vector2(0.5f, 0.5f));
			Text text = o.GetComponentInChildren<Text>();
			text.text = r.name + "\n(" + r.money + " $)";

			BuildOption s = o.GetComponent<BuildOption>();
			s.currentProdMenu = transform.parent.parent.gameObject;
			s.currentProd = transform.parent.parent.parent.gameObject;
			s.attachedRecipe = r;
		}
	}
}
