﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProdOverview : MonoBehaviour
{
	public Text OperationText;
	public Text TotalText;

	public void SetText(string operation, string total)
	{
		OperationText.text = operation;
		TotalText.text = total;
	}
}
