﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WidthFitter : MonoBehaviour
{
	private RectTransform thisRect;
	private RectTransform parentRect;

    private void Awake()
	{
		thisRect = transform as RectTransform;
		parentRect = transform.parent as RectTransform;
	}

	private void Update()
	{
		float width = parentRect.rect.height;
		float height = thisRect.rect.height;	

		if (width != thisRect.rect.width)
			thisRect.sizeDelta = new Vector2(width, height);
	}
}
