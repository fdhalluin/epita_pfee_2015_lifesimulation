﻿using UnityEngine;
using System.Collections;

public class BuildOption : MonoBehaviour {

	// The sidepanel.
	// For access to the setcurrentRecipe method
	public GameObject currentProd;

	// The menubar for hiding
	public GameObject currentProdMenu;

	// The recipe attached to the button
	public Recipe attachedRecipe;

	// When a recipe is chosen, set it and close menu
	public void OnClick()
	{
		CurrentProd s = currentProd.GetComponent<CurrentProd>();
		s.Current = attachedRecipe;
		currentProdMenu.SetActive(false);
	}

}
