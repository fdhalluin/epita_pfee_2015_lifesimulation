using UnityEngine;

public class TextureStore : MonoBehaviour
{
    public Texture2D[] Textures;
    public static TextureStore Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
}
