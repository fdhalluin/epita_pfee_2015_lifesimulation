﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class CurrentProd : MonoBehaviour {

	public Button button;
	public Text NameText;
	public Text PlasticText;
	public Text MetalText;

	private Recipe current = Recipe.recipies[0];

	public Recipe Current
	{
		get {return current;}
		set {current = value; DisplayCurrent();}
	}

	// Use this for initialization
	void Start () {
		if (current != null)
			DisplayCurrent();
	}
	
	public void DisplayCurrent()
	{
		NameText.text = current.name + " (" + current.money + " $)";
		PlasticText.text = current.plastic + " x Plastic";
		MetalText.text = current.electronics + " x Electronics";

		Image button_image = button.GetComponent<Image>();
		Texture2D t = TextureStore.Instance.Textures[current.textureId];
		button_image.sprite = Sprite.Create(t, 
		                                    new Rect(0, 0, t.width, t.height),
		                                    new Vector2(0.5f, 0.5f));
	}
}
