﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TaskItem
{
    public GameObject go { get; protected set; }
    public Task task { get; protected set; }
    public Text ageText { get; protected set; }
    public Text ownerText { get; protected set; }
    public float since { get; protected set; }

    public virtual void UpdateFields(float now)
    {
        float age = now - since;
        ageText.text = string.Format("Age: {0}s", (int)age);

        if (task.GetWorker() != null)
            ownerText.text = string.Format("Owner: {0}", task.GetWorker().Name);
        else
            ownerText.text = "Owner: NA";
    }
}
