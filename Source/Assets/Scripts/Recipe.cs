using System;
using System.Collections.Generic;

// Container class for a production goal
public class Recipe
{
	public string name = "None";
	public int plastic = 0;
	public int electronics = 0;
	public uint money = 0;
	public uint producted = 0;

	// Id of texture in TextureStore
	public int textureId = 0;

	public Recipe(string name, int textureId, int plastic, int electronics, uint money)
	{
		this.name = name;
		this.textureId = textureId;
		this.plastic = plastic;
		this.electronics = electronics;
		this.money = money;
	}

	/// <summary>
	/// Static list with all possible build options
	/// </summary>
	public static List<Recipe> recipies = new List<Recipe>
	{
		new Recipe("Phone",    	        16, 2, 2, 60),
		new Recipe("Plastic Duck",      7, 3, 0, 40),
		new Recipe("Drill",     	    14, 6, 4, 120),
		new Recipe("Drone",    	        13, 10, 8, 210),
		new Recipe("Motherboard",       12, 1, 18, 230),
		new Recipe("Toy", 		        15, 8, 3, 130)
	};
}
