﻿using System.Collections.Generic;
using UnityEngine;

public enum IAType
{
	Rando,
	Basic,
	Advanced,
}

public class TaskManager: MonoBehaviour
{
    public GameObject buildTaskPrefab;
    public GameObject getTaskPrefab;

    public RectTransform tasksView;
    private Dictionary<Task, TaskItem> taskItems;
	public Map map;
	public IAType IAT;
	public bool workshopAssignementSelection;
	public bool solverAnticipation;

	public CurrentProd currentProd;
	// Number of products built at the same time
	public int ConcurrentTasks = 2;

	private bool basicTasksInitialized;

	private List<Task> activeTasks;
	private List<GetTask> primalTasks;
	private Dictionary<Workshop, uint> workshopNbOfActiveTasks;
	private bool productionStopped;


	// Singleton
	private static TaskManager _instance;
	public static TaskManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<TaskManager>();
				_instance.currentProd = GameObject.FindObjectOfType<CurrentProd>();
			}
				
			return _instance;
		}
	}

	void Awake()
	{
        taskItems = new Dictionary<Task, TaskItem>();
		activeTasks = new List<Task> ();
		primalTasks = new List<GetTask> ();
		workshopNbOfActiveTasks = new Dictionary<Workshop, uint> ();
		productionStopped = false;
		IAT = IAType.Advanced;
		workshopAssignementSelection = true;
		solverAnticipation = true;
	}

	void Start()
	{
		basicTasksInitialized = false;
	}

	public void InitBasicTasks()
	{
		foreach (Workshop w in map.getWorkshops())
		{
			workshopNbOfActiveTasks[w] = 0;
		}

		// Add ConcurrentTasks building tasks in the begining
		if (IAT != IAType.Rando)
		{
			for (int i = 0; i < ConcurrentTasks; i++)
				primalTasks.Add(GetMeComponent (EComponent.Packaging, map.GetFinishTile (), null));
		}
		
		basicTasksInitialized = true;
		
		if (IAT == IAType.Advanced)
			UpdateWithAI();
	}

	public void ReinitializeManager()
	{
		workshopNbOfActiveTasks = new Dictionary<Workshop, uint> ();
		foreach (Workshop w in map.getWorkshops())
		{
			workshopNbOfActiveTasks[w] = 0;
		}

		while (activeTasks.Count > 0)
			RemoveActiveTask(activeTasks[activeTasks.Count - 1]);

		DestroyAllTasks ();
		activeTasks.Clear();

		taskItems.Clear();

		basicTasksInitialized = false;

		for (int i = 0; i < Recipe.recipies.Count; ++i)
		{
			Recipe.recipies[i].producted = 0;
		}
	}

	public GetTask GetMeComponent(EComponent component, Tile arrival, Task t)
	{
		GetTask task = null;
		
		if (component == EComponent.Packaging)
		{
			Workshop w = AssignWorkshopToTask(component, arrival);
			task = new GetTask(arrival, w.GetComponent<Tile>(), t, EComponent.Packaging, true, currentProd.Current);
		}
		else if (component == EComponent.Basic)
			task = new GetTask(arrival, map.GetDeliveryTile(), t, EComponent.Basic);
		else if (component == EComponent.Plastic)
		{
			//UnityEngine.MonoBehaviour.print("I WANT PLASTIC");
			Workshop w = AssignWorkshopToTask(component, arrival);
			task = new GetTask (arrival, w.GetComponent<Tile> (), t, EComponent.Plastic);
		}
		else if (component == EComponent.Electronic)
		{
			//UnityEngine.MonoBehaviour.print("I WANT ELECTRONIC");
			Workshop w = AssignWorkshopToTask(component, arrival);
			task = new GetTask (arrival, w.GetComponent<Tile> (), t, EComponent.Electronic);
		}

		return task;
	}
	
	public Agent AssignWorker(Tile goal1, Tile goal2, EGoal typeOfWork)
	{
		Agent worker = null;
		if (map.getWorkers ().Count != 0 && map.GetNumberOfOccupiedWorkers() < map.getWorkers ().Count)
		{
			foreach (Agent a in map.getWorkers())
			{
				if (!a.Occupied())
				{
					worker = a;
					worker.SetOccupation(true);
					map.IncreaseNumberOfOccupiedWorkers();

					if (typeOfWork == EGoal.Build)
					{
						Workshop w = goal1.GetComponent<Workshop>();
						Tile pos = w.GetClosestAdjacentTile(worker.transform.position);
						if (pos == null)
						{
							Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w);
							return null;
						}
						worker.typeOfWork = EGoal.Build;
						worker.Move(pos);
					}
					else
					{
						worker.typeOfWork = EGoal.Carrying;
						
						if (goal1.type == ESelection.DeliveryZone)
						{
							Workshop w2 = goal2.GetComponent<Workshop>();
							Tile pos = w2.GetClosestAdjacentTile(worker.transform.position);
							if (pos == null)
							{
								Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w2);
								return null;
							}
							worker.DoubleMove(goal1, pos);
						}
						else if (goal2.type == ESelection.FinishZone)
						{
							Workshop w1 = goal1.GetComponent<Workshop>();
							Tile pos = w1.GetClosestAdjacentTile(worker.transform.position);
							if (pos == null)
							{
								Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w1);
								return null;
							}
							worker.DoubleMove(pos, goal2);
						}
						else
						{
							Workshop w1 = goal1.GetComponent<Workshop>();
							Tile pos1 = w1.GetClosestAdjacentTile(worker.transform.position);
							if (pos1 == null)
							{
								Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w1);
								return null;
							}
							Workshop w2 = goal2.GetComponent<Workshop>();
							Tile pos2 = w2.GetClosestAdjacentTile(worker.transform.position);
							if (pos2 == null)
							{
								Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w2);
								return null;
							}
							
							worker.DoubleMove(pos1, pos2);
						}
					}
					break;
				}
			}
		}
		return worker;
	}

	public void AssignWorkerToTask(Agent worker, Task t)
	{
		if (worker == null)
		{
  			Debug.LogError("[TaskManager] AssignWorkerToTask : assigning null worker to task : " + t);
			return;
		}

		if (worker.GetAssignedTask() == t)
			return;
		

		if (!worker.Occupied ())
		{
			worker.SetOccupation (true);
			map.IncreaseNumberOfOccupiedWorkers ();
		}
		

		EGoal typeOfWork;

		if (t is GetTask)
			typeOfWork = EGoal.Carrying;
		else
			typeOfWork = EGoal.Build;

		worker.AssignTask(t);
		t.SetWorker(worker);

		if (typeOfWork == EGoal.Build)
		{
			BuildTask bt = (BuildTask)t;
			Workshop w = bt.GetArrival().GetComponent<Workshop>();
			Tile pos = w.GetClosestAdjacentTile(worker.transform.position);
			if (pos == null)
			{
				Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w);
				return;
			}
			worker.typeOfWork = EGoal.Build;

			if (pos.X == worker.X && pos.Y == worker.Y && bt.getTasksList().Count == 0)
			{
				worker.SetProgressBarVisibility(true);
				w.SetAgentWorking(worker);
			}
			else
				worker.Move(pos);
		}
		else
		{
			worker.typeOfWork = EGoal.Carrying;

			GetTask gt = (GetTask)t;

			if (gt.GetDepartTile().type == ESelection.DeliveryZone)
			{
				Workshop w2 = gt.GetArrival().GetComponent<Workshop>();
				Tile pos = w2.GetClosestAdjacentTile(worker.transform.position);
				if (pos == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w2);
					return;
				}
				worker.DoubleMove(gt.GetDepartTile(), pos);
			}
			else if (gt.GetArrival().type == ESelection.FinishZone)
			{
				Workshop w1 = gt.GetDepartTile().GetComponent<Workshop>();
				Tile pos = w1.GetClosestAdjacentTile(worker.transform.position);
				if (pos == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w1);
					return;
				}
				worker.DoubleMove(pos, gt.GetArrival());
			}
			else
			{
				Workshop w1 = gt.GetDepartTile().GetComponent<Workshop>();
				Tile pos1 = w1.GetClosestAdjacentTile(worker.transform.position);
				if (pos1 == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w1);
					return;
				}
				Workshop w2 = gt.GetArrival().GetComponent<Workshop>();
				Tile pos2 = w2.GetClosestAdjacentTile(worker.transform.position);
				if (pos2 == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w2);
					return;
				}
				worker.DoubleMove(pos1, pos2);
			}
		}
	}

	private double distBetweenTiles(Workshop w1, Tile t2)
	{
		Tile t1 = w1.GetComponent<Tile>();
		double t1x = (double) t1.X;
		double t2x = (double) t2.X;
		double t1y = (double) t1.Y;
		double t2y = (double) t2.Y;

		double x1 = t2x - t1x;
		double y1 = t2y - t1y;
		return System.Math.Sqrt (x1 * x1 + y1 * y1 ) ;
	}

	public Workshop AssignWorkshopToTask(EComponent component, Tile arrival)
	{
		Workshop w = null;
		if (workshopAssignementSelection)
		{
			double mindist = 1000;
			uint numberofTasks = 1000;

			//workshopActiveTasks;
			foreach (Workshop workshop in map.getWorkshops())
			{
				if ((workshop.type == EType.Packaging && component == EComponent.Packaging) ||
				    (workshop.type == EType.Plastic && component == EComponent.Plastic) ||
				    (workshop.type == EType.Electronic && component == EComponent.Electronic)) {

					if (workshop.adjacentTiles.Count != 0 &&
					    (workshopNbOfActiveTasks [workshop] < numberofTasks || (workshopNbOfActiveTasks [workshop] == numberofTasks && distBetweenTiles (workshop, arrival) < mindist))) {
						w = workshop;
						mindist = distBetweenTiles (workshop, arrival);
						numberofTasks = workshopNbOfActiveTasks [workshop];
					}
				}
			}
			++workshopNbOfActiveTasks [w];
		}
		else
		{
			EType ty = EType.Plastic;

			if (component == EComponent.Electronic)
				ty = EType.Electronic;
			else if (component == EComponent.Packaging)
				ty = EType.Packaging;

			bool moving = false;
			if (map.getWorkshopsNumber() == 0)
				moving = true;
			while (!moving)
			{
				int res = Random.Range (0, map.getWorkshopsNumber ());

				Workshop wo = map.getWorkshops() [res];

				if (wo.type == ty && wo.adjacentTiles.Count != 0)
				{
					workshopNbOfActiveTasks [wo] += 1;
					w = wo;
					moving = true;
				}
			}
		}
		return w;
	}
	
	public bool ReAssignWorker(Agent agent, Tile tile)
	{
		if (agent.Goal.type == ESelection.FinishZone)
			return true;

		EGoal typeOfWork = agent.typeOfWork;
		
		if (typeOfWork == EGoal.Build || agent.Goal2 == null)
		{
			Workshop w = agent.GetAssignedTask().GetArrival().GetComponent<Workshop>();

			if (w.adjacentTiles.Count != 0)
			{
				Tile pos = w.GetClosestAdjacentTile(agent.transform.position);
				if (pos == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w);
					return false;
				}
				agent.Move(pos);
				return true;
			}
			return false;
		}
		else if (agent.Goal.type == ESelection.DeliveryZone)
		{
			Workshop w2 = agent.GetAssignedTask().GetArrival().GetComponent<Workshop>();
			if (w2.adjacentTiles.Count != 0)
			{
				Tile pos = w2.GetClosestAdjacentTile(agent.transform.position);
				if (pos == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w2);
					return false;
				}
				agent.DoubleMove(agent.Goal, pos);
				return true;
			}
			return false;

		}
		else if (agent.Goal2.type == ESelection.FinishZone)
		{
			GetTask gt = (GetTask) agent.GetAssignedTask();
			Workshop w1 = gt.GetDepartTile().GetComponent<Workshop>();
			if (w1.adjacentTiles.Count != 0)
			{
				Tile pos = w1.GetClosestAdjacentTile(agent.transform.position);
				if (pos == null)
				{
					Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w1);
					return false;
				}
				agent.DoubleMove(pos, agent.Goal2);
				return true;
			}
			return false;
		}
		else
		{
			GetTask gt = (GetTask) agent.GetAssignedTask();

			Workshop w1 = gt.GetDepartTile().GetComponent<Workshop>();
			Tile pos1 = w1.GetClosestAdjacentTile(agent.transform.position);
			if (pos1 == null)
			{
				Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w1);
				return false;
			}
			Workshop w2 = gt.GetArrival().GetComponent<Workshop>();
			Tile pos2 = w2.GetClosestAdjacentTile(agent.transform.position);
			if (pos2 == null)
			{
				Debug.LogError("[TaskManager] : No adjacent tile for ws :" + w2);
				return false;
			}
			
			if (w1.adjacentTiles.Count != 0 && w2.adjacentTiles.Count != 0)
			{
				agent.DoubleMove(pos1, pos2);
				return true;
			}
		}
		//TELL WORKER TO MOVE

		return false;
	}

	public void Ping(Agent agent)
	{
		agent.SetCarrying(false);
		agent.typeOfWork = EGoal.Nothing;
		agent.SetOccupation(false);
		map.DecreaseNumberOfOccupiedWorkers();

		Task t = agent.GetAssignedTask ();
		agent.AssignTask(null);

		if (t is GetTask && ((GetTask) t).GetDepartTile() != null && ((GetTask) t).GetDepartTile().GetComponent<Workshop> () != null)
		{
			Tile dep = ((GetTask) t).GetDepartTile();

			DecreaseWorkshopNbOfActiveTasks ((Workshop)dep.GetComponent<Workshop> ());

			dep.GetTasks().Remove (t);
			foreach(Tile tile in ((Workshop) dep.GetComponent<Workshop> ()).adjacentTiles)
			{
				tile.GetTasks().Remove(t);
			}
		}

        RemoveActiveTask(t);

		t.TaskDone ();
		t.SetWorker (null);
		if (t.GetArrival().GetComponent<Workshop> () != null)
		{
			Tile arr = t.GetArrival();
			foreach(Tile tile in ((Workshop) arr.GetComponent<Workshop> ()).adjacentTiles)
			{
				tile.GetTasks().Remove(t);
			}
		}

		if (t.GetFather () != null)
		{
			if (t.GetFather () is BuildTask)
			{
				BuildTask father = (BuildTask)t.GetFather ();

				GetTask gtask = (GetTask)t;
				father.getTasksList ().Remove (gtask);
				
				if (father.getTasksList ().Count == 0)
					AddActiveTask (father);
				gtask.DestroyTask ();
			}
			else
			{
				GetTask gt = (GetTask)t.GetFather ();
				gt.AddWaitingTask(null);
				AddActiveTask (gt);
				((BuildTask)t).DestroyTask ();
			}
		}

		if (IAT == IAType.Advanced && basicTasksInitialized)
			UpdateWithAI();
	}

	void FixedUpdate()
	{
        UpdateItemsDisplay();

		if (IAT == IAType.Advanced)
		{
			// UpdateWithAI should be called when a new task becomes active (or when we add an agent)
		}
		else if (IAT == IAType.Basic)
		{
			UpdateNoAI();
		}
	}

	public void UpdateWithAI()
	{
		// Assign workers depending on the solver
		Solver solver = new Solver(map.getWorkers(), activeTasks);

		if (! solver.IsValid())
			return;

		Dictionary<Agent, Task> solution = solver.RunSolver();
		// Solution contains pairs : <Worker, Task>

		if (! solver.IsValid())
			return;

		foreach (Agent a in map.getWorkers())
		{
			if (a.Carrying() || (solution.ContainsKey(a) && solution[a] == a.GetAssignedTask()))
				continue;
			a.typeOfWork = EGoal.Nothing;
			if (a.Occupied())
			{
				a.SetProgressBarVisibility(false);
				map.DecreaseNumberOfOccupiedWorkers();
			}
			
			Task t = a.GetAssignedTask ();
			if (t != null)
			{
				if (t is BuildTask)
				{
					Workshop w = t.GetArrival().GetComponent<Workshop>();
					w.SetAgentWorking(null);
				}
				t.SetWorker (null);
			}
			a.SetOccupation(false);
			a.AssignTask(null);
			a.Goal = null;
			a.Goal2 = null;
			a.GetNavMeshAgent ().ResetPath ();
		}

		foreach (var p in solution)
		{
			if (p.Key.GetAssignedTask () == p.Value)
				continue;
			AssignWorkerToTask(p.Key, p.Value);
		}
	}

	private void UpdateNoAI()
	{
		if (productionStopped || !basicTasksInitialized)
			return;
		foreach (Task task in activeTasks)
		{
			if (map.GetNumberOfOccupiedWorkers() >= map.getWorkers ().Count)
				return;
			if (task.GetWorker() == null)
			{
				Agent worker = null;
				if (task is BuildTask)
				{
					BuildTask castTask = (BuildTask) task;
					Workshop w = castTask.GetArrival().GetComponent<Workshop>();
					if (w.ReadyForWork(castTask) && w.AgentWorking() == null)
						worker = AssignWorker(castTask.GetArrival(), null, EGoal.Build);
				}
				else
				{
					GetTask castTask2 = (GetTask) task;
					worker = AssignWorker(castTask2.GetDepartTile(), castTask2.GetArrival(), EGoal.Carrying);
				}
				
				if (worker != null)
				{
					worker.AssignTask(task);
					task.SetWorker(worker);
				}
			}
		}
		if (map.GetNumberOfOccupiedWorkers() >= map.getWorkers().Count)
			return;
		foreach (Agent agent in map.getWorkers())
		{
			if (!agent.Occupied())
				agent.Move(map.GetDeliveryTile());
		}
	}

	public List<Task> getActiveTasks()
	{
		return activeTasks;
	}

	public void SetMap(Map m)
	{
		map = m;
	}

    private void UpdateItemsDisplay()
    {
		float now = Time.time;

        foreach (TaskItem ti in taskItems.Values)
            ti.UpdateFields(now);
    }

    public void RemoveActiveTask(Task t)
    {
        CountTask(t);

		try
		{
        	TaskItem ti = taskItems[t];
			taskItems.Remove(t);
        	Destroy(ti.go);

			// If we have finished a production task, add a new one
			if (t is GetTask && ((GetTask)t).IsProductionTask () && !productionStopped)
			{
				primalTasks.Remove((GetTask) t);

				if (ShouldAddNewTask())
					primalTasks.Add(GetMeComponent(EComponent.Packaging, map.GetFinishTile(), null));
			}

			activeTasks.Remove (t);
		}
		catch (KeyNotFoundException e)
		{
			Debug.LogError(e.Message);
		}
    }

    public void AddActiveTask(Task t)
    {
        TaskItem ti = null;
        GameObject go = null;

        if (t is BuildTask)
        {
            go = Instantiate(buildTaskPrefab) as GameObject;
            ti = new BuildTaskItem(t, go);
        }
        else
        {
            go = Instantiate(getTaskPrefab) as GameObject;
            ti = new GetTaskItem(t, go);
        }

        go.GetComponent<RectTransform>().SetParent(tasksView, false);
        taskItems.Add(t, ti);

        activeTasks.Add(t);
    }

    private static void CountTask(Task t)
    {
        TaskType type = TaskType.Delivery;

        if (t is BuildTask)
        {
            switch (((BuildTask)t).type)
            {
                case EType.Electronic:
                    type = TaskType.Electronic;
                    break;
                case EType.Plastic:
                    type = TaskType.Plastic;
                    break;
                case EType.Packaging:
                    type = TaskType.Packaging;
                    break;
                default:
                    throw new UnityException("OUPS");
            }
        }

        if (t.GetWorker() != null)
            t.GetWorker().achieveTask(type);
    }

	public void DestroyAllTasks()
	{
		productionStopped = true;
		foreach (GetTask t in primalTasks)
		{
			t.DestroyTask();
		}
		primalTasks.Clear();
		activeTasks.Clear ();
		taskItems.Clear ();
		productionStopped = false;
	}

	public bool BasicTasksInitialized()
	{
		return basicTasksInitialized;
	}

	public void IncreaseConcurrentTasks()
	{
		ConcurrentTasks += 1;
		if (basicTasksInitialized && primalTasks.Count < ConcurrentTasks && !productionStopped)
		    primalTasks.Add(GetMeComponent (EComponent.Packaging, map.GetFinishTile (), null));
	}

	public bool ShouldAddNewTask()
	{
		return basicTasksInitialized && primalTasks.Count < ConcurrentTasks && !productionStopped;
	}

	public void DecreaseConcurrentTasks()
	{
		if (ConcurrentTasks == 0)
			Debug.LogError ("[TASKMANAGER] Tried to decrease number of Concurrent tasks where it is already 0");
		ConcurrentTasks -= 1;
	}

	public Dictionary<Workshop, uint> GetWorkshopNbOfActiveTasks()
	{
		return workshopNbOfActiveTasks;
	}

	public void DecreaseWorkshopNbOfActiveTasks(Workshop w)
	{
		if (!workshopNbOfActiveTasks.ContainsKey(w))
			Debug.LogError ("[TASKMANAGER] Tried to decrease WorkshopNbOfActiveTasks where Workshop was not added in workshopNbOfActiveTasks");

		if (workshopNbOfActiveTasks[w] == 0)
			Debug.LogError ("[TASKMANAGER] Tried to decrease number of WorkshopNbOfActiveTasks where it is already 0");
		--workshopNbOfActiveTasks [w];
	}
}
