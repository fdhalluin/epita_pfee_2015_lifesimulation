﻿using UnityEngine;
using System.Collections;

public class FacingSprite : MonoBehaviour
{
	Camera cam;

	void Start()
	{
		cam = Camera.main;
	}

	void Update()
	{
		transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward * -1, Vector3.up);
	}
}
