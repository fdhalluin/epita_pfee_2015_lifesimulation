﻿using UnityEngine;
using System.Collections;

public class ComponentCube : MonoBehaviour
{	
	// Basic, Plastics, Electro, Finished
	public Material[] componentsMaterials;
	public MeshRenderer renderer;

	private EComponent type = EComponent.Basic;

	public void SetComponentType(EComponent comp)
	{
		type = comp;
		switch (type)
		{
		case EComponent.Basic:
			renderer.material = componentsMaterials[0];
			break;
		case EComponent.Plastic:
			renderer.material = componentsMaterials[1];
			break;
		case EComponent.Electronic:
			renderer.material = componentsMaterials[2];
			break;
		default:
			renderer.material = componentsMaterials[3];
			break;
		}
	}
}
