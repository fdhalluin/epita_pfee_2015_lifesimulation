using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum EGoal
{
	Nothing,    // 0
	Build,     // 1
	Carrying,  // 2
	WaitingToBuild,    // 3
	WaitingATask //4
}

public enum EStats
{
	Electronics,
	Plastics,
	Packaging,
	MovingSpeed
}

public enum TaskType
{
    Delivery,
    Electronic,
    Plastic,
    Packaging
}

public class Agent: MonoBehaviour
{
	// Units per step
	public int movingSpeedAttribute;
	public int electronics;
	public int plastics;
	public int packaging;

	public float actualSpeed;
	// Basic, Plastics, Electro, Finished
	public Material[] componentsMaterials;
	public MeshRenderer componentCube;
	public GameObject SelectionCanvas;

    public Color baseColor;
    public Color electronicsColor;
    public Color packagingColor;
    public Color plasticsColor;

    public int achievedTasks { get; private set; }

    private Dictionary<TaskType, float> tasksCount;

	public uint X
	{
		get
		{
			return (uint) Mathf.FloorToInt(transform.position.x);
		}
	}

	public uint Y
	{
		get
		{
			return (uint)Mathf.FloorToInt (transform.position.z);
		}
	}

	private Vector3[] ScaleForSpeed =
	{
		// Speeds 1 to 5
		new Vector3(0.5f, 0.30f, 0.5f),
		new Vector3(0.5f, 0.35f,0.5f),
		new Vector3(0.5f, 0.5f,0.5f),
		new Vector3(0.47f, 0.59f, 0.47f),
		new Vector3(0.38f, 0.67f, 0.38f)
	};

	public Tile Goal;
	public Tile Goal2;
    public Map map;
	public ProgressBar progressBar;

	public EComponent componentCarrying;

	public GameObject robotContainer;
	public Renderer headRend;
	public Renderer armLeftRend;
	public Renderer armRightRend;
	public Renderer bodyRend;

	public Color highlight;
	public Collider colider;

	public Transform leftArm;
	public Transform rightArm;

	public string Name;

	public EGoal typeOfWork;

	private bool carrying = false;

	private Color baseHeadColor;
	private Color baseBodyColor;
	private bool occupied = false;

	private Task TaskAssigned;
	private EStats mainStat;
    // Navigation variables
    private NavMeshAgent navMeshAgent;

    private bool reachedDestinationOnce = true;


	void Awake()
	{
		navMeshAgent = GetComponent<NavMeshAgent>();
		navMeshAgent.speed = movingSpeedAttribute / 2.0f;
		actualSpeed = movingSpeedAttribute / 2.0f;
		baseBodyColor = bodyRend.material.color;
		SetCarrying(false);
		Name = NameGenerator.GenerateName() + " " + NameGenerator.GenerateName();
        achievedTasks = 0;

        tasksCount = new Dictionary<TaskType, float>();
        foreach (TaskType task in Enum.GetValues(typeof(TaskType)))
            tasksCount[task] = 0;
	
		movingSpeedAttribute = UnityEngine.Random.Range(1,9);
		plastics = UnityEngine.Random.Range(1,9);
		electronics = UnityEngine.Random.Range(1,9);
		packaging = UnityEngine.Random.Range(1,9);
	}

	void Start()
	{
		navMeshAgent.speed = (movingSpeedAttribute / 2.0f);
		actualSpeed = (movingSpeedAttribute / 2.0f);
		robotContainer.transform.localScale = ScaleForSpeed[(int)((movingSpeedAttribute + 1) / 2.0) - 1];

		if (plastics > electronics)
		{
			if (plastics > packaging)
				mainStat = EStats.Plastics;
			else
				mainStat = EStats.Packaging;
		}
		else
		{
			if (electronics > packaging)
				mainStat = EStats.Electronics;
			else
				mainStat = EStats.Packaging;
		}

		switch (mainStat)
		{
		case EStats.Electronics:
			baseHeadColor = Color.Lerp(baseColor, electronicsColor, (electronics - 5) / 5f);;
			break;
		case EStats.Packaging:
			baseHeadColor = Color.Lerp(baseColor, packagingColor, (packaging - 5) / 5f);
			break;

		case EStats.Plastics:
			baseHeadColor = Color.Lerp(baseColor, plasticsColor, (plastics - 5) / 5f);
			break;

		default:
			break;
		}
	}

	public void InitWithStats(int speed, int plast, int elec, int pack, string name)
	{
		Name = name;
		movingSpeedAttribute = speed;
		plastics = plast;
		electronics = elec;
		packaging = pack;

		Start();
	}

	void Update()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity) && colider == hitInfo.collider)
		{
			// The mouse is over the agent
			headRend.material.color = highlight;
			armLeftRend.material.color = highlight;
			armRightRend.material.color = highlight;
			bodyRend.material.color = highlight;

			if (Input.GetMouseButtonDown(0))
			{
				UIManager.instance.DisplayAgentInfo(this);
				SelectionCanvas.SetActive(true);
			}
		}
		else
		{
			headRend.material.color = baseHeadColor;
			armLeftRend.material.color = baseBodyColor;
			armRightRend.material.color = baseBodyColor;
			bodyRend.material.color = baseBodyColor;
		}
	}

	void FixedUpdate()
	{
		if ((!navMeshAgent.pathPending) &&
		    (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) &&
		    (navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f))
		{
			if (!reachedDestinationOnce)
			{
				reachedDestinationOnce = true;


				if (TaskManager.instance.IAT == IAType.Rando)
					map.AgentUpdate(this);
				else
					map.AgentUpdate2(this);
			}
			else if (typeOfWork == EGoal.WaitingToBuild && TaskManager.instance.IAT != IAType.Rando)
				map.AgentUpdate2(this);
		}
		else
			reachedDestinationOnce = false;
	}

	void OnDestroy()
	{
		if (UIManager.instance != null)
			UIManager.instance.OnAgentDestroyed(this);
	}

	public void Move (Tile tile)
    {
        reachedDestinationOnce = false;
        navMeshAgent.SetDestination(tile.transform.position);
        Goal = tile;
		Goal2 = null;
	}
	
	public void DoubleMove (Tile tile, Tile tile2)
	{
		reachedDestinationOnce = false;
		navMeshAgent.SetDestination(tile.transform.position);
		Goal = tile;
		Goal2 = tile2;
	}

    public void Select()
    {
		SelectionCanvas.SetActive(true);
    }

	public void Unselect()
	{
		SelectionCanvas.SetActive(false);
	}
	
	public bool Carrying()
	{
		return carrying;
	}

    public float efficiency()
    {
        if (Counter.workersTasks == 0)
            return 0;
        else
            return (float)achievedTasks / Counter.workersTasks; 
    }

    public void achieveTask(TaskType task)
    {
        Counter.incrementWorkers(task);
        achievedTasks += 1;
        tasksCount[task] += 1;
    }

    public float[] occupation()
    {
        Array types = Enum.GetValues(typeof(TaskType));
        float[] occupations = new float[types.Length];

        for (int i = 0; i < types.Length; ++i)
        {
            TaskType type = (TaskType)types.GetValue(i);

            if (achievedTasks == 0 || Counter.tasksCount[type] == 0)
                occupations[i] = 0;
            else
			{
                // occupations[i] = tasksCount[(TaskType)types.GetValue(i)] / (achievedTasks * Counter.tasksCount[type]);
				occupations[i] = tasksCount[(TaskType)types.GetValue(i)] / achievedTasks;
			}
        }

        return occupations;
    }

	public float[] tasksDone()
	{
		Array types = Enum.GetValues(typeof(TaskType));
		float[] tasks = new float[types.Length];
		for (int i = 0; i < types.Length; ++i)
		{
			TaskType type = (TaskType)types.GetValue(i);

			tasks[i] = tasksCount[(TaskType)types.GetValue(i)];
		}
		
		return tasks;
	}

	public void SetCarrying(bool b)
	{
		carrying = b;

		componentCube.gameObject.SetActive(b);
		if (carrying)
		{
			leftArm.localRotation = new Quaternion(0,0,0,0);
			leftArm.localPosition = new Vector3(leftArm.localPosition.x, leftArm.localPosition.y, 0.5f);
			rightArm.localRotation = new Quaternion(0,0,0,0);
			rightArm.localPosition = new Vector3(rightArm.localPosition.x, rightArm.localPosition.y, 0.5f);

			
			switch (componentCarrying)
			{
			case EComponent.Basic:
				componentCube.material = componentsMaterials[0];
				break;
			case EComponent.Plastic:
				componentCube.material = componentsMaterials[1];
				break;
			case EComponent.Electronic:
				componentCube.material = componentsMaterials[2];
				break;
			default:
				componentCube.material = componentsMaterials[3];
				break;
			}
		}
		else
		{
			leftArm.localRotation = Quaternion.Euler(-90,0,0);
			leftArm.localPosition = new Vector3(leftArm.localPosition.x, leftArm.localPosition.y, 0);
			rightArm.localRotation = Quaternion.Euler(-90,0,0);
			rightArm.localPosition = new Vector3(rightArm.localPosition.x, rightArm.localPosition.y, 0);
		}
	}

	private void ReactivateAvoidance()
	{
		navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
	}

	public void SetProgressBarValue(float value)
	{
		progressBar.SetFillAmount(value);
	}

	public void SetProgressBarVisibility(bool visible)
	{
		progressBar.gameObject.SetActive(visible);
	}

	public void ResetProgressBar()
	{
		progressBar.ResetFillAmount();
	}

	public NavMeshAgent GetNavMeshAgent()
	{
		return navMeshAgent;
	}

	public bool Occupied()
	{
		return occupied;
	}

	public void SetOccupation(bool b)
	{
		occupied = b;
	}



	public void AssignTask(Task t)
	{
		TaskAssigned = t;
	}

	public Task GetAssignedTask()
	{
		return TaskAssigned;
	}
}
