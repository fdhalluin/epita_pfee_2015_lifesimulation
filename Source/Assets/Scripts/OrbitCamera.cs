﻿using UnityEngine;
using System.Collections;

// Simple orbit camera
using UnityEngine.EventSystems;


public class OrbitCamera : MonoBehaviour
{
    public Vector3 CenterPosition = Vector3.zero;

    public float Zoom = 5;
    public float ZoomSpeed = 3;
    public float MinZoom = 1;
    public float MaxZoom = 10;

    public float ArrowMoveSpeed = 1;

    public float ZoomOrthographicSizeFactor = 1;
    public float ZoomDistanceFactor = 10;

    public float RotatePitchFactor = 3;
    public float RotateYawFactor = 3;

	public int PanButtonId = 2;
	public int RotateButtonId = 1;

	private static readonly float _minPitch = 5f;
	private static readonly float _maxPitch = 85f;

    private Vector3 targetPos;
    public float moveDampingFactor = 0.25f;

    private Vector2 _pressedMousePosition;
    private Vector3 _pressedCenterPosition;

    void Update()
    {
        var mouseScrollWheel = Input.GetAxis("Mouse ScrollWheel");

        if (! EventSystem.current.IsPointerOverGameObject() && Mathf.Abs(mouseScrollWheel) > float.Epsilon)
        {
            Zoom = Mathf.Clamp(Zoom + ZoomSpeed * -mouseScrollWheel, MinZoom, MaxZoom);
        }

		if (Input.GetMouseButtonDown(PanButtonId))
        {
            _pressedMousePosition = Input.mousePosition;
            _pressedCenterPosition = CenterPosition;
        }

		if (Input.GetMouseButton(PanButtonId))
        {
            var oldRay = GetComponent<Camera>().ScreenPointToRay(_pressedMousePosition);
            var oldGroundPosition = oldRay.origin + oldRay.direction * -oldRay.origin.y / oldRay.direction.y;
            var newRay = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            var newGroundPosition = newRay.origin + newRay.direction * -newRay.origin.y / newRay.direction.y;
            var offset = newGroundPosition - oldGroundPosition;
            CenterPosition = _pressedCenterPosition - offset;
        }


        if (Input.GetMouseButton(RotateButtonId))
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            var euler = transform.localEulerAngles;
            euler += new Vector3(-mouseY * RotatePitchFactor, mouseX * RotateYawFactor, 0);
            euler.x = Mathf.Clamp(euler.x, _minPitch, _maxPitch);
            transform.localEulerAngles = euler;
        }
        else
        {
			float h = Input.GetAxisRaw("Horizontal");
			float v = Input.GetAxisRaw("Vertical");

			// Project camera orientation on horizontal plane
			Vector3 f = transform.forward;
			f.y = 0;
			f.Normalize();

			Quaternion r = Quaternion.LookRotation(f);

			CenterPosition += r * Vector3.forward * ArrowMoveSpeed * v;
			CenterPosition += r * Vector3.right * ArrowMoveSpeed * h;
        }


		transform.position = CenterPosition - transform.localRotation * Vector3.forward * Zoom * ZoomDistanceFactor;
        //transform.position = Utils.Damp(transform.position, targetPos, moveDampingFactor, Time.deltaTime);

        GetComponent<Camera>().orthographicSize = Zoom * ZoomOrthographicSizeFactor;
    }
}
