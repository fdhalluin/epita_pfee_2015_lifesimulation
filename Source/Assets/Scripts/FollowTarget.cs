﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour
{
	public GameObject Target;
	public float Distance;
	public float Damping;

	void LateUpdate()
	{
		if (Target == null)
			return;

		var offset = (Target.transform.position - transform.position);
		var distance = offset.magnitude;
	
		if (distance > Distance)
		{
			var direction = offset.normalized;

			// Same damping as in Character.cs
			transform.position = Vector3.Lerp(transform.position,
			                                  Target.transform.position - direction * Distance,
			                                  Time.deltaTime / (Time.deltaTime + Damping));
		}
	}
}
