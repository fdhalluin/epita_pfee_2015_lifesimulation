﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

// Solves the assignment problem with Munkres' algorithm
// Done withe the help of http://csclab.murraystate.edu/bob.pilgrim/445/munkres.html

public class Solver
{
	private List<Agent> Agents;
	private List<Task> Tasks;
	private int nRows;
	private int nCols;
	private int[,] Costs;
	private int[,] Mask;
	private bool[] RowCover;
	private bool[] ColCover;
	private int pathRow0;
	private int pathCol0;
	private List<KeyValuePair<int, int>> path;
	private Dictionary<Agent, Task> solution;
	private bool valid;
	private int startOfParentTasks;
	private bool rotated;

	private int step;

	// Rows = Workers; Cols = Tasks
	public Solver(List<Agent> agents, List<Task> tasks)
	{
		if (agents == null || agents.Count == 0 || tasks == null || tasks.Count == 0)
			Debug.LogError("Creating solver with null agents or tasks");

		rotated = false;
		this.Agents = new List<Agent>();
		this.Tasks = new List<Task>();

		foreach (Agent a in agents)
		{
			if (! a.Carrying())
				this.Agents.Add(a);
		}

		foreach (Task t in tasks)
		{
			if (t.GetWorker() && t.GetWorker().Carrying())
			{
				// Do not add the task, worker is still carrying somtething
			}
			else
			{
				this.Tasks.Add(t);
			}
		}
		
		startOfParentTasks = this.Tasks.Count;
		
		if (TaskManager.instance.solverAnticipation && this.Agents.Count > this.Tasks.Count)
		{
			// Add the parent tasks
			for (int i = 0; i < startOfParentTasks; ++i)
			{
				Task t = this.Tasks[i].GetFather();
				if (t != null && ! this.Tasks.Contains(t))
				{
					this.Tasks.Add(t);
				}
			}
		}

		solution = new Dictionary<Agent, Task>();

		this.nRows = this.Agents.Count;
		this.nCols = this.Tasks.Count;

		if (nRows == 0 || nCols == 0)
		{
			// Nothing to assign
			valid = false;
			step = 8;
			return;
		}

		CreateCostMatrix(this.Agents, this.Tasks);
		valid = true;

		if (nCols < nRows)
		{
			// We need to rotate the matrix
			int[,] rotatedCosts = new int[nCols, nRows];

			for (int i = 0; i < nRows; ++i)
			{
				for (int j = 0; j < nCols; ++j)
				{
					rotatedCosts[j, i] = Costs[i, j];
				}
			}

			Costs = rotatedCosts;

			int temp = nCols;
			nCols = nRows;
			nRows = temp;
			rotated = true;
		}

		// Initializing masks to default values
		Mask = new int[nRows, nCols];
		RowCover = new bool[nRows];
		ColCover = new bool[nCols];
		for (int r = 0; r < nRows; ++r)
		{
			RowCover[r] = false;
			for (int c = 0; c < nCols; ++c)
			{
				Mask[r, c] = 0;
			}
		}
		for (int c = 0; c < nCols; ++c)
			ColCover[c] = false;

		pathRow0 = -1;
		pathCol0 = -1;
		path = new List<KeyValuePair<int, int>>();
		step = 1;
	}

	private void CreateCostMatrix(List<Agent> agents, List<Task> tasks)
	{
		Costs = new int[nRows, nCols];

		for (int i = 0; i < agents.Count; ++i)
		{
			Agent a = agents[i];

			// First compute only the children tasks
			for (int j = 0; j < startOfParentTasks; ++j)
			{
				Task t = tasks[j];

				if (t is GetTask)
				{
					GetTask g = (GetTask)t;

					int cost = ComputeGetTaskCost(g, a);

					Costs[i, j] = cost;
				}
				else
				{
					BuildTask b = (BuildTask)t;

					int cost = ComputeBuildTaskCost(b, a);

					Costs[i, j] = cost;
				}
			}
		}

		// Then, compute the parents tasks
		if (startOfParentTasks < nCols)
		{
			for (int i = 0; i < agents.Count; ++i)
			{
				Agent a = agents[i];

				for (int j = startOfParentTasks; j < nCols; ++j)
				{
					Task t = tasks[j];
					
					if (t is GetTask)
					{
						GetTask g = (GetTask)t;
						BuildTask son = g.getWaitTask();
						int index = FindIndexOfTask(son);
						if (index == Tasks.Count)
							Debug.LogError("[SOLVER] Son not found for task " + g);

						int cost = GetMaxCostForTask(index);
						cost += ComputeGetTaskCost(g, a);

						Costs[i, j] = cost;
					}
					else
					{
						BuildTask b = (BuildTask)t;

						int cost = 0;
						foreach (GetTask son in b.getTasksList())
						{
							int index = FindIndexOfTask(son);
							if (index == Tasks.Count)
								continue;
							
							cost += GetMaxCostForTask(index);
						}
						cost += ComputeBuildTaskCost(b, a);

						Costs[i, j] = cost;
					}
				}
			}
		}
	}

	private int ComputeBuildTaskCost(BuildTask b, Agent a)
	{
		int cost = 0;
		
		Tile tile = b.GetArrival();
		
		Workshop ws = tile.GetComponent<Workshop>();
		if (ws == null)
			Debug.LogError("[SOLVER] CreateCostMatrix : found a buildtask with an arrival tile that is not a workshop : " + tile);
		
		
		switch (ws.type)
		{
		case EType.Plastic:
			cost = (int)(Mathf.Lerp((11 - a.plastics), 0, (10f - ws.CurrentProduction()) / 10f) * 1000f);
			cost += (int)((Vector3.Distance(a.transform.position, ws.transform.position) / a.actualSpeed) * 1000f);
			break;
			
		case EType.Electronic:
			cost = (int)(Mathf.Lerp((11 - a.electronics), 0, (10f - ws.CurrentProduction()) / 10f) * 1000f);
			cost += (int)((Vector3.Distance(a.transform.position, ws.transform.position) / a.actualSpeed) * 1000f);
			break;
			
		case EType.Packaging:
			cost = (int)(Mathf.Lerp((11 - a.packaging), 0, (10f - ws.CurrentProduction()) / 10f) * 1000f);
			cost += (int)((Vector3.Distance(a.transform.position, ws.transform.position) / a.actualSpeed) * 1000f);
			break;
		}

		return cost;
	}

	private int ComputeGetTaskCost(GetTask g, Agent a)
	{
		int cost = 0;
		
		if (! a.Carrying())
		{
			cost += (int)((Vector3.Distance(a.transform.position, g.GetDepartTile().transform.position) / a.actualSpeed) * 1000f);
			cost += (int)((Vector3.Distance(g.GetDepartTile().transform.position, g.GetArrival().transform.position) / a.actualSpeed) * 1000f);
		}
		else
		{
			cost += (int)((Vector3.Distance(a.transform.position, g.GetArrival().transform.position) / a.actualSpeed) * 1000f);
		}

		return cost;
	}

	private int FindIndexOfTask(Task t)
	{
		int i = 0;
		while (i < Tasks.Count && Tasks[i] != t)
			i += 1;
		return i;
	}

	private int GetMaxCostForTask(int index)
	{
		int res = 0;
		for (int i = 0; i < nRows; ++i)
		{
			int c = Costs[i, index];
			if (c > res)
				res = c;
		}
		return res;
	}

	public Dictionary<Agent, Task> RunSolver()
	{
		if (this.Costs == null)
		{
			Debug.LogError("Trying to run solver with null costs matrix");
			return null;
		}

		/*
		int[] assignments = HungarianAlgorithm.FindAssignments(this.Costs);

		for (int a = 0; a < assignments.Count(); ++a)
		{
			solution.Add(this.Agents.ElementAt(a), this.Tasks.ElementAt(assignments[a]));
		}
		valid = true;

		*/
		bool done = false;
		while (!done)
		{
			switch (step)
			{
			case 1:
				StepOne();
				break;
			case 2:
				StepTwo();
				break;
			case 3:
				StepThree();
				break;
			case 4:
				StepFour();
				break;
			case 5:
				StepFive();
				break;
			case 6:
				StepSix();
				break;
			case 7:
				StepSeven();
				done = true;
				break;
			case 8:
				// Solver not valid (no agent or task)
				done = true;
				break;
			}
		}

		return solution;
	}

	// For each row of the cost matrix, find the smallest element and substract
	// it from every element in its row.
	private void StepOne()
	{
		int minInRow;

		for (int r = 0; r < nRows; ++r)
		{
			minInRow = Costs[r, 0];
			for (int c = 0; c < nCols; ++c)
			{
				if (Costs[r, c] < minInRow)
					minInRow = Costs[r, c];
			}

			for (int c = 0; c < nCols; ++c)
			{
				Costs[r, c] -= minInRow;
			}
		}

		this.step = 2;
	}

	// Find a zero (Z) in the matrix. If there is no starred zero in its row or
	//  column, star Z. Repeat for each element in the matrix.
	private void StepTwo()
	{
		for (int r = 0; r < nRows; ++r)
		{
			for (int c = 0; c < nCols; ++c)
			{
				if (Costs[r, c] == 0 && (! RowCover[r]) && (! ColCover[c]))
				{
					Mask[r, c] = 1;
					RowCover[r] = true;
					ColCover[c] = true;
				}
			}
		}

		for (int r = 0; r < nRows; ++r)
			RowCover[r] = false;
		for (int c = 0; c < nCols; ++c)
			ColCover[c] = false;

		this.step = 3;
	}

	// Cover each column containing a starred zero. If K columns are covered (K = min(nRows, nCols))
	// the starred zeros describe a complete set of unique assignments and we are done.
	private void StepThree()
	{
		int colCount = 0;
		for (int r = 0; r < nRows; ++r)
		{
			for (int c = 0; c < nCols; ++c)
			{
				if (Mask[r, c] == 1 && Costs[r, c] == 0)
				{
					ColCover[c] = true;
				}
			}
		}

		for (int c = 0; c < nCols; ++c)
		{
			if (ColCover[c])
				colCount += 1;
		}

		if (colCount >= nCols || colCount >= nRows)
			this.step = 7; // DONE
		else
			this.step = 4;
	}

	// Find a noncovered zero and prime it. If there is no starred zero in the
	// row containing this primed zero, go to step 5. Otherwise, cover this row
	// and uncover the column containing the starred zero. Continue until there
	// are no uncovered zeros left, and save the smallest uncovered value, and
	// go to step 6.
	private void StepFour()
	{
		int row = -1;
		int col = -1;
		bool done = false;

		while (! done)
		{
			FindAZero(ref row, ref col);
			if (row == -1)
			{
				done = true;
				this.step = 6;
			}
			else
			{
				Mask[row, col] = 2;
				if (StarInRow(row))
				{
					FindStarInRow(row, ref col);
					RowCover[row] = true;
					ColCover[col] = false;
				}
				else
				{
					done = true;
					this.step = 5;
					pathRow0 = row;
					pathCol0 = col;
				}
			}
		}
	}

	private void FindAZero(ref int row, ref int col)
	{
		int r = 0;
		int c = 0;

		bool done = false;
		row = -1;
		col = -1;

		while (! done)
		{
			c = 0;
			while (true)
			{
				if (Costs[r, c] == 0 && (! RowCover[r]) && (! ColCover[c]))
				{
					row = r;
					col = c;
					done = true;
				}
				c += 1;
				if (done || c >= nCols)
					break;
			}
			r += 1;
			if (r >= nRows)
				done = true;
		}
	}

	private bool StarInRow(int row)
	{
		bool tmp = false;
		for (int c = 0; c < nCols; ++c)
		{
			if (Mask[row, c] == 1)
			{
				tmp = true;
				break;
			}
		}
		return tmp;
	}

	private void FindStarInRow(int row, ref int col)
	{
		col = -1;
		for (int c = 0; c < nCols; ++c)
		{
			if (Mask[row, c] == 1)
			{
				col = c;
				break;
			}
		}
	}

	// Construct a series of alternating  primed and starred zeros as follows :
	// Let Z0 represent the uncovered primed zero found in step 4. Let Z1 be the
	// starred zero in the column of Z0 (if any). Let Z2 be the primed zero in the
	// row of Z1 (there will always be one). Continue until the series terminates
	// at a primed zero that has no starred zero in its column. Unstar each starred
	// zero of the series, star each primed zero of the series, erase all primes and
	// uncover every line in the matrix. Return to step 3.
	public void StepFive()
	{
		bool done = false;
		int r = -1;
		int c = -1;

		KeyValuePair<int, int> pair = new KeyValuePair<int, int>(pathRow0, pathCol0);
		path.Add(pair);

		while (! done)
		{
			if (path.Count > (nRows * nCols) + 10)
			{
				Debug.LogError("[SOLVER] Infinite loop in step 5");
				path.Clear();
				this.step = 7;
				valid = false;
				return;
			}

			r = FindStarInCol(path.Last().Value);
			if (r > -1)
			{
				KeyValuePair<int, int> pair2 = new KeyValuePair<int, int>(r, path.Last().Value);
				path.Add(pair2);
			}
			else
			{
				done = true;
			}

			if (! done)
			{
				c = FindPrimeInRow(r);
				KeyValuePair<int, int> pair3 = new KeyValuePair<int, int>(r, c);
				path.Add(pair3);
			}
		}

		AugmentPath();
		ClearCovers();
		ErasePrimes();

		this.step = 3;
	}

	private int FindStarInCol(int c)
	{
		int r = -1;
		for (int i = 0; i < nRows; ++i)
		{
			if (Mask[i, c] == 1)
			{
				r = i;
				break;
			}
		}

		return r;
	}

	private int FindPrimeInRow(int r)
	{
		int c = -1;
		for (int j = 0; j < nCols; ++j)
		{
			if (Mask[r, j] == 2)
			{
				c = j;
				break;
			}
		}

		return c;
	}

	private void AugmentPath()
	{
		for (int p = 0; p < path.Count; ++p)
		{
			KeyValuePair<int, int> pair = path[p];
			if (Mask[pair.Key, pair.Value] == 1)
				Mask[pair.Key, pair.Value] = 0;
			else
				Mask[pair.Key, pair.Value] = 1;
		}

		path.Clear();
	}

	private void ClearCovers()
	{
		for (int r = 0; r < nRows; ++r)
			RowCover[r] = false;
		for (int c = 0; c < nCols; ++c)
			ColCover[c] = false;
	}

	private void ErasePrimes()
	{
		for (int r = 0; r < nRows; ++r)
		{
			for (int c = 0; c < nCols; ++c)
			{
				if (Mask[r, c] == 2)
					Mask[r, c] = 0;
			}
		}
	}

	// Add the value found in step 4 to every element of each covered row, and
	// subtract it from every element of each uncovered column. Return to step
	// 4 without altering any stars, primes or covered lines.
	private void StepSix()
	{
		int minVal = int.MaxValue;
		FindSmallest(ref minVal);
		for (int r = 0; r < nRows; ++r)
		{
			for (int c = 0; c < nCols; ++c)
			{
				if (RowCover[r])
					Costs[r, c] += minVal;
				if (! ColCover[c])
					Costs[r, c] -= minVal;
			}
		}

		this.step = 4;
	}

	private void FindSmallest(ref int minVal)
	{
		for (int r = 0; r < nRows; ++r)
		{
			for (int c = 0; c < nCols; ++c)
			{
				if ((! RowCover[r]) && (! ColCover[c]))
				{
					if (minVal > Costs[r, c])
						minVal = Costs[r, c];
				}
			}
		}
	}

	// Fill the solution with starred zeros
	private void StepSeven()
	{
		int numAssigned = 0;
		for (int r = 0; r < nRows; ++r)
		{
			for (int c = 0; c < nCols; ++c)
			{
				if (Mask[r, c] == 1 && Costs[r, c] == 0)
				{
					Agent a;
					Task t;

					if (rotated)
					{
						a = Agents.ElementAt(c);
						t = Tasks.ElementAt(r);
					}
					else
					{
						a = Agents.ElementAt(r);
						t = Tasks.ElementAt(c);
					}

					if (a == null || t == null)
					{
						Debug.LogError("[SOLVER] Step 7 : found an agent or task that is null in the list");
					}

					solution.Add(a, t);
					numAssigned++;
				}
			}
		}
		if (numAssigned != nRows && numAssigned != nCols)
		{
			Debug.LogError("[SOLVER] Assigned " + numAssigned + " workers out of " + nRows);
		}
		else
			valid = true;
	}

	public bool IsValid()
	{
		return valid;
	}
}

public static class HungarianAlgorithm
{
	/// <summary>
	/// Finds the optimal assignments for a matrix of agents and costed tasks.
	/// </summary>
	/// <param name="costs">A cost matrix; each row contains elements that represent the associated costs of each
	/// task for the agent.</param>
	/// <returns>An array of assignments; element <em>i</em> is the index of the assigned task (column) for agent
	/// (row) <em>i</em>.</returns>
	public static int[] FindAssignments(int[,] costs)
	{
		var h = costs.GetLength(0);
		var w = costs.GetLength(1);

		for (int i = 0; i < h; i++)
		{
			var min = int.MaxValue;
			for (int j = 0; j < w; j++)
				min = Math.Min(min, costs[i, j]);
			for (int j = 0; j < w; j++)
				costs[i, j] -= min;
		}

		var masks = new byte[h, w];
		var rowsCovered = new bool[h];
		var colsCovered = new bool[w];
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (costs[i, j] == 0 && !rowsCovered[i] && !colsCovered[j])
				{
					masks[i, j] = 1;
					rowsCovered[i] = true;
					colsCovered[j] = true;
				}
			}
		}
		ClearCovers(ref rowsCovered, ref colsCovered, w, h);

		var path = new Location[w * h];
		Location pathStart = default(Location);
		var step = 1;
		while (step != -1)
		{
			switch (step)
			{
			case 1:
				step = RunStep1(costs, masks, ref rowsCovered, ref colsCovered, w, h);
				break;
			case 2:
				step = RunStep2(costs, ref masks, ref rowsCovered, ref colsCovered, w, h, ref pathStart);
				break;
			case 3:
				step = RunStep3(costs, ref masks, ref rowsCovered, ref colsCovered, w, h, ref path, pathStart);
				break;
			case 4:
				step = RunStep4(ref costs, masks, rowsCovered, colsCovered, w, h);
				break;
			}
		}

		var agentsTasks = new int[h];
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (masks[i, j] == 1)
				{
					agentsTasks[i] = j;
					break;
				}
			}
		}
		return agentsTasks;
	}

	private static int RunStep1(int[,] costs, byte[,] masks, ref bool[] rowsCovered, ref bool[] colsCovered, int w, int h)
	{
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (masks[i, j] == 1)
					colsCovered[j] = true;
			}
		}
		var colsCoveredCount = 0;
		for (int j = 0; j < w; j++)
		{
			if (colsCovered[j])
				colsCoveredCount++;
		}
		if (colsCoveredCount == h)
			return -1;
		else
			return 2;
	}

	private static int RunStep2(int[,] costs, ref byte[,] masks, ref bool[] rowsCovered, ref bool[] colsCovered, int w, int h,
		ref Location pathStart)
	{
		Location loc;
		while (true)
		{
			loc = FindZero(costs, rowsCovered, colsCovered, w, h);
			if (loc.Row == -1)
			{
				return 4;
			}
			else
			{
				masks[loc.Row, loc.Column] = 2;
				var starCol = FindStarInRow(masks, w, loc.Row);
				if (starCol != -1)
				{
					rowsCovered[loc.Row] = true;
					colsCovered[starCol] = false;
				}
				else
				{
					pathStart = loc;
					return 3;
				}
			}
		}
	}

	private static int RunStep3(int[,] costs, ref byte[,] masks, ref bool[] rowsCovered, ref bool[] colsCovered, int w, int h,
		ref Location[] path, Location pathStart)
	{
		var pathIndex = 0;
		path[0] = pathStart;
		while (true)
		{
			var row = FindStarInColumn(masks, h, path[pathIndex].Column);
			if (row == -1)
				break;
			pathIndex++;
			path[pathIndex] = new Location(row, path[pathIndex - 1].Column);
			var col = FindPrimeInRow(masks, w, path[pathIndex].Row);
			pathIndex++;
			path[pathIndex] = new Location(path[pathIndex - 1].Row, col);
		}
		ConvertPath(ref masks, path, pathIndex + 1);
		ClearCovers(ref rowsCovered, ref colsCovered, w, h);
		ClearPrimes(ref masks, w, h);
		return 1;
	}

	private static int RunStep4(ref int[,] costs, byte[,] masks, bool[] rowsCovered, bool[] colsCovered, int w, int h)
	{
		var minValue = FindMinimum(costs, rowsCovered, colsCovered, w, h);
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (rowsCovered[i])
					costs[i, j] += minValue;
				if (!colsCovered[j])
					costs[i, j] -= minValue;
			}
		}
		return 2;
	}

	private static void ConvertPath(ref byte[,] masks, Location[] path, int pathLength)
	{
		for (int i = 0; i < pathLength; i++)
		{
			if (masks[path[i].Row, path[i].Column] == 1)
				masks[path[i].Row, path[i].Column] = 0;
			else if (masks[path[i].Row, path[i].Column] == 2)
				masks[path[i].Row, path[i].Column] = 1;
		}
	}

	private static Location FindZero(int[,] costs, bool[] rowsCovered, bool[] colsCovered,
		int w, int h)
	{
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (costs[i, j] == 0 && !rowsCovered[i] && !colsCovered[j])
					return new Location(i, j);
			}
		}
		return new Location(-1, -1);
	}

	private static int FindMinimum(int[,] costs, bool[] rowsCovered, bool[] colsCovered, int w, int h)
	{
		var minValue = int.MaxValue;
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (!rowsCovered[i] && !colsCovered[j])
					minValue = Math.Min(minValue, costs[i, j]);
			}
		}
		return minValue;
	}

	private static int FindStarInRow(byte[,] masks, int w, int row)
	{
		for (int j = 0; j < w; j++)
		{
			if (masks[row, j] == 1)
				return j;
		}
		return -1;
	}

	private static int FindStarInColumn(byte[,] masks, int h, int col)
	{
		for (int i = 0; i < h; i++)
		{
			if (masks[i, col] == 1)
				return i;
		}
		return -1;
	}

	private static int FindPrimeInRow(byte[,] masks, int w, int row)
	{
		for (int j = 0; j < w; j++)
		{
			if (masks[row, j] == 2)
				return j;
		}
		return -1;
	}

	private static void ClearCovers(ref bool[] rowsCovered, ref bool[] colsCovered, int w, int h)
	{
		for (int i = 0; i < h; i++)
			rowsCovered[i] = false;
		for (int j = 0; j < w; j++)
			colsCovered[j] = false;
	}

	private static void ClearPrimes(ref byte[,] masks, int w, int h)
	{
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (masks[i, j] == 2)
					masks[i, j] = 0;
			}
		}
	}

	private struct Location
	{
		public int Row;
		public int Column;

		public Location(int row, int col)
		{
			this.Row = row;
			this.Column = col;
		}
	}
}