﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InterfaceButton : MonoBehaviour
{
	public bool selected = false;
	private Button button;

	void Start ()
	{
		button = GetComponent<Button>();

		if (selected)
		{
			ColorBlock colors = new ColorBlock();
			colors.normalColor = new Color(0.9f, 0.9f, 0);
			colors.highlightedColor = new Color(0.9f, 0.9f, 0);
			colors.pressedColor = new Color(0.6f, 0.6f, 0);
			colors.colorMultiplier = 1.0f;
			button.colors = colors;
		}
		else
		{
			ColorBlock colors = new ColorBlock();
			colors.normalColor = new Color(1.0f, 1.0f, 1.0f);
			colors.highlightedColor = new Color(1.0f, 1.0f, 1.0f);
			colors.pressedColor = new Color(0.8f, 0.8f, 0.8f);
			colors.colorMultiplier = 1.0f;
			button.colors = colors;
		}
	}

	public bool Selected()
	{
		return selected;
	}

	public void OnClick()
	{
		selected = !selected;

		if (selected)
		{
			ColorBlock colors = new ColorBlock();
			colors.normalColor = new Color(0.9f, 0.9f, 0);
			colors.highlightedColor = new Color(0.9f, 0.9f, 0);
			colors.pressedColor = new Color(0.6f, 0.6f, 0);
			colors.colorMultiplier = 1.0f;
			button.colors = colors;
		}
		else
		{
			ColorBlock colors = new ColorBlock();
			colors.normalColor = new Color(1.0f, 1.0f, 1.0f);
			colors.highlightedColor = new Color(1.0f, 1.0f, 1.0f);
			colors.pressedColor = new Color(0.8f, 0.8f, 0.8f);
			colors.colorMultiplier = 1.0f;
			button.colors = colors;
		}
	}
}
