public class Task
{
	protected Tile arrival;
	protected Agent worker;
	protected Task father;
	protected bool used;

	public Task (Tile w, Task t)
	{
		this.arrival = w;
		father = t;
		used = false;
	}

	public Agent GetWorker()
	{
		return worker;
	}

	public Tile GetArrival()
	{
		return arrival;
	}

	public Task GetFather()
	{
		return father;
	}

	public void SetWorker(Agent a)
	{
		worker = a;
	}

	public void TaskDone()
	{
		used = true;
	}

	public void TaskNotDone()
	{
		used = false;
	}

	public bool IsTaskDone()
	{
		return used;
	}
}
