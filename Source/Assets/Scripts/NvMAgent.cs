﻿using UnityEngine;
using System.Collections;

// This is a DEBUG class for DebugBitch
public class NvMAgent : MonoBehaviour {

    public Transform target;
    private NavMeshAgent navMeshAgent;

	// Use this for initialization
	void Start () {
        navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        navMeshAgent.SetDestination(target.position);
	}
}
