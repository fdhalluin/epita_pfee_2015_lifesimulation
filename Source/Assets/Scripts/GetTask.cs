using System.Collections;
using UnityEngine;

public class GetTask : Task
{
	private Tile departTile;
	private BuildTask waitTask;
	private bool isProductionTask;
	private EComponent componentType;
	private Recipe recipe;

	public GetTask (Tile w, Tile p, Task t, EComponent comp, bool productionTask = false, Recipe r = null) : base(w, t)
	{
		departTile = p;
		ParseTile ();
		isProductionTask = productionTask;
		componentType = comp;
		recipe = r;
	}

	public void ParseTile()
	{
		used = false;
		worker = null;
		if (departTile.type == ESelection.DeliveryZone)
		{
			TaskManager.instance.AddActiveTask(this);
			Workshop w2 = arrival.GetComponent<Workshop> ();
			foreach (Tile t in w2.adjacentTiles)
				t.AddTask(this);
		}
		else
		{
			Workshop w = departTile.GetComponent<Workshop> ();
			/*if (w.DispCraftedItems () > 0)
			{
				w.DecreaseDispCraftedItems (1);
				TaskManager.instance.AddActiveTask (this);
			}
			else
				waitTask = new BuildTask(departTile, this);*/
			waitTask = new BuildTask(departTile, this);
			departTile.AddTask(this);
			foreach (Tile t in w.adjacentTiles)
				t.AddTask(this);
			if (arrival.GetComponent<Workshop>() != null)
			{
				Workshop w2 = arrival.GetComponent<Workshop> ();
				foreach (Tile t in w2.adjacentTiles)
					t.AddTask(this);
			}
		}
	}

	public BuildTask getWaitTask()
	{
		return waitTask;
	}

	public void AddWaitingTask(BuildTask t)
	{
		waitTask = t;
	}

	public void RemoveWaitingTask()
	{
		waitTask = null;
	}

	public Tile GetDepartTile()
	{
		return departTile;
	}

	public void SetDepartTile(Tile t)
	{
		departTile = t;
	}

	public void DestroyTask()
	{
		used = true;
		if (TaskManager.instance.getActiveTasks ().Contains (this) && waitTask == null)
		{
			if ((worker == null || !worker.Carrying()) && departTile.GetComponent<Workshop>() != null)
			{
				Workshop w = (Workshop) departTile.GetComponent<Workshop> ();
				//w.IncreaseDispCraftedItems (1);
				w.ReduceCraftedItems ();
			}
		}

		if (TaskManager.instance.getActiveTasks().Contains(this))
            TaskManager.instance.RemoveActiveTask(this);
		
		if (worker != null)
		{
			//worker.Move(TaskManager.instance.map.GetDeliveryTile());
			worker.SetCarrying(false);
			worker.typeOfWork = EGoal.Nothing;
			worker.SetOccupation(false);
			TaskManager.instance.map.DecreaseNumberOfOccupiedWorkers();
			worker.AssignTask(null);
			worker = null;
		}

		if (departTile != null && departTile.GetComponent<Workshop>() != null)
		{
			departTile.GetTasks().Remove (this);
			foreach(Tile t in ((Workshop) departTile.GetComponent<Workshop> ()).adjacentTiles)
			{
				t.GetTasks().Remove(this);
			}
		}

		if (arrival.GetComponent<Workshop>() != null)
		{
			foreach(Tile t in ((Workshop) arrival.GetComponent<Workshop> ()).adjacentTiles)
			{
				t.GetTasks().Remove(this);
			}
		}
		
		if (waitTask != null)
			waitTask.DestroyTask();
		waitTask = null;


	}

	public bool IsProductionTask()
	{
		return isProductionTask;
	}

	public EComponent ComponentType()
	{
		return componentType;
	}

	public Recipe GetRecipe()
	{
		return recipe;
	}
}
