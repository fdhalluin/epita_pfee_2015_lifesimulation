﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Singleton for managing each step
public class TimeManager : MonoBehaviour 
{
	// Singleton
	private static TimeManager _instance;
	public static TimeManager instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<TimeManager>();
			return _instance;
		}
	}

	public enum ETimeState
	{
		Pause,
		Play,
		Fast,
		Faster,
	}

	public int SecondsForOneGame = 600;

	// Time constants
	public const float FastFactor = 2f;
	public const float FasterFactor = 4f;

	public Text timeText;

	private ETimeState timeState = ETimeState.Play;
	private float startTime;
	private float currentTimeToEnd;

	void Start()
	{
		startTime = Time.time;
		currentTimeToEnd = SecondsForOneGame;
		Play();
	}

	void Update()
	{
		// if (Input.GetKeyDown(KeyCode.Return))
		//	ResetTime();

		currentTimeToEnd = SecondsForOneGame - (Time.time - startTime);

		if (Input.GetKeyDown(KeyCode.Backspace) && SecondsForOneGame > 60)
			SecondsForOneGame -= 60;

		if (currentTimeToEnd <= 0)
		{
			UIManager.instance.ShowOverviewPanel();
			Pause();
		}

		int seconds = (int)(currentTimeToEnd % 60);
		timeText.text = (int)(currentTimeToEnd / 60) + ":" + (seconds < 10 ? "0" : "") + seconds;
	}

	public void ResetTime()
	{
		startTime = Time.time;
		SecondsForOneGame = 600;
		currentTimeToEnd = SecondsForOneGame;
	}

	public void Pause()
	{
        SoundManager.instance.SetDim(true);

		Time.timeScale = 0;
		timeState = ETimeState.Pause;
	}

	public void Play()
	{
		if (! UIManager.instance.ConstructionMode())
		{
            if (timeState == ETimeState.Fast || timeState == ETimeState.Faster)
                if (currentTimeToEnd > 0)
                    SoundManager.instance.PlayClip(SoundEffect.scratch_slow);
            SoundManager.instance.SetDim(false);

			Time.timeScale = 1;
			timeState = ETimeState.Play;
		}
	}

	public void Fast()
	{
		if (! UIManager.instance.ConstructionMode())
		{
            if (timeState == ETimeState.Play || timeState == ETimeState.Pause)
                SoundManager.instance.PlayClip(SoundEffect.scratch);
            else if (timeState == ETimeState.Faster)
            {
                SoundManager.instance.PlayClip(SoundEffect.scratch_slow);
            }
            SoundManager.instance.SetDim(false);

			Time.timeScale = FastFactor;
			timeState = ETimeState.Fast;
		}
	}

	public void Faster()
	{
		if (! UIManager.instance.ConstructionMode())
		{
            if (timeState == ETimeState.Fast || timeState == ETimeState.Play || timeState == ETimeState.Pause)
                SoundManager.instance.PlayClip(SoundEffect.scratch);
            SoundManager.instance.SetDim(false);

			Time.timeScale = FasterFactor;
			timeState = ETimeState.Faster;
		}
	}

	public ETimeState GetState()
	{
		return timeState;
	}
}
