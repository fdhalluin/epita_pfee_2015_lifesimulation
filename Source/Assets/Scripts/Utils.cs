﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Some utils functions you may use
public static class Utils
{
	// Damp is a nice function to fade a value towards a target overtime
	// Use a factor between 0 (instant) and ~2 (very slow)
	public static float Damp(float source, float target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Mathf.Lerp(source, target, lerpFactor);
	}

	public static Color Damp(Color source, Color target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Color.Lerp(source, target, lerpFactor);
	}
	
	public static Vector3 Damp(Vector3 source, Vector3 target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Vector3.Lerp(source, target, lerpFactor);
	}
	
	public static Quaternion Damp(Quaternion source, Quaternion target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Quaternion.Slerp(source, target, lerpFactor);
	}

	// Sample point on a bezier curve (p0 and p3 are end points, p1 and p2 are control points)
	public static Vector3 CubicBezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
	{
		return Mathf.Pow(1 - t, 3) * p0 + 3 * Mathf.Pow(1 - t, 2) * t * p1 + 3 * (1 - t) * Mathf.Pow(t, 2) * p2 + Mathf.Pow(t, 3) * p3;
	}

	// Fisher-Yates shuffle for lists
	public static void Shuffle<T>(this IList<T> list) 
	{
		int n = list.Count;
		System.Random rnd = new System.Random();
		while (n > 1)
		{
			int k = (rnd.Next(0, n) % n);
			n--;
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	// Round/ceil/floor to a given precision
	public static float RoundTo(float value, float size)
	{
		return Mathf.Round(value / size) * size;
	}

	public static float CeilTo(float value, float size)
	{
		return Mathf.Ceil(value / size) * size;
	}

	public static float FloorTo(float value, float size)
	{
		return Mathf.Floor(value / size) * size;
	}
}


