﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour
{
	public Color highlight;
	public uint X;
	public uint Y;

	public ESelection type;
	public bool Walkable; // Can the player walk on this tile?

	private Map map;
	private Renderer[] rend;
	private Collider col;
	private List<Task> tasks;

	protected Color[] baseColor;

	public void Awake ()
	{
		rend = GetComponentsInChildren<Renderer>();
		col = GetComponentInChildren<Collider>();

		baseColor = new Color[rend.Length];
		for (int i = 0; i < rend.Length; i++)
		{
			baseColor[i] = rend[i].material.color;
		}

		tasks = new List<Task> ();
	}

	public void SetMap(Map m)
	{
		map = m;
	}

	public void SetBaseColor(Color c)
	{
		for (int i = 0; i < baseColor.Length; i++)
		{
			baseColor[i] = c;
		}
	}

	public bool IsWorkshop()
	{
		return (type == ESelection.WSElectronics || type == ESelection.WSPackaging || type == ESelection.WSPlastics);
	}

	void Update ()
	{
		if (UIManager.instance.ConstructionMode())
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			// Check that the mouse is not over the interface, then if it is over the tile
			if (! UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() &&
			    Physics.Raycast(ray, out hitInfo, Mathf.Infinity) && col == hitInfo.collider)
			{
				// The mouse is over the tile
				foreach (Renderer r in rend)
				{
					r.material.color = highlight;
				}

				if (Input.GetMouseButtonDown(0))
				{
					map.OnTileClicked(this);
				}
			}
			else
			{
				for (int i = 0; i < rend.Length; i++)
				{
					rend[i].material.color = baseColor[i];
				}
			}
		}
		else if (IsWorkshop())
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if (! UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() &&
			    Physics.Raycast(ray, out hitInfo, Mathf.Infinity) && col == hitInfo.collider)
			{
				// The mouse is over the tile
				foreach (Renderer r in rend)
				{
					r.material.color = highlight;
					
					if (Input.GetMouseButtonDown(0))
					{
						Workshop ws = gameObject.GetComponent<Workshop>();
						if (ws != null)
						{
							ws.Select();
							UIManager.instance.DisplayInfo(ws);
						}
					}
				}
			}
			else
			{
				for (int i = 0; i < rend.Length; i++)
				{
					rend[i].material.color = baseColor[i];
				}
			}
		}
		else
		{
			for (int i = 0; i < rend.Length; i++)
			{
				rend[i].material.color = baseColor[i];
			}
		}
	}

	public List<Task> GetTasks()
	{
		return tasks;
	}

	public void SetTasks(List<Task> lt)
	{
		tasks = lt;
	}

	public void AddTask(Task t)
	{
		tasks.Add(t);
	}
}
