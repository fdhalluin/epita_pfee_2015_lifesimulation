using System.Collections.Generic;
using UnityEngine;

public class BuildTask : Task {

    public EType type { get; private set; }
	private List<GetTask> tasks;
	private Recipe recipe;
	private uint elec;
	private uint plastic;
	private int totalNumberOfPlasticSons;
	private int totalNumberOfElecSons;
	private int totalNumberOfBasicSons;

	public BuildTask (Tile w, Task t) : base(w, t)
	{
		totalNumberOfPlasticSons = 0;
		totalNumberOfElecSons = 0;
		totalNumberOfBasicSons = 0;
		tasks = new List<GetTask> ();
		Workshop wo = w.GetComponent<Workshop>();
        type = wo.type;

		if (wo.type == EType.Packaging)
		{
			recipe = TaskManager.instance.currentProd.Current;
			elec = (uint) recipe.electronics;
			plastic = (uint) recipe.plastic;
		}


		ParseWorkshop ();
	}

	public void ParseWorkshop()
	{
		worker = null;
		Workshop w2 = arrival.GetComponent<Workshop> ();

		if (type == EType.Packaging)
		{
			for (int i = 0; i < elec; ++i)
			{	
				tasks.Add (TaskManager.instance.GetMeComponent (EComponent.Electronic, arrival, this));
				/*if (w2.DispElectronicComponents () > 0)
					w2.DecreaseDispElectronicComponents (1);
				else
					tasks.Add (TaskManager.instance.GetMeComponent (EComponent.Electronic, arrival, this));*/
				++totalNumberOfElecSons;
			}
			for (int i = 0; i < plastic; ++i)
			{
				tasks.Add (TaskManager.instance.GetMeComponent (EComponent.Plastic, arrival, this));
				/*if (w2.DispPlasticComponents () > 0)
					w2.DecreaseDispPlasticComponents (1);
				else
					tasks.Add (TaskManager.instance.GetMeComponent (EComponent.Plastic, arrival, this));*/
				++totalNumberOfPlasticSons;
			}
		}
		else
		{
			tasks.Add (TaskManager.instance.GetMeComponent (EComponent.Basic, arrival, this));
			/*if (w2.DispBasicComponents () > 0)
				w2.DecreaseDispBasicComponents (1);
			else
				tasks.Add (TaskManager.instance.GetMeComponent (EComponent.Basic, arrival, this));*/
			++totalNumberOfBasicSons;
		}


		foreach (Tile t in w2.adjacentTiles)
			t.AddTask(this);

		if (tasks.Count == 0)
			TaskManager.instance.AddActiveTask(this);
		//CAN I BUILD?
	}

	public void AddTask(GetTask task)
	{
		tasks.Add(task);
	}

	public void RemoveTask(GetTask task)
	{
		tasks.Remove (task);
	}

	public List<GetTask> getTasksList()
	{
		return tasks;
	}

	public void DestroyTask()
	{

		if (tasks.Count < totalNumberOfPlasticSons + totalNumberOfElecSons + totalNumberOfBasicSons && !used)
		{
			int pl = 0;
			int el = 0;
			int bs = 0;
			foreach (GetTask t in tasks)
			{
				if (t.ComponentType() == EComponent.Basic)
					++bs;
				else if (t.ComponentType() == EComponent.Plastic)
					++pl;
				else if (t.ComponentType() == EComponent.Electronic)
					++el;
				else
					Debug.LogError ("PACKAGING SHOULD NOT BE HERE");
			}

			Workshop w = arrival.GetComponent<Workshop> ();
			if (totalNumberOfPlasticSons - pl != 0)
				w.DecreasePlasticComponents ((uint) (totalNumberOfPlasticSons - pl));
				//w.IncreaseDispPlasticComponents ((uint)(totalNumberOfPlasticSons - pl));

			
			if (totalNumberOfElecSons - el != 0)
				w.DecreaseElectronicComponents ((uint) (totalNumberOfElecSons - el));
				//w.IncreaseDispElecComponents ((uint) (totalNumberOfElecSons - el));

			if (totalNumberOfBasicSons - bs != 0)
				w.DecreaseBasicComponents ((uint) (totalNumberOfBasicSons - bs));
				//w.IncreaseDispBasicComponents ((uint)(totalNumberOfBasicSons - bs));

		}

		used = true;
		if (TaskManager.instance.getActiveTasks().Contains(this))
			TaskManager.instance.RemoveActiveTask(this);

		if (worker != null)
		{
			worker.SetCarrying(false);
			worker.typeOfWork = EGoal.Nothing;
			worker.SetOccupation(false);
			TaskManager.instance.map.DecreaseNumberOfOccupiedWorkers();
			
			worker.AssignTask(null);
			worker = null;
		}

		if (arrival.GetComponent<Workshop>() != null)
		{
			foreach(Tile t in ((Workshop) arrival.GetComponent<Workshop> ()).adjacentTiles)
			{
				t.GetTasks().Remove(this);
			}
		}

		foreach (GetTask t in tasks)
		{
			t.DestroyTask();
		}
		tasks.Clear ();
	}

	public uint GetElec()
	{
		return elec;
	}

	public uint GetPlastic()
	{
		return plastic;
	}

	public Recipe GetRecipe()
	{
		return recipe;
	}

}
