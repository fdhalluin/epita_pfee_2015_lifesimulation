﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoneyManager : MonoBehaviour
{
	// Singleton
	private static MoneyManager _instance;
	public static MoneyManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<MoneyManager>();
			}
			
			return _instance;
		}
	}

	public uint BaseMoney;
	public uint SavedMoney;
	public Text MoneyText;
	
	public uint AgentPrice = 100;
	public uint SpecialistPrice = 140;
	public uint TransportSpecPrice = 150;
	public uint PlasticPrice = 30;
	public uint ElecPrice = 30;
	public uint PackPrice = 40;
	public uint DestroyPrice = 5;
	public uint WallPrice = 0;

	public uint StringToPrice(string s)
	{
		switch (s)
		{
		case "Agent":
			return AgentPrice;
		case "PlasticSpecialist":
		case "ElecSpecialist":
		case "PackagingSpecialist":
			return SpecialistPrice;
		case "SpeedSpecialist":
			return TransportSpecPrice;
		case "Plastic":
			return PlasticPrice;
		case "Elec":
			return ElecPrice;
		case "Pack":
			return PackPrice;
		case "Destroy":
			return DestroyPrice;
		case "Wall":
			return WallPrice;
		default:
			return 0;
		}
	}

	private uint currentMoney;

	void Awake()
	{
		currentMoney = BaseMoney;
		SavedMoney = BaseMoney;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.F12))
			IncreaseMoney(500);
	}

	public uint GetMoney()
	{
		return currentMoney;
	}

	public void SetMoney(uint amount)
	{
		currentMoney = amount;
		MoneyText.text = currentMoney.ToString() + " $";
	}

	public void IncreaseMoney(uint amount)
	{
		currentMoney += amount;
		MoneyText.text = currentMoney.ToString() + " $";
	}

	public bool DecreaseMoneyIFP(uint amount)
	{
		if (amount > currentMoney)
			return false;

		currentMoney -= amount;
		MoneyText.text = currentMoney.ToString() + " $";

		return true;
	}
}
