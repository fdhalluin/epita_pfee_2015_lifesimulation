using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EType
{
	Plastic,     // 0
	Electronic,  // 1
	Packaging,   // 2
}

public class Workshop : MonoBehaviour {
	
	public Map map;
	public EType type;
	public GameObject ComponentPrefab;
	public GameObject SelectionCanvas;

	public float BaseTimeToMakeComponent = 10f;

    private int totalCrafted = 0;
	private int craftedItems = 0;
	private uint dispCraftedItems = 0;

	public List<Tile> adjacentTiles;
	
	private Agent agentWorking;
	private float currentProduction; // Time until the production is over

	private List<GameObject> basicComponentsGO;
	private List<GameObject> plasticComponentsGO;
	private List<GameObject> electronicComponentsGO;
	private uint basicComponents = 0;
	private uint electronicEntryComponents = 0;
	private uint plasticEntryComponents = 0;

	private uint dispBasicComponents = 0;
	private uint dispElectronicEntryComponents = 0;
	private uint dispPlasticEntryComponents = 0;

	private Vector3[] basicCompPositions =
	{
		new Vector3(-0.3f,0,0.3f),
		new Vector3(0,0,0.3f),
		new Vector3(-0.3f,0,0),
		new Vector3(0,0,0),
		new Vector3(-0.3f,0,-0.3f),
		new Vector3(0,0,-0.3f)
	};

	private Vector3[] plasticCompPositions =
	{
		new Vector3(-0.3f,0,0.3f),
		new Vector3(0,0,0.3f),
		new Vector3(-0.3f,0,0)
	};

	private Vector3[] electronicCompPositions =
	{
		new Vector3(0,0,0),
		new Vector3(-0.3f,0,-0.3f),
		new Vector3(0,0,-0.3f)
	};

	private Vector3[] craftedCompPositions =
	{
		new Vector3(0.3f,0,0.3f),
		new Vector3(0.3f,0,0),
		new Vector3(0.3f,0,-0.3f)
	};

	void Start()
	{
		basicComponentsGO = new List<GameObject>();
		plasticComponentsGO = new List<GameObject>();
		electronicComponentsGO = new List<GameObject>();

		currentProduction = BaseTimeToMakeComponent;
	}

    public float efficiency()
    {
        float total = Counter.workshopsTasks[type];
        if (total == 0)
            return 0;
        else
            return totalCrafted / total;
    }

	public void IncreaseBasicComponents()
	{
		basicComponents++;
		GameObject component = (GameObject)Instantiate(ComponentPrefab, Vector3.zero, Quaternion.identity);
		component.transform.parent = transform;

		component.GetComponent<ComponentCube>().SetComponentType(EComponent.Basic);
		int compNumber = basicComponentsGO.Count;
		Vector3 pos = basicCompPositions[compNumber % basicCompPositions.Length];
		component.transform.localPosition = new Vector3(pos.x, 0.6f + ((compNumber / basicCompPositions.Length) * 0.2f), pos.z);
		component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));

		basicComponentsGO.Add(component);
	}

	public void DecreaseBasicComponents(uint number)
	{
		if (basicComponents < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of basic entry components where it is already 0");
		basicComponents -= number;
		for (int i = 0; i < number; ++i)
		{
			GameObject go = basicComponentsGO[basicComponentsGO.Count - 1];
			basicComponentsGO.Remove(go);
			Destroy (go);
		}
	}

	public void IncreaseElectronicComponents()
	{
		electronicEntryComponents++;
		
		GameObject component = (GameObject)Instantiate(ComponentPrefab, Vector3.zero, Quaternion.identity);
		component.transform.parent = transform;

		int compNumber = electronicComponentsGO.Count;
		component.GetComponent<ComponentCube>().SetComponentType(EComponent.Electronic);
		Vector3 pos = electronicCompPositions[compNumber % electronicCompPositions.Length];
		component.transform.localPosition = new Vector3(pos.x, 0.6f + ((compNumber / electronicCompPositions.Length) * 0.2f), pos.z);
		component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));
		electronicComponentsGO.Add(component);
	}

	public void DecreaseElectronicComponents(uint number)
	{
		if (electronicEntryComponents < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of electronic entry components where it is already 0");
		electronicEntryComponents -= number;
		for (int i = 0; i < number; ++i)
		{
			GameObject go = electronicComponentsGO[electronicComponentsGO.Count - 1];
			electronicComponentsGO.Remove(go);
			Destroy (go);
		}
	}
	
	public void IncreasePlasticComponents()
	{
		plasticEntryComponents++;
		
		GameObject component = (GameObject)Instantiate(ComponentPrefab, Vector3.zero, Quaternion.identity);
		component.transform.parent = transform;
		component.GetComponent<ComponentCube>().SetComponentType(EComponent.Plastic);

		int compNumber = plasticComponentsGO.Count;
		Vector3 pos = plasticCompPositions[compNumber % plasticCompPositions.Length];
		component.transform.localPosition = new Vector3(pos.x, 0.6f + ((compNumber / plasticCompPositions.Length) * 0.2f), pos.z);
		component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));
		plasticComponentsGO.Add(component);
	}

	public void IncreaseCraftedItems()
	{
		++craftedItems;
        ++totalCrafted;
        Counter.incrementWorkshops(type);

		GameObject component;
		int compNumber;
		Vector3 pos;

		switch (type)
		{
		case EType.Plastic:

			component = (GameObject)Instantiate(ComponentPrefab, Vector3.zero, Quaternion.identity);
			component.transform.parent = transform;
			component.GetComponent<ComponentCube>().SetComponentType(EComponent.Plastic);
			
			compNumber = plasticComponentsGO.Count;
			pos = craftedCompPositions[compNumber % craftedCompPositions.Length];
			component.transform.localPosition = new Vector3(pos.x, 0.6f + ((compNumber / craftedCompPositions.Length) * 0.2f), pos.z);
			component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));
			plasticComponentsGO.Add(component);

			break;

		case EType.Electronic:

			component = (GameObject)Instantiate(ComponentPrefab, Vector3.zero, Quaternion.identity);
			component.transform.parent = transform;
			component.GetComponent<ComponentCube>().SetComponentType(EComponent.Electronic);
			
			compNumber = electronicComponentsGO.Count;
			pos = craftedCompPositions[compNumber % craftedCompPositions.Length];
			component.transform.localPosition = new Vector3(pos.x, 0.6f + ((compNumber / craftedCompPositions.Length) * 0.2f), pos.z);
			component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));
			electronicComponentsGO.Add(component);

			break;

		case EType.Packaging:

			component = (GameObject)Instantiate(ComponentPrefab, Vector3.zero, Quaternion.identity);
			component.transform.parent = transform;
			component.GetComponent<ComponentCube>().SetComponentType(EComponent.Packaging);
			
			compNumber = basicComponentsGO.Count;
			pos = craftedCompPositions[compNumber % craftedCompPositions.Length];
			component.transform.localPosition = new Vector3(pos.x, 0.6f + ((compNumber / craftedCompPositions.Length) * 0.2f), pos.z);
			component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));
			basicComponentsGO.Add(component);

			break;
		}
	}

	
	public void ReduceCraftedItems()
	{
		if (craftedItems == 0)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of crafted components where it is already 0");
		craftedItems--;

		GameObject go;
		switch (type)
		{
		case EType.Plastic:
			go = plasticComponentsGO[plasticComponentsGO.Count - 1];
			plasticComponentsGO.Remove(go);
			Destroy(go);
			break;
		case EType.Electronic:
			go = electronicComponentsGO[electronicComponentsGO.Count - 1];
			electronicComponentsGO.Remove(go);
			Destroy(go);
			break;
		case EType.Packaging:
			go = basicComponentsGO[basicComponentsGO.Count - 1];
			basicComponentsGO.Remove(go);
			Destroy(go);
			break;
		}
	}
	
	public void DecreaseDispCraftedItems(uint number)
	{
		if (dispCraftedItems < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of basic disp entry components where it is already 0");
		dispCraftedItems -= number;
	}

	public void IncreaseDispCraftedItems(uint number)
	{
		dispCraftedItems += number;
	}
		

	public void DecreasePlasticComponents(uint number)
	{
		if (plasticEntryComponents < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of plastic entry components where it is already 0");
		plasticEntryComponents -= number;
		for (int i = 0; i < number; ++i)
		{
			GameObject go = plasticComponentsGO[plasticComponentsGO.Count - 1];
			plasticComponentsGO.Remove(go);
			Destroy (go);
		}
	}

	public Tile GetClosestAdjacentTile(Vector3 pos)
	{
		if (adjacentTiles.Count == 0)
			return null;

		Tile res = null;
		float minDist = float.MaxValue;

		foreach (Tile tile in adjacentTiles)
		{
			float dist = Vector3.Distance(pos, tile.transform.position);
			if (dist < minDist)
			{
				minDist = dist;
				res = tile;
			}
		}

		return res;
	}
	
	public void SetAgentWorking(Agent a)
	{
		agentWorking = a;

		if (agentWorking != null)
		{
			agentWorking.transform.LookAt(this.transform);
		}
	}

	public void Select()
	{
		SelectionCanvas.SetActive(true);
	}

	public void Unselect()
	{
		SelectionCanvas.SetActive(false);
	}



	public void DecreaseDispBasicComponents(uint number)
	{
		if (dispBasicComponents < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of basic disp entry components where it is already 0");
		dispBasicComponents -= number;
	}

	public void DecreaseDispPlasticComponents(uint number)
	{
		if (dispPlasticEntryComponents < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of plastic disp entry components where it is already 0");
		dispPlasticEntryComponents -= number;
	}

	public void DecreaseDispElectronicComponents(uint number)
	{
		if (dispElectronicEntryComponents < number)
			Debug.LogError ("[WORKSHOP] Tried to decrease number of electronic disp entry components where it is already 0");
		dispElectronicEntryComponents -= number;
	}

	public void IncreaseDispBasicComponents(uint number)
	{
		dispBasicComponents += number;
	}

	public void IncreaseDispPlasticComponents(uint number)
	{
		dispPlasticEntryComponents += number;
	}

	public void IncreaseDispElecComponents(uint number)
	{
		dispElectronicEntryComponents += number;
	}

	public Agent AgentWorking()
	{
		return agentWorking;
	}

	void FixedUpdate ()
	{
		if (basicComponents == 0 && electronicEntryComponents == 0 && plasticEntryComponents == 0 && agentWorking != null)
		{
			agentWorking.typeOfWork = EGoal.Nothing;
			agentWorking = null;
			return;
		}

		if (agentWorking != null)
		{
			// We want : Skill 1 = 10 seconds; Skill 6 = 5 seconds; Skill 10 = 1 second
			// So we decrease by ((10 / (10 - Skill + 1)) * time)
			if (type == EType.Electronic)
				currentProduction -= (10 / (11 - agentWorking.electronics)) * Time.fixedDeltaTime;
			else if (type == EType.Plastic)
				currentProduction -= (10 / (11 - agentWorking.plastics)) * Time.fixedDeltaTime;
			else if (type == EType.Packaging)
				currentProduction -= (10 / (11 - agentWorking.packaging)) * Time.fixedDeltaTime;
			else
				Debug.LogError("[Workshop] Step : type is not set");
			
			agentWorking.SetProgressBarVisibility(true);
			agentWorking.SetProgressBarValue((1000 - (currentProduction * 100)) / 1000);
			
			if (currentProduction <= 0)
			{
				currentProduction = BaseTimeToMakeComponent;

				IncreaseCraftedItems();

				if (type == EType.Packaging)
				{
					if (TaskManager.instance.IAT != IAType.Rando)
					{
						BuildTask bt = (BuildTask) agentWorking.GetAssignedTask();
						DecreasePlasticComponents(bt.GetPlastic());
						DecreaseElectronicComponents(bt.GetElec());
					}
					else
					{
						DecreasePlasticComponents((uint) TaskManager.instance.currentProd.Current.plastic);
						DecreaseElectronicComponents((uint) TaskManager.instance.currentProd.Current.electronics);
					}
				}
				else
					DecreaseBasicComponents(1);
				Agent tempAgent = agentWorking;
				agentWorking = null;
				if (TaskManager.instance.IAT == IAType.Rando)
					map.AgentUpdate(tempAgent);
				else
					TaskManager.instance.Ping(tempAgent);
			}
		}
	}

	public bool ReadyForWork(BuildTask task)
	{
		if (type == EType.Packaging)
			return (electronicEntryComponents >= task.GetElec()) && (plasticEntryComponents >= task.GetPlastic());
		return basicComponents >= 1;
	}

	public bool ReadyForWorkForBasicIA()
	{
		if (type == EType.Packaging)
			return (electronicEntryComponents >= TaskManager.instance.currentProd.Current.electronics) && (plasticEntryComponents >= TaskManager.instance.currentProd.Current.plastic);
		return basicComponents >= 1;
	}

	/*public bool DisponibilityReadyForWork()
	{
		if (type == EType.Packaging)
			return (dispelectronicEntryComponents >= 2) && (dispplasticEntryComponents >= 2);
		return dispbasicComponents >= 1;
	}*/

	public float CurrentProduction()
	{
		return currentProduction;
	}

	public uint BasicComponents()
	{
		return basicComponents;
	}

	public uint DispBasicComponents ()
	{
		return dispBasicComponents;
	}

	public uint PlasticComponents()
	{
		return plasticEntryComponents;
	}

	public uint DispPlasticComponents()
	{
		return dispPlasticEntryComponents;
	}

	public uint ElectronicComponents()
	{
		return electronicEntryComponents;
	}

	public uint DispElectronicComponents()
	{
		return dispElectronicEntryComponents;
	}

	public int CraftedItems()
	{
		return craftedItems;
	}

	public uint DispCraftedItems()
	{
		return dispCraftedItems;
	}
}