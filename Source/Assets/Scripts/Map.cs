﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;


public enum ESelection
{
	Empty,         // 0
	Wall,          // 1
	Worker,        // 2
	WSPlastics,    // 3
	WSElectronics, // 4
	WSPackaging,   // 5
	DeliveryZone,  // 6
	FinishZone,    // 7
	SpeedSpecialist, // 8
	PlasticSpecialist, // 9
	ElecSpecialist, // 10
	PackSpecialist, // 11
}

public enum EComponent
{
	Basic,       // 0
	Plastic,     // 1
	Electronic,  // 2
	Packaging,   // 3
}

public class Map : MonoBehaviour
{
	public GameObject emptyTile;
	public GameObject wallTile;
	public GameObject tilesContainer;
	
	public GameObject AgentGO;
	public GameObject WorkshopPlastics;
	public GameObject WorkshopElectronics;
	public GameObject WorkshopPackaging;
	public GameObject DeliveryZone;
	public GameObject FinishZone;
	public GameObject TerrainGO;
	public GameObject PackageGO;

	private uint mapSize = 20;
	private bool shouldRestartSolver = false;
	private bool shouldUpdateRandom = false;
	private uint DeliveredProducts = 0;
	private uint numberOfOccupiedWorkers = 0;
	private Tile FinishTile;
	private Tile DeliveryTile;

	private Tile[,] tiles;
	private ESelection[,] savedTiles;
	private uint savedMoney;
	private ESelection selectedType; // Selected in the interface

	private List<Agent> Workers;
	private List<Workshop> Workshops;
	private List<KeyValuePair<string, int[]>> SavedWorkers; // x, y, speed, pla, ele, pack

	private List<GameObject> PackagesGOs;

	void Awake ()
	{
		Workers = new List<Agent>();
		SavedWorkers = new List<KeyValuePair<string, int[]>>();
		Workshops = new List<Workshop>();
		PackagesGOs = new List<GameObject>();
		selectedType = ESelection.Empty;
		
		savedTiles = new ESelection[mapSize,mapSize];

	}

	void Start()
	{
	}

	public void InitMap(bool hard)
	{
		
		UIManager.instance.SetConstructionMode(true);
		UIManager.instance.SetInitialConstruction(true);
		
		GenerateMap(mapSize);

		if (hard)
			MakeBasicHardMap();
		else
			MakeBasicNormalMap();
		
		MoneyManager.instance.SetMoney(MoneyManager.instance.BaseMoney);
		savedMoney = MoneyManager.instance.BaseMoney;

		TimeManager.instance.ResetTime();
	}

	void Update ()
	{
		if (! UIManager.instance.ConstructionMode() && shouldRestartSolver && TaskManager.instance.IAT == IAType.Advanced)
		{
			shouldRestartSolver = false;
			TaskManager.instance.UpdateWithAI();
		}
		else if (! UIManager.instance.ConstructionMode() && shouldUpdateRandom && TaskManager.instance.IAT == IAType.Rando)
		{
			foreach (Agent a in Workers)
				AgentUpdate(a);
		}
	}

	private Vector3[] FinishedProductsPositions =
	{
		new Vector3(277, 0.65f, 266),
		new Vector3(275, 0.65f, 266),
		new Vector3(273, 0.65f, 266),
		new Vector3(277, 0.65f, 264),
		new Vector3(275, 0.65f, 264),
		new Vector3(273, 0.65f, 264),
		new Vector3(277, 0.65f, 262),
		new Vector3(275, 0.65f, 262),
		new Vector3(273, 0.65f, 262),
		new Vector3(277, 0.65f, 260),
		new Vector3(275, 0.65f, 260),
		new Vector3(273, 0.65f, 260),
		new Vector3(277, 0.65f, 258),
		new Vector3(275, 0.65f, 258),
		new Vector3(273, 0.65f, 258),
		new Vector3(277, 0.65f, 256),
		new Vector3(275, 0.65f, 256),
		new Vector3(273, 0.65f, 256),
		new Vector3(277, 0.65f, 254),
		new Vector3(275, 0.65f, 254),
		new Vector3(273, 0.65f, 254),
		new Vector3(277, 0.65f, 252),
		new Vector3(275, 0.65f, 252),
		new Vector3(273, 0.65f, 252),
		new Vector3(277, 0.65f, 250),
		new Vector3(275, 0.65f, 250),
		new Vector3(273, 0.65f, 250),
	};
	
	private Tile InstantiateTile(GameObject prefab, uint x, uint y)
	{
		Tile newTile = ((GameObject)Instantiate(prefab, new Vector3(x, 0, y), Quaternion.identity)).GetComponent<Tile>();


		newTile.X = x;
		newTile.Y = y;
		newTile.transform.parent = tilesContainer.transform;
		newTile.SetMap(this);

		tiles[x,y] = newTile;

		return newTile;
	}

	private Agent InstantiateAgent(uint x, uint y)
	{
		shouldRestartSolver = true;

		// Only one pawn per tile
		foreach(Agent p in Workers)
		{
			if (p.X == x && p.Y == y)
			{
				Workers.Remove(p);

				if (p.Occupied())
				{
					Task task = (Task) p.GetAssignedTask();
					bool b = p.Carrying();

					DecreaseNumberOfOccupiedWorkers();
					task.SetWorker(null);
					p.AssignTask(null);
					p.typeOfWork = EGoal.Nothing;
					p.SetOccupation(false);
					p.SetCarrying(false);
					
					if (b)
					{
						GetTask gtask = (GetTask) task;
						gtask.DestroyTask();
						if (task.GetFather() != null)
						{
							gtask.TaskNotDone();
							gtask.ParseTile();
						}
					}
				}

				if (UIManager.instance.InitialConstructionMode())
				{
					foreach (KeyValuePair<string, int[]> pair in SavedWorkers)
					{
						if (pair.Value[0] == (int)x && pair.Value[1] == (int)y)
						{
							SavedWorkers.Remove(pair);
							break;
						}
					}
				}
                EmployeesView.GetInstance().UnregisterAgent(p);
				Destroy(p.gameObject);
				return null;
			}
		}
		
		Agent newPawn = ((GameObject)Instantiate(AgentGO, new Vector3(x, 0, y), Quaternion.identity)).GetComponent<Agent>();
		newPawn.map = this;

		newPawn.transform.parent = transform;

		newPawn.GetNavMeshAgent().updatePosition = true;
		newPawn.GetNavMeshAgent().updateRotation = true;

		Workers.Add(newPawn);

		if (TaskManager.instance.IAT == IAType.Rando)
			AgentUpdate(newPawn);

		return newPawn;
	}


	public void GenerateMap(uint size)
	{
		Workers = new List<Agent>();
		Workshops = new List<Workshop>();

		mapSize = size;
		tiles = new Tile[size,size];

		for (uint x = 0; x < size; ++x)
		{
			for (uint y = 0; y < size; ++y)
			{
				InstantiateTile(emptyTile, x, y);
				if (UIManager.instance.InitialConstructionMode())
					savedTiles[x,y] = ESelection.Empty;
			}
		}
	}

	public void RegenerateMap()
	{
		if (UIManager.instance.InitialConstructionMode())
			return;

		// Deleting everything
		DeliveredProducts = 0;
		DeliveryTile = null;
		FinishTile = null;
		TimeManager.instance.ResetTime();
		TaskManager.instance.DestroyAllTasks();

		numberOfOccupiedWorkers = 0;
		foreach (Agent a in Workers)
		{
			EmployeesView.GetInstance().UnregisterAgent(a);
			Destroy(a.gameObject);
		}
		Workers.Clear();

		foreach (Tile t in tiles)
			DestroyTile(t);
		Workshops.Clear();

		foreach (GameObject go in PackagesGOs)
			Destroy(go);
		PackagesGOs.Clear();

		MoneyManager.instance.SetMoney(MoneyManager.instance.BaseMoney);

		// Regenerating the saved map
		GenerateMap(mapSize);
		
		UIManager.instance.SetConstructionMode(true);
		for (int i = 0; i < mapSize; ++i)
		{
			for (int j = 0; j < mapSize; ++j)
			{
				ESelection t = savedTiles[i,j];
				selectedType = t;
				OnTileClicked(tiles[i,j]);
			}
		}

		foreach (KeyValuePair<string, int[]> statsPair in SavedWorkers)
		{
			int[] stats = statsPair.Value;
			Agent a = InstantiateAgent((uint)stats[0], (uint)stats[1]);
			if (a == null)
				continue;

			a.InitWithStats(stats[2], stats[3], stats[4], stats[5], statsPair.Key);
			
			EmployeesView.GetInstance().RegisterAgent(a);
		}

		MoneyManager.instance.SetMoney(savedMoney);

		TaskManager.instance.ReinitializeManager();

		UIManager.instance.SetInitialConstruction(true);
	}

	public void OnLaunch()
	{
		savedMoney = MoneyManager.instance.GetMoney();
		MoneyManager.instance.SavedMoney = savedMoney;
	}

	public void AgentUpdate(Agent agent)
	{
		uint X = agent.X;
		uint Y = agent.Y;

		//IS AGENT IN EXIT TILE?
		if (FinishTile.X == X && FinishTile.Y == Y && agent.Carrying() && agent.componentCarrying == EComponent.Packaging)
		{
			agent.SetCarrying(false);
			AddDeliveredProduct(TaskManager.instance.currentProd.Current);

			// Debug.Log("Finished products : " + DeliveredProducts + " Step : " + TimeManager.instance.GetStepNumber());
            // Update text display
            TextMesh tM = FinishTile.GetComponentInChildren<TextMesh>();
            tM.text = DeliveredProducts.ToString();
		}

		//IS AGENT IN ENTRY TILE?
		if (DeliveryTile.X == X && DeliveryTile.Y == Y && !agent.Carrying())
		{
			agent.componentCarrying = EComponent.Basic;
			agent.SetCarrying(true);
		}

		//WORKSHOP TIME
		foreach(Workshop workshop in Workshops)
		{
			foreach(Tile tile in workshop.adjacentTiles)
			{
				if (tile.X == X && tile.Y == Y)
				{
					if (agent.Carrying())
					{
						if (agent.componentCarrying == EComponent.Basic
						    && (workshop.type == EType.Electronic || workshop.type == EType.Plastic))
						{
							agent.SetCarrying(false);
							workshop.IncreaseBasicComponents();
						}
						else if (workshop.type == EType.Packaging
						         && (agent.componentCarrying == EComponent.Electronic || agent.componentCarrying == EComponent.Plastic))
						{
							agent.SetCarrying(false);
							if (agent.componentCarrying == EComponent.Electronic)
								workshop.IncreaseElectronicComponents();
							else
								workshop.IncreasePlasticComponents();
						}
					}

					if (workshop.CraftedItems() > 0 && !agent.Carrying())
					{
						//PICK UP ITEMS
						workshop.ReduceCraftedItems();
						if (workshop.type == EType.Electronic)
							agent.componentCarrying = EComponent.Electronic;
						else if (workshop.type == EType.Plastic)
							agent.componentCarrying = EComponent.Plastic;
						else if (workshop.type == EType.Packaging)
							agent.componentCarrying = EComponent.Packaging;
						
						agent.SetCarrying(true);
					}
					if (workshop.AgentWorking() == null)
					{
						if (workshop.type == EType.Packaging && workshop.ReadyForWorkForBasicIA())
						{
							//WORK AT PACKAGING FOR PHONES
							workshop.SetAgentWorking(agent);
							return;
						}
						else if (workshop.ReadyForWorkForBasicIA())
						{
							//WORK AT OTHERS
							workshop.SetAgentWorking(agent);
							return;
						}
					}
				}
			}
		}

		//GO PICK BASIC COMPONENTS
		if (!agent.Carrying()) {
			agent.Move (DeliveryTile);
			return;
		}
		//OR GO TO EXIT STATE, GIVES PHONES
		if (agent.Carrying() && agent.componentCarrying == EComponent.Packaging) {
			agent.Move (FinishTile);
			return;
		}
		
		//GO TO WORKSHOP IF YOU CAN
		if (agent.Carrying())
		{
			bool moving = false;
			while (!moving)
			{
				int res = Random.Range (0, Workshops.Count);

				Workshop w = Workshops [res];

				if (agent.componentCarrying == EComponent.Basic && w.type == EType.Packaging)
					continue;
				if ((agent.componentCarrying == EComponent.Electronic || agent.componentCarrying == EComponent.Plastic)
				    && w.type != EType.Packaging)
					continue;

				if (w.adjacentTiles.Count != 0)
				{
					Tile t = w.GetClosestAdjacentTile(agent.transform.position);
					agent.Move(t);
					moving = true;
				}
			}
		}
	}

	public void AgentUpdate2(Agent agent)
	{
		uint X = agent.X;
		uint Y = agent.Y;

		if (agent.GetAssignedTask() == null)
			return;

		if (agent.GetAssignedTask () is BuildTask)
		{
			BuildTask btt = (BuildTask) agent.GetAssignedTask ();
			if (btt.getTasksList().Count != 0)
			{
				agent.SetCarrying(false);
				agent.typeOfWork = EGoal.Nothing;
				agent.SetOccupation(false);
				btt.SetWorker(null);
				agent.AssignTask(null);
				agent.Goal = null;
				agent.Goal2 = null;
				agent.GetNavMeshAgent ().ResetPath ();
				return;
			}
		}
		else
		{
			GetTask gtt = (GetTask) agent.GetAssignedTask ();
			if (gtt.getWaitTask() != null)
			{
				agent.SetCarrying(false);
				agent.typeOfWork = EGoal.Nothing;
				agent.SetOccupation(false);
				gtt.SetWorker(null);
				agent.AssignTask(null);
				agent.Goal = null;
				agent.Goal2 = null;
				agent.GetNavMeshAgent ().ResetPath ();
				return;
			}
		}

		//IS AGENT IN EXIT TILE?
		if (FinishTile.X == X && FinishTile.Y == Y && agent.Carrying() && agent.componentCarrying == EComponent.Packaging
		    && agent.typeOfWork == EGoal.Carrying && agent.Goal2 == null
		    && agent.GetAssignedTask().GetArrival().type == ESelection.FinishZone)
		{
			AddDeliveredProduct(((GetTask)agent.GetAssignedTask()).GetRecipe());

			// Update text display
			TextMesh tM = FinishTile.GetComponentInChildren<TextMesh>();
			tM.text = DeliveredProducts.ToString();;
			TaskManager.instance.Ping(agent);
			return;
		}
		
		//IS AGENT IN ENTRY TILE?
		if (DeliveryTile.X == X && DeliveryTile.Y == Y && !agent.Carrying()
		    && agent.typeOfWork == EGoal.Carrying && agent.Goal2 != null
		    && ((GetTask)agent.GetAssignedTask()).GetDepartTile().type == ESelection.DeliveryZone)
		{

			agent.componentCarrying = EComponent.Basic;
			agent.SetCarrying(true);
			agent.Goal = agent.Goal2;
			agent.Goal2 = null;
			agent.Move(agent.Goal);

			return;
		}
		
		Workshop workshop;
		if (agent.Goal2 != null)
			workshop = ((GetTask) agent.GetAssignedTask ()).GetDepartTile ().GetComponent<Workshop> ();
		else
			workshop = agent.GetAssignedTask ().GetArrival ().GetComponent<Workshop> ();

		if (workshop == null || workshop.adjacentTiles == null) 
		{
			if (agent.Carrying ())
			{
				agent.GetNavMeshAgent().ResetPath();
				agent.Move (agent.Goal);
				return;
			}
			DecreaseNumberOfOccupiedWorkers();
			agent.SetCarrying(false);
			agent.typeOfWork = EGoal.Nothing;
			agent.SetOccupation(false);
			agent.GetAssignedTask().SetWorker(null);
			agent.AssignTask(null);

			agent.GetNavMeshAgent().ResetPath();
			agent.Goal = null;
			agent.Goal2 = null;
			if (TaskManager.instance.IAT == IAType.Advanced && TaskManager.instance.BasicTasksInitialized())
				TaskManager.instance.UpdateWithAI();
			//agent.Move(DeliveryTile);
			return;
		}

		bool tileFound = false;
		foreach (Tile tile in workshop.adjacentTiles)
		{
			if (tile.X == X && tile.Y == Y)
			{
				tileFound = true;
				break;
			}
		}
		
		if (!tileFound)
		{
			//print("ResetAgent");
			if (agent.Goal == null)
			{
				if (agent.GetAssignedTask() is GetTask)
					Debug.LogError("[MAP] AgentUpdate2: Assigned task is a GetTask and Goal is null");
				agent.SetCarrying(false);
				agent.typeOfWork = EGoal.Nothing;
				agent.SetOccupation(false);
				agent.GetAssignedTask().SetWorker(null);
				workshop.SetAgentWorking(null);
				agent.AssignTask(null);
				return;
			}
			agent.GetNavMeshAgent().ResetPath();
			if (agent.GetAssignedTask () is BuildTask)
				agent.Move(agent.Goal);
			else
				agent.DoubleMove(agent.Goal, agent.Goal2);
			return;
		}

		
		if (agent.Carrying() && agent.typeOfWork == EGoal.Carrying && agent.Goal2 == null)
		{
			if (agent.componentCarrying == EComponent.Basic
			    && (workshop.type == EType.Electronic || workshop.type == EType.Plastic))
			{
				workshop.IncreaseBasicComponents();
				//--numberOfOccupiedWorkers;
				TaskManager.instance.Ping(agent);
			}
			else if (workshop.type == EType.Packaging
			         && (agent.componentCarrying == EComponent.Electronic || agent.componentCarrying == EComponent.Plastic))
			{
				if (agent.componentCarrying == EComponent.Electronic)
					workshop.IncreaseElectronicComponents();
				else
					workshop.IncreasePlasticComponents();
				//--numberOfOccupiedWorkers;
				TaskManager.instance.Ping(agent);
			}
			return;
		}
		else if (!agent.Carrying() && agent.typeOfWork == EGoal.Carrying && agent.Goal2 != null)
		{
			if (workshop.CraftedItems() > 0)
			{

				workshop.ReduceCraftedItems();
				if (workshop.type == EType.Electronic)
					agent.componentCarrying = EComponent.Electronic;
				else if (workshop.type == EType.Plastic)
					agent.componentCarrying = EComponent.Plastic;
				else if (workshop.type == EType.Packaging)
					agent.componentCarrying = EComponent.Packaging;
				
				agent.SetCarrying(true);
				agent.Goal = agent.Goal2;
				agent.Goal2 = null;
				agent.Move(agent.Goal);
			}
			return;
		}
		else if ((agent.typeOfWork == EGoal.Build || agent.typeOfWork == EGoal.WaitingToBuild) && agent.Goal2 == null)
		{
			if (workshop.ReadyForWork(((BuildTask) agent.GetAssignedTask())))
			{
				if (workshop.AgentWorking() == null)
				{
					//WORK
					agent.Goal = null;
					agent.typeOfWork = EGoal.Build;
					agent.GetNavMeshAgent ().ResetPath ();
					workshop.SetAgentWorking(agent);
				}
				else
					agent.typeOfWork = EGoal.WaitingToBuild;
			}
			return;
		}
		else
			print("TILE Problem");
		

		print("Loop ended: it's problematic: " + X + " " + Y);
		if (agent.Goal)
			print("GOAL: " + agent.Goal.X + " " + agent.Goal.Y);
		foreach(Tile tile in workshop.adjacentTiles)
		{
			print(tile.X + " " + tile.Y);
		}
	}

	private void AddDeliveredProduct(Recipe r)
	{
		DeliveredProducts++;

		MoneyManager.instance.IncreaseMoney(r.money);
		foreach (Recipe rec in Recipe.recipies)
		{
			if (rec.name == r.name)
			{
				rec.producted += 1;
				break;
			}
		}

		GameObject component = (GameObject)Instantiate(PackageGO, Vector3.zero, Quaternion.identity);
		component.transform.parent = TerrainGO.transform;

		int compNumber = PackagesGOs.Count;
		Vector3 pos = FinishedProductsPositions[compNumber % FinishedProductsPositions.Length];
		component.transform.localPosition = new Vector3(pos.x, 0.65f + ((compNumber / FinishedProductsPositions.Length) * 0.94f), pos.z);
		component.transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f));

		PackagesGOs.Add(component);
	}

	// Hard-coded map
	private void MakeBasicHardMap()
	{
		UIManager.instance.SetConstructionMode(true);
		
		selectedType = ESelection.Wall;
		for (uint x = 0; x < mapSize; x++)
		{
			for (uint y = 0; y < mapSize; y++)
			{
				OnTileClicked(tiles[x, y]);
			}
		}
		
		// Workshops
		selectedType = ESelection.WSPlastics;
		OnTileClicked(tiles[6,2]);
		
		selectedType = ESelection.WSElectronics;
		OnTileClicked(tiles[14,6]);
		
		selectedType = ESelection.WSPackaging;
		OnTileClicked(tiles[11,13]);
		
		selectedType = ESelection.DeliveryZone;
		OnTileClicked(tiles[1,10]);
		
		selectedType = ESelection.FinishZone;
		OnTileClicked(tiles[18,10]);
		
		selectedType = ESelection.Worker;
		OnTileClicked(tiles[3,13]);
		OnTileClicked(tiles[3,13]);
		
		selectedType = ESelection.Empty;

		for (uint x = 2; x < 18; ++x)
			OnTileClicked(tiles[x,10]);
		OnTileClicked(tiles[3,12]);
		OnTileClicked(tiles[3,11]);
		OnTileClicked(tiles[3,10]);

		for (uint y = 9; y > 2; --y)
			OnTileClicked(tiles[6,y]);

		OnTileClicked(tiles[11,11]);
		OnTileClicked(tiles[11,12]);

		OnTileClicked(tiles[14,9]);
		OnTileClicked(tiles[14,8]);
		OnTileClicked(tiles[14,7]);
	}

	// Hard-coded map
	private void MakeBasicNormalMap()
	{
		UIManager.instance.SetConstructionMode(true);

		selectedType = ESelection.Wall;
		for (uint x = 0; x < mapSize; x++)
		{
			OnTileClicked(tiles[x, 0]);
			OnTileClicked(tiles[x, mapSize - 1]);
		}

		for (uint y = 1; y < mapSize - 1; y++)
		{
			OnTileClicked(tiles[0, y]);
			OnTileClicked(tiles[mapSize - 1, y]);
		}
		OnTileClicked(tiles[1,8]);
		OnTileClicked(tiles[2,8]);
		OnTileClicked(tiles[3,8]);
		OnTileClicked(tiles[1,12]);
		OnTileClicked(tiles[2,12]);
		OnTileClicked(tiles[3,12]);

		OnTileClicked(tiles[7,8]);
		OnTileClicked(tiles[7,9]);
		OnTileClicked(tiles[7,10]);
		OnTileClicked(tiles[7,11]);
		OnTileClicked(tiles[7,12]);
		OnTileClicked(tiles[7,13]);
		OnTileClicked(tiles[7,14]);
		OnTileClicked(tiles[7,15]);
		OnTileClicked(tiles[7,16]);
		OnTileClicked(tiles[7,17]);
		OnTileClicked(tiles[7,18]);

		OnTileClicked(tiles[8,12]);
		OnTileClicked(tiles[11,12]);
		OnTileClicked(tiles[12,12]);
		OnTileClicked(tiles[13,12]);
		OnTileClicked(tiles[14,12]);
		OnTileClicked(tiles[15,12]);
		OnTileClicked(tiles[18,12]);

		OnTileClicked(tiles[8,8]);
		OnTileClicked(tiles[9,8]);
		OnTileClicked(tiles[10,8]);
		OnTileClicked(tiles[11,8]);
		OnTileClicked(tiles[14,8]);
		OnTileClicked(tiles[15,8]);
		OnTileClicked(tiles[16,8]);
		OnTileClicked(tiles[17,8]);
		OnTileClicked(tiles[18,8]);

		OnTileClicked(tiles[9,7]);
		OnTileClicked(tiles[9,6]);
		OnTileClicked(tiles[9,3]);
		OnTileClicked(tiles[9,2]);
		OnTileClicked(tiles[9,1]);

		OnTileClicked(tiles[15,9]);
		OnTileClicked(tiles[15,10]);
		OnTileClicked(tiles[15,11]);

		// Workshops
		selectedType = ESelection.WSPlastics;
		OnTileClicked(tiles[2,6]);
		OnTileClicked(tiles[2,2]);
		OnTileClicked(tiles[3,4]);
		OnTileClicked(tiles[6,4]);
		OnTileClicked(tiles[7,6]);
		OnTileClicked(tiles[7,2]);

		selectedType = ESelection.WSElectronics;
		OnTileClicked(tiles[11,6]);
		OnTileClicked(tiles[14,6]);
		OnTileClicked(tiles[17,6]);
		OnTileClicked(tiles[12,4]);
		OnTileClicked(tiles[16,4]);
		OnTileClicked(tiles[14,2]);

		selectedType = ESelection.WSPackaging;
		OnTileClicked(tiles[10,17]);
		OnTileClicked(tiles[16,17]);
		OnTileClicked(tiles[10,14]);
		OnTileClicked(tiles[16,14]);

		selectedType = ESelection.DeliveryZone;
		OnTileClicked(tiles[1,10]);

		selectedType = ESelection.FinishZone;
		OnTileClicked(tiles[18,10]);

		selectedType = ESelection.Worker;
		OnTileClicked(tiles[1,17]);

		selectedType = ESelection.Empty;
	}

	public void OnTileClicked(Tile tile)
	{
		if (UIManager.instance.ConstructionMode())
		{
			// Make sure there is always a delivery and finish tile
			if (tile.type == ESelection.DeliveryZone || tile.type == ESelection.FinishZone)
			{
				return;
			}

			// Make sure there is always at least one workshop of each type
			if (tile.type == ESelection.WSElectronics && GetLastWorkshopOfType(EType.Electronic) != null)
			{
				return;
			}
			if (tile.type == ESelection.WSPackaging && GetLastWorkshopOfType(EType.Packaging) != null)
			{
				return;
			}
			if (tile.type == ESelection.WSPlastics && GetLastWorkshopOfType(EType.Plastic) != null)
			{
				return;
			}

			// If we are blocking the last way to the last workshop of this kind, return
			if (! CheckAdjacencies(tile))
			{
				return;
			}

			if (tile.Walkable)
			{
				if (selectedType == ESelection.Worker)
				{
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.AgentPrice))
						return;

					Agent a = InstantiateAgent(tile.X, tile.Y);

					if (a != null)
					{
						EmployeesView.GetInstance().RegisterAgent(a);
						if (UIManager.instance.InitialConstructionMode())
							SavedWorkers.Add(new KeyValuePair<string, int[]> (a.Name, new int[]{(int)tile.X, (int)tile.Y, a.movingSpeedAttribute, a.plastics, a.electronics, a.packaging} ));
					}
					return;
				}
				if (selectedType == ESelection.SpeedSpecialist)
				{
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.TransportSpecPrice))
						return;

					Agent a = InstantiateAgent(tile.X, tile.Y);
					if (a == null)
						return;
					a.InitWithStats(Random.Range(7, 11), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), NameGenerator.GenerateName() + " " + NameGenerator.GenerateName());

					if (UIManager.instance.InitialConstructionMode())
						SavedWorkers.Add(new KeyValuePair<string, int[]> (a.Name, new int[]{(int)tile.X, (int)tile.Y, a.movingSpeedAttribute, a.plastics, a.electronics, a.packaging} ));

					EmployeesView.GetInstance().RegisterAgent(a);
					return;
				}
				if (selectedType == ESelection.PlasticSpecialist)
				{
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.SpecialistPrice))
						return;

					Agent a = InstantiateAgent(tile.X, tile.Y);
					if (a == null)
						return;

					a.InitWithStats(Random.Range(1, 7), Random.Range(7, 11), Random.Range(1, 6), Random.Range(1, 6), NameGenerator.GenerateName()+ " " + NameGenerator.GenerateName());

					if (UIManager.instance.InitialConstructionMode())
						SavedWorkers.Add(new KeyValuePair<string, int[]> (a.Name, new int[]{(int)tile.X, (int)tile.Y, a.movingSpeedAttribute, a.plastics, a.electronics, a.packaging} ));
					
					EmployeesView.GetInstance().RegisterAgent(a);
					return;
				}
				if (selectedType == ESelection.ElecSpecialist)
				{
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.SpecialistPrice))
						return;

					Agent a = InstantiateAgent(tile.X, tile.Y);
					if (a == null)
						return;

					a.InitWithStats(Random.Range(1, 7), Random.Range(1, 6), Random.Range(7, 11), Random.Range(1, 6), NameGenerator.GenerateName()+ " " + NameGenerator.GenerateName());

					if (UIManager.instance.InitialConstructionMode())
						SavedWorkers.Add(new KeyValuePair<string, int[]> (a.Name, new int[]{(int)tile.X, (int)tile.Y, a.movingSpeedAttribute, a.plastics, a.electronics, a.packaging} ));
					
					EmployeesView.GetInstance().RegisterAgent(a);
					return;
				}
				if (selectedType == ESelection.PackSpecialist)
				{
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.SpecialistPrice))
						return;

					Agent a = InstantiateAgent(tile.X, tile.Y);
					if (a == null)
						return;

					a.InitWithStats(Random.Range(1, 7), Random.Range(1, 6), Random.Range(1, 6), Random.Range(7, 11), NameGenerator.GenerateName()+ " " + NameGenerator.GenerateName());

					if (UIManager.instance.InitialConstructionMode())
						SavedWorkers.Add(new KeyValuePair<string, int[]> (a.Name, new int[]{(int)tile.X, (int)tile.Y, a.movingSpeedAttribute, a.plastics, a.electronics, a.packaging} ));
					
					EmployeesView.GetInstance().RegisterAgent(a);
					return;
				}
			}

			if (selectedType != ESelection.Empty && tile.type == selectedType)
			{
				// If the tile already contains the selected type, set it as empty
				// InstantiateTile(emptyTile, tile.X, tile.Y);
				// if (UIManager.instance.InitialConstructionMode())
				// 	savedTiles[tile.X, tile.Y] = ESelection.Empty;
				return;
			}
			else
			{
				Workshop workshop;
				switch(selectedType)
				{
				case ESelection.Wall:
					InstantiateTile(wallTile, tile.X, tile.Y);
					break;
				case ESelection.WSPlastics:
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.PlasticPrice))
						return;
					workshop = InstantiateTile(WorkshopPlastics, tile.X, tile.Y).GetComponent<Workshop>();
					workshop.type = EType.Plastic;
					workshop.map = this;
					Workshops.Add(workshop);
					TaskManager.instance.GetWorkshopNbOfActiveTasks()[workshop] = 0;
					break;
				case ESelection.WSElectronics:
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.ElecPrice))
						return;
					workshop = InstantiateTile(WorkshopElectronics, tile.X, tile.Y).GetComponent<Workshop>();
					workshop.type = EType.Electronic;
					workshop.map = this;
					Workshops.Add(workshop);
					TaskManager.instance.GetWorkshopNbOfActiveTasks()[workshop] = 0;
					break;
				case ESelection.WSPackaging:
					if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.PackPrice))
						return;
					workshop = InstantiateTile(WorkshopPackaging, tile.X, tile.Y).GetComponent<Workshop>();
					workshop.type = EType.Packaging;
					workshop.map = this;
					Workshops.Add(workshop);
					TaskManager.instance.GetWorkshopNbOfActiveTasks()[workshop] = 0;
					break;
				case ESelection.DeliveryZone:
					if (DeliveryTile != null)
					{
						InstantiateTile(emptyTile, DeliveryTile.X, DeliveryTile.Y);
						if (UIManager.instance.InitialConstructionMode())
							savedTiles[DeliveryTile.X, DeliveryTile.Y] = ESelection.Empty;
						DestroyTile(DeliveryTile);
					}
					DeliveryTile = InstantiateTile(DeliveryZone, tile.X, tile.Y);
					break;
				case ESelection.FinishZone:
					if (FinishTile != null)
					{
						InstantiateTile(emptyTile, FinishTile.X, FinishTile.Y);
						if (UIManager.instance.InitialConstructionMode())
							savedTiles[FinishTile.X, FinishTile.Y] = ESelection.Empty;
						DestroyTile(FinishTile);
					}
					FinishTile = InstantiateTile(FinishZone, tile.X, tile.Y);

					break;
				default:
					// Default on empty tile
					if (tile.type == ESelection.Wall)
					{
						if (! MoneyManager.instance.DecreaseMoneyIFP(MoneyManager.instance.DestroyPrice))
							return;
					}
					InstantiateTile(emptyTile, tile.X, tile.Y);
					break;
				}

				if (UIManager.instance.InitialConstructionMode())
					savedTiles[tile.X, tile.Y] = selectedType;
			}


			foreach (Workshop w in Workshops)
			{
				Tile current = w.GetComponent<Tile> ();
				List<Task> tempTasks = null;
				if (w.adjacentTiles.Count != 0)
					tempTasks = w.adjacentTiles[0].GetTasks();
				w.adjacentTiles.Clear ();
				w.SetAgentWorking(null);
				
				if (current.X >= 1 && tiles [current.X - 1, current.Y].Walkable)
					w.adjacentTiles.Add (tiles [current.X - 1, current.Y]);
				
				if (current.Y >= 1 && tiles [current.X, current.Y - 1].Walkable)
					w.adjacentTiles.Add (tiles [current.X, current.Y - 1]);
				
				if (current.Y + 1 < mapSize - 1 && tiles [current.X, current.Y + 1].Walkable)
					w.adjacentTiles.Add (tiles [current.X, current.Y + 1]);
				
				if (current.X + 1 < mapSize - 1 && tiles [current.X + 1, current.Y].Walkable)
					w.adjacentTiles.Add (tiles [current.X + 1, current.Y]);
				if (tempTasks != null && tempTasks.Count != 0)
				{
					foreach(Tile adj in w.adjacentTiles)
					{
						adj.SetTasks(tempTasks);
					}
				}
					
			}
			
			foreach (Agent worker in Workers)
			{
				worker.SetProgressBarVisibility(false);
				if (TaskManager.instance.IAT == IAType.Rando)
					AgentUpdate(worker);
			}
			DestroyTile(tile);
		} // !if (constructionMode)
	}

	private void Destroy_Workshop(GetTask gtask, Tile t)
	{
		Workshop workshop = gtask.GetDepartTile().GetComponent<Workshop>();

		TaskManager.instance.DecreaseWorkshopNbOfActiveTasks (workshop);

		if (TaskManager.instance.GetWorkshopNbOfActiveTasks()[workshop] == 0)
			TaskManager.instance.GetWorkshopNbOfActiveTasks ().Remove (workshop);

		if (TaskManager.instance.getActiveTasks().Contains(gtask))
			TaskManager.instance.RemoveActiveTask(gtask);
		if (gtask.GetWorker() != null)
		{
			gtask.GetWorker().SetCarrying(false);
			gtask.GetWorker().typeOfWork = EGoal.Nothing;
			gtask.GetWorker().SetOccupation(false);
			
			DecreaseNumberOfOccupiedWorkers();
			
			gtask.GetWorker().AssignTask(null);
			//gtask.GetWorker().Move(DeliveryTile);
			gtask.SetWorker(null);
		}
		
		EType ty = EType.Plastic;
		
		if (t.type == ESelection.WSElectronics)
			ty = EType.Electronic;
		else if (t.type == ESelection.WSPackaging)
			ty = EType.Packaging;

		bool moving = false;
		if (Workshops.Count == 0)
			moving = true;
		while (!moving)
		{
			int res = Random.Range (0, getWorkshopsNumber ());
			
			Workshop w = Workshops [res];
			
			if (w.type == ty && w.adjacentTiles.Count != 0 && w != workshop)
			{
				foreach(Tile ti in workshop.adjacentTiles)
				{
					ti.GetTasks().Remove(gtask);
				}
				
				if (gtask.GetArrival().GetComponent<Workshop>() != null)
				{
					foreach(Tile ti in gtask.GetArrival().GetComponent<Workshop>().adjacentTiles)
					{
						ti.GetTasks().Remove(gtask);
					}
				}
				//gtask.DestroyTask();
				gtask.TaskNotDone();
				gtask.SetDepartTile(w.GetComponent<Tile> ());
				++TaskManager.instance.GetWorkshopNbOfActiveTasks () [w];
				gtask.ParseTile();
				moving = true;
			}
		}
	}

	private void DestroyTile(Tile t)
	{
		if (t.type == ESelection.WSElectronics || t.type == ESelection.WSPackaging || t.type == ESelection.WSPlastics)
		{
			Workshop w = t.GetComponent<Workshop>();

			UIManager.instance.OnWorkshopDestroyed(w);

			Agent a = null;
			if (w.AgentWorking() != null)
				a = w.AgentWorking();
			w.SetAgentWorking(null);
			Workshops.Remove(w);
			if (a != null && TaskManager.instance.IAT == IAType.Rando)
				shouldUpdateRandom = true;
		}

		if (TaskManager.instance.IAT != IAType.Rando)
		{
			foreach (Task task in t.GetTasks())
			{
				if (!task.IsTaskDone())
				{
					if (task is GetTask && t == ((GetTask) task).GetDepartTile()) //Workshop
					{
						GetTask gtask = (GetTask) task;
						if (gtask.getWaitTask() != null)
							gtask.getWaitTask().DestroyTask();
						if (!(TaskManager.instance.getActiveTasks().Contains(gtask) && gtask.GetWorker() != null && gtask.GetWorker().Carrying()))
							Destroy_Workshop(gtask, t);
						//WORKSHOP
					}
					else if (task.GetWorker() != null && (task.GetWorker().Goal == t || task.GetWorker().Goal2 == t))
					{
						if (TaskManager.instance.ReAssignWorker(task.GetWorker(), t))
						{
							//OK
						}
					}
				}
			}
			t.GetTasks ().Clear ();
		}

		Destroy(t.gameObject);
		if (TaskManager.instance.IAT == IAType.Advanced && TaskManager.instance.BasicTasksInitialized ())
		{
			ResetWorkers ();
			shouldRestartSolver = true;
		}
	}

	public void ResetWorkers()
	{
		foreach (Agent a in Workers)
		{
			if (a.Carrying())
				continue;
			a.typeOfWork = EGoal.Nothing;
			if (a.Occupied())
			{
				a.SetProgressBarVisibility(false);
				DecreaseNumberOfOccupiedWorkers();
			}

			Task t = a.GetAssignedTask ();
			if (t != null)
			{
				if (t is BuildTask)
				{
					Workshop w = t.GetArrival().GetComponent<Workshop>();
					w.SetAgentWorking(null);
				}
				t.SetWorker (null);
			}
			a.SetOccupation(false);
			a.AssignTask(null);
			a.Goal = null;
			a.Goal2 = null;
			a.GetNavMeshAgent ().ResetPath ();
		}
	}

	// Return null if there are several workshops of this type
	private Workshop GetLastWorkshopOfType(EType type)
	{
		Workshop res = null;
		foreach (Workshop w in Workshops)
		{
			if (w.type == type)
			{
				if (res == null)
					res = w;
				else
					return null;
			}
		}

		return res;
	}

	// Return false if tile is the last adjacency of the last workshop of any type
	private bool CheckAdjacencies(Tile tile)
	{
		foreach (Workshop w in Workshops)
		{
			if (w.adjacentTiles.Count == 1 && w.adjacentTiles[0] == tile)
				return false;
		}
		return true;
	}

	public bool IsTileWalkable(uint x, uint y)
	{
		return tiles[x, y].Walkable;
	}

	// Interface callbacks
	public void SetSelectedType(int type)
	{
		ESelection selection = (ESelection)type;
        selectedType = selection;
	}

	public Tile[,] GetTiles()
	{
		return tiles;
	}

	public uint getMapSize()
	{
		return mapSize;
	}

	public int getWorkshopsNumber()
	{
		return Workshops.Count;
	}

	public List<Workshop> getWorkshops()
	{
		return Workshops;
	}

	public List<Agent> getWorkers()
	{
		return Workers;
	}

	public Tile GetFinishTile()
	{
		return FinishTile;
	}

	public Tile GetDeliveryTile()
	{
		return DeliveryTile;
	}

	public void IncreaseNumberOfOccupiedWorkers()
	{
		numberOfOccupiedWorkers++;
	}

	public void DecreaseNumberOfOccupiedWorkers()
	{
		if (numberOfOccupiedWorkers == 0)
			Debug.LogError ("[MAP] Tried to decrease numberOfOccupiedWorkers where it is already 0");
		--numberOfOccupiedWorkers;
	}

	public uint GetNumberOfOccupiedWorkers()
	{
		return numberOfOccupiedWorkers;
	}
}
