﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundEffect
{
    click = 0,
    scratch = 1,
    scratch_slow = 2
}

public class SoundManager : MonoBehaviour {

    public UnityEngine.Camera orbit;
    public UnityEngine.Camera menu;
    public List<AudioClip> clips;

    public bool mute = false;

	// Singleton
	private static SoundManager _instance;

    // BG Music
    private AudioSource gameMusic;

    // Copy lists
    private List<AudioSource> camera;

	public static SoundManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<SoundManager>();
                _instance.Fill();
			}
				
			return _instance;
		}
	}

    public void Mute(bool mute)
    {
        this.mute = mute;

        if (mute)
        {
            gameMusic.playOnAwake = false;
            gameMusic.Stop();
            menu.GetComponent<AudioSource>().Stop();
            menu.GetComponent<AudioSource>().playOnAwake = false;
        }
    }

    private void Fill()
    {
        gameMusic = orbit.GetComponent<AudioSource>();

        if (mute)
            Mute(true);
    }

    public void PlayClip(SoundEffect effect)
    {
        if (!mute)
            gameMusic.PlayOneShot(clips[(int)effect]);
    }

    // 0: normal
    // 1: fast
    // 2: light speed
    public void SetSpeed(int speed)
    {
        switch (speed)
        {
            case 0:
                gameMusic.pitch = 1f;
                break;
            case 1:
                gameMusic.pitch = 1.1f;
                break;
            case 2:
                gameMusic.pitch = 1.2f;
                break;
        }

    }

    public void SetDim(bool active)
    {
        if (active)
            gameMusic.volume = 0.2f;
        else
            gameMusic.volume = 0.5f;
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
