﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Options for extra columns
 */
[Flags]
public enum CSVOptions
{
    None = 0x0,
    Timestamp = 0x1,
    LoggerHash = 0x2,
}

/**
 * This class simply stores a log that will be saved as a CSV file
 * the aim of this is to store various types of data.
 */
public class Logger : MonoBehaviour {

    // Objects to listen to
    public string outputFilePath = "out.csv";
    public string delimiter = ",";
    public bool excelMetadata = false;

    // Store file in ram
    private StringBuilder strb;

	// Init
	void Start () {
        strb = new StringBuilder();
	}

    // Flush to file on application quit
    void OnApplicationQuit()
    {
        FlushToFile();
    }

    // Actual I/O operations are done here
    public void FlushToFile()
    {
        FileStream f = File.Open(outputFilePath, FileMode.Create);
        StreamWriter sw = new StreamWriter(f);

        if (excelMetadata)
            sw.Write("sep=" + delimiter + Environment.NewLine);
        sw.Write(strb.ToString());

        sw.Close();

        // Flush
        strb.Length = 0;
    }

    // Add a line in the output CSV, each string in a different column
    public void appendData(List<string> str)
    {
        if (str.Count > 1)
            strb.Append(str[0]);

        for (int i = 1; i < str.Count; i++)
        {
            strb.Append(delimiter);
            strb.Append(str[i]);
        }

        strb.Append(Environment.NewLine);
    }

    // Add a line in the output CSV, each string in a different column
    // With optional additional info
    public void appendData(List<string> str, CSVOptions options)
    {
        if ((options & CSVOptions.Timestamp) != 0)
            str.Add(Time.realtimeSinceStartup.ToString());
        if ((options & CSVOptions.LoggerHash) != 0)
            str.Add(this.GetHashCode().ToString());

        appendData(str);
    }
}
