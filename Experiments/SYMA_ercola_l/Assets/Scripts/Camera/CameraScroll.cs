﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CameraScroll : MonoBehaviour
{
    public float scrollSpeed;

    private static float padScroll;
    private int stationCount;


    public static float MouseScroll
    {
        get
        {
            float mouseScroll = Input.GetAxis("Mouse ScrollWheel");
            if (mouseScroll != 0)
                return mouseScroll;
            else
                return padScroll;
        }
    }

    private void LateUpdate()
    {
        var camera = Camera.main;
        float input = MouseScroll;
        var newPos = Mathf.Clamp
        (
            camera.orthographicSize + scrollSpeed * input,
            40,
            150 * stationCount / 25
        );
        
        if (!EventSystem.current.IsPointerOverGameObject())
            camera.orthographicSize = newPos;
    }

    private void OnGUI()
    {
        if (Event.current.type == EventType.ScrollWheel)
            padScroll = Event.current.delta.y / 100;
        else
            padScroll = 0;
    }

    private void Awake()
    {
        var simulation = GameObject.Find("Simulation Values");
        stationCount = simulation.GetComponent<SimulationValues>().StationsCount;
    }
}
