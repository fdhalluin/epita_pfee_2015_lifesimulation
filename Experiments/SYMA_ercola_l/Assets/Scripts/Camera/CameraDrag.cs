﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CameraDrag : MonoBehaviour
{
    private Vector3 origin;
    private Vector3 difference;
    private bool drag = false;

    private void LateUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                drag = false;
                return;
            }

            difference = (Camera.main.ScreenToWorldPoint(Input.mousePosition)) - Camera.main.transform.position;
            if (!drag)
            {
                drag = true;
                origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
        else
            drag = false;

        if (drag)
        {
            var pos = origin - difference;
            var posX = Mathf.Clamp(pos.x, -150, 150);
            var posY = Mathf.Clamp(pos.y, -60, 60);

            Camera.main.transform.position = new Vector3(posX, posY, pos.z);

        }
    }
}
