﻿using System;
using System.Linq;
using System.Collections.Generic;

public class StationAgent
{
    public int Count
    {
        get { return Queues.Values.Sum(q => q.Count); }
    }

    public bool HasMonitor { get; set; }

    public Dictionary<int, Queue<TravelerAgent>> Queues { get; private set; }

    public StationAgent()
    {
        Queues =  new Dictionary<int, Queue<TravelerAgent>>();
        HasMonitor = false;
    }

    public void AddLine(int line)
    {
        Queues.Add(line, new Queue<TravelerAgent>());
    }

    public void RemoveTraveler()
    {
        foreach (var queue in Queues.Values)
            if (queue.Count > 0)
            {
                var traveler = queue.Dequeue();
                AgentManager.Instance.Travelers.Remove(traveler);
                break;
            }
    }

    public void HandleBus(BusAgent bus, int here, int from)
    { 
        var travelers = bus.Disembark();

        foreach (var traveler in travelers)
        {
            traveler.UpdatePosition(here, HasMonitor);
            AddTraveler(traveler);
        }

        var queue = Queues[from];
        if (queue.Count == 0)
            return;

        var currentTraveler = queue.Peek();
        while (bus.Embark(currentTraveler))
        {
            queue.Dequeue();
            if (queue.Count == 0)
                return;
            currentTraveler = queue.Peek();
        }
    }

    public void AddTraveler(TravelerAgent traveler)
    {
        int lineNumber = traveler.Next;
        Queues[lineNumber].Enqueue(traveler);
        AgentManager.Instance.Travelers.Add(traveler);
    }
}
