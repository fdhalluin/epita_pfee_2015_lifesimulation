﻿public class Messages
{
    public static string ArriveAtStation(TravelerAgent traveler)
    {
        if (traveler.Position == traveler.Destination)
            return string.Format("At station {0}, destination reached!", traveler.Position);
        else
            return string.Format("At station {0}, still in transit!", traveler.Position);
    }

    public static string NextStation(TravelerAgent traveler)
    {
        return string.Format("The next step is station {0}!", traveler.Next);
    }
    
    public static string NewDestination(TravelerAgent traveler)
    {
        return string.Format("I have a new final destination, station {0}!", traveler.Destination);
    }

    public static string Undisturbed(TravelerAgent traveler)
    {
        return string.Format("Traffic is undisturbed, let's head to {0}", traveler.Next);
    }

    public static string OptimizePath(TravelerAgent traveler)
    {
        if (!traveler.Smartphone)
            return "Let's optimize my path using the station's monitor!";
        else
            return "Let's optimize my path using my smartphone!";
    }

    public static string Header(TravelerAgent traveler)
    {
        return string.Format("Traveler {0}: ", traveler.Serial);
    }

    public static string NotConnexe()
    {
        return "It seems the trasport network isn't connected, please restart the simulation using another seed :(";
    }

    public static string Comparison(TravelerAgent traveler, float dist1, int alternative, float dist2)
    {
        return string.Format
        (
            "Traffic information tells me to head next to {0} whith a distance of {1}," +
            "instead of {2} with a total distance of {3}",
            traveler.Next,
            dist1,
            alternative,
            dist2
        );
    }
}
