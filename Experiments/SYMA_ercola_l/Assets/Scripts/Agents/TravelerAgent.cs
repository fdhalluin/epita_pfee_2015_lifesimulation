﻿using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class TravelerAgent
{
    public Queue<int> Path { get; private set; }
    public int Next { get { return Path.Peek(); } }
    public int Position { get; private set; }
    public int Destination { get; private set; }
    public bool Smartphone { get; set; }
    public bool Tracked { get; set; }
    public int Serial { get; private set; }
    public string Header { get; private set; }

    private static int count;

    public TravelerAgent(int pos)
    {
        Position = pos;
        Tracked = false;
        Smartphone = false;
        Serial = count++;
        Header = Messages.Header(this);
        Path = new Queue<int>();
        ChooseDestination(false);
    }

    public void UpdatePosition(int position, bool hasMonitor)
    {
        Position = position;

        if (Tracked)
            Logs.Instance.PostMessage(Header, Messages.ArriveAtStation(this));

        if (Position == Destination)
            ChooseDestination(hasMonitor);
        else
            ChooseNext(hasMonitor);

        UIController.Instance.UpdateTraveler(this);
    }

    private void ChooseNext(bool hasMonitor)
    {
        if (!hasMonitor && !Smartphone)
        {
            Path.Dequeue();
            if (Tracked)
                Logs.Instance.PostMessage(Header, Messages.NextStation(this));
        }
        else
        {
            if (Tracked)
                Logs.Instance.PostMessage(Header, Messages.OptimizePath(this));
            ComputeNewPath(true);
        }
    }

    private void ComputeNewPath(bool hasMonitor)
    {
        Path.Clear();
        int[,] map;

        if (hasMonitor || Smartphone)
            map = AgentManager.Instance.MonitorRoadMap;
        else
            map = AgentManager.Instance.RoadMap;

        int current = Position;
        bool impossible = false;
        while (current != Destination)
        {
            current = map[current, Destination];
            if (current == -1)
            {
                impossible = true;
                break;
            }
            Path.Enqueue(current);
        }

        if ((hasMonitor || Smartphone) && Tracked)
        {
            var alternative = AgentManager.Instance.RoadMap[Position, Destination];
            if (alternative != Next)
                Logs.Instance.PostMessage
                (
                    Header,
                    Messages.Comparison
                    (
                        this,
                        AgentManager.Instance.TrafficDistances[Position, Destination],
                        alternative,
                        AgentManager.Instance.GeometricDistances[Position, Destination]
                    )
                );
            else
                Logs.Instance.PostMessage(Header, Messages.Undisturbed(this));
        }

        if (impossible)
        {
            Logs.Instance.PostMessage(Header, Messages.NotConnexe());
            ChooseDestination(hasMonitor);
        }
    }

    private void ChooseDestination(bool hasMonitor)
    {
        int max = AgentManager.Instance.StationsCount;

        do
        {
            Destination = Random.Range(0, max);
        } while (Destination == Position);

        ComputeNewPath(hasMonitor);

        if (Tracked)
            Logs.Instance.PostMessage(Header, Messages.NewDestination(this));
    }
}
