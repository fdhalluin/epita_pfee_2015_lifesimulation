﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class BusAgent
{
    public float Speed { get; private set; }
    public bool Ok { get; private set; }

    private const int CAPACITY = 5;
    private List<TravelerAgent> travelers = new List<TravelerAgent>();
    private float lastSpeed;

    public bool Embark(TravelerAgent traveler)
    {
        if (travelers.Count >= CAPACITY)
            return false;
        travelers.Add(traveler);
        return true;
    }

    public void UTurn()
    {
        Speed = -Speed; 
    }

    public void SetSpeed(float value)
    {
        if (!Ok)
        {
            lastSpeed = value;
            return;
        }

        if (Speed < 0)
            Speed = -value;
        else
            Speed = value;
    }

    public List<TravelerAgent> Disembark()
    {
        var tmp = travelers;
        travelers = new List<TravelerAgent>();
        return tmp;
    }

    public void Destroy()
    {
        foreach (var traveler in travelers)
            AgentManager.Instance.Travelers.Remove(traveler);
    }

    public void ToggleStatus()
    {
        if (Ok)
        {
            lastSpeed = Speed;
            Speed = 0;
        }
        else
            Speed = lastSpeed;

        Ok = !Ok;

        AgentManager.Instance.ShouldRecompute = true;
    }

    public BusAgent()
    {
        Ok = true;
    }
}