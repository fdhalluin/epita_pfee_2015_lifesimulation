﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class AgentManager
{
    private static AgentManager instance = new AgentManager();
    public static AgentManager Instance
    {
        get { return instance; }
    }

    public int StationsCount { get; private set; }
    public bool ShouldRecompute { get; set; }

    public HashSet<TravelerAgent> Travelers { get; private set; }

    public int[,] RoadMap
    {
        get { return roadMap; }
    }

    public float[,] GeometricDistances
    {
        get { return geometricDistances; }
    }

    public int[,] MonitorRoadMap
    {
        get { return monitorRoadMap; }
    }

    public float[,] TrafficDistances
    {
        get { return trafficDistances; }
    }

    private int[,] roadMap;
    private float[,] geometricDistances;
    private int[,] monitorRoadMap;
    private float[,] trafficDistances;

    private delegate float Distance(Edge<StationController> edge);

    public void ComputeRoadMap(Graph<StationController> graph)
    {
        StationsCount = graph.Vertices.Count;
        Distance distance = (e => e.First.Distance(e.Second));
        FloydWarshall(graph, distance, out roadMap, out geometricDistances);
        ShouldRecompute = true;
    }

    public void ComputeMonitorRoadMap(Graph<StationController> graph)
    {
        if (!ShouldRecompute)
            return;

        Distance distance = e =>
        {
            var availableBuses = e.Controller.AvailableBuses;
            if (availableBuses == 0)
                return float.MaxValue;

            var d = GeometricDistances[e.First.Serial, e.Second.Serial];
            return (d / e.Controller.Speed) / availableBuses;
        };

        FloydWarshall(graph, distance, out monitorRoadMap, out trafficDistances);
        ShouldRecompute = false;
    }

    private static void FloydWarshall(Graph<StationController> graph,
                                      Distance distance,
                                      out int[,] map,
                                      out float[,] distances)
    {
        int vCount = graph.Vertices.Count;
        map = new int[vCount, vCount];
        distances = new float[vCount, vCount];

        for (int i = 0; i < vCount; ++i)
            for (int j = 0; j < vCount; ++j)
            {
                map[i, j] = -1;
                distances[i, j] = float.MaxValue;
            }

        foreach (var edge in graph.Edges)
        {
            var s1 = edge.First.Serial;
            var s2 = edge.Second.Serial;

            var dist = distance(edge);
            map[s1, s2] = s2;
            map[s2, s1] = s1;
            distances[s1, s2] = distances[s2, s1] = dist;
        }

        for (int k = 0; k < vCount; ++k)
            for (int i = 0; i < vCount; ++i)
                for (int j = 0; j < vCount; ++j)
                    if (distances[i, j] > distances[i, k] + distances[k, j])
                    {
                        distances[i, j] = distances[i, k] + distances[k, j];
                        map[i, j] = map[i, k];
                    }
    }

    private AgentManager()
    {
        Travelers = new HashSet<TravelerAgent>();
    }
}