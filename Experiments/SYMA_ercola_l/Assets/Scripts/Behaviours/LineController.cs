﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using System.Collections.Generic;

public class LineController : MonoBehaviour
{
    public float speedMin;
    public float speedMax;

    public GameObject bus;

    public float Speed { get; private set; }
    public List<BusController> Buses { get; private set; }
    public Edge<StationController> Edge { get; private set; }
    public int AvailableBuses
    {
        get { return (from bus in Buses where bus.Ok select bus).Count(); }
    }

    private Vector3 middle;
    private Quaternion rotation;
    private Vector3 direction;

    public void Init(Edge<StationController> edge, Vector3 middle, Quaternion rotation, Vector3 direction)
    {
        Edge = edge;
        Buses = new List<BusController>();
        this.middle = middle;
        this.rotation = rotation;
        this.direction = direction;

        Speed = Random.Range(speedMin, speedMax);
    }

    public void SelectedColors()
    {
        var lineRenderer = gameObject.GetComponent<LineRenderer>();
        var color =  new Color
        (
            229f / 255f,
            124f / 255f,
            67f / 255f,
            200f / 255f
        );

        lineRenderer.SetColors(color, color);
    }

    public void SetSpeed(float value)
    {
        Speed = value;
        foreach (var bus in Buses)
            bus.Speed = value;
    }

    public void AddBus()
    {
        var position = middle;
        position.z = -1f;

        var busObject = Instantiate(bus, position, rotation) as GameObject;
        var busController = busObject.GetComponent<BusController>();
        busController.Init(Edge, direction, Speed);
        Buses.Add(busController);

        UpdateCount();
    }

    public void RemoveBus()
    {
        if (Buses.Count == 0)
            return;

        var toRemove = Buses[Buses.Count - 1];
        Buses.RemoveAt(Buses.Count - 1);
        toRemove.Destroy();

        UpdateCount();
    }

    public void NormalColors()
    {
        var lineRenderer = gameObject.GetComponent<LineRenderer>();
        var color = new Color ( 1f, 1f, 1f, 65f / 255f);
        lineRenderer.SetColors(color, color);
    }

    private void UpdateCount()
    {
        var ui = UIController.Instance;
        if (ui.CurrentLine == this)
            ui.UpdateBusesDisplay();
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            UIController.Instance.ShowLine(this);
    }

}