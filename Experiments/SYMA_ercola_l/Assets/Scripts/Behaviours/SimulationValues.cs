﻿using UnityEngine;
using System.Collections;

public class SimulationValues : MonoBehaviour
{
    public int StationsCount { get; set; }
    public int TravelersCount { get; set; }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
