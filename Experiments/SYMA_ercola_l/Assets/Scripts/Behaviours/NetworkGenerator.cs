﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class NetworkGenerator : MonoBehaviour
{
    public GameObject line;
    public GameObject station;

    private SimulationValues simulationValues;
    private Graph<StationController> network;

    private void Awake()
    {
        var simulation = GameObject.Find("Simulation Values");
        simulationValues = simulation.GetComponent<SimulationValues>();

        var stations = GenStations();
        network = new Graph<StationController>(stations);
        PruneIntersectingEdges();
        GenLines();
        SetupAgents();
        GenTravelers();

    }

    private void LateUpdate()
    {
        AgentManager.Instance.ComputeMonitorRoadMap(network);
    }

    private void SetupAgents()
    {
        AgentManager.Instance.ComputeRoadMap(network);

        foreach (var edge in network.Edges)
        {
            edge.First.AddLine(edge.Second.Serial);
            edge.Second.AddLine(edge.First.Serial);
        }

        UIController.Instance.ShowStation(network.Vertices[0]);
    }

    private void GenTravelers()
    {
        int nbTravelers = simulationValues.TravelersCount;

        for (int i = 0; i < nbTravelers; ++i)
        {
            int position = Random.Range(0, network.Vertices.Count);
            var station = network.Vertices[position];
            var traveler = new TravelerAgent(position);
            station.AddTraveler(traveler);
        }
    }

    private void PruneIntersectingEdges()
    {
        var toRemove = new List<Edge<StationController>>();
        foreach (var u in network.Edges)
        {
            foreach (var v in network.Edges)
            {
                var u1 = new Vector2(u.First.X, u.First.Y);
                var u2 = new Vector2(u.Second.X, u.Second.Y);
                var v1 = new Vector2(v.First.X, v.First.Y);
                var v2 = new Vector2(v.Second.X, v.Second.Y);

                if (u1 == v1 || u1 == v2 || u2 == v1 || u2 == v2)
                    continue;

                if (ccw(u1, v1, v2) != ccw(u2, v1, v2) && ccw(u1, u2, v1) != ccw(u1, u2, v2))
                {
                    var d1 = u.First.Distance(u.Second);
                    var d2 = v.First.Distance(v.Second);

                    if (d1 >= d2)
                    {
                        toRemove.Add(u);
                        break;
                    }
                    else
                        toRemove.Add(v);
                }
            }
        }

        foreach (var edge in toRemove)
            network.Edges.Remove(edge);
    }

    private static Quaternion ComputeRotation(Vector2 a, Vector2 b, out Vector3 direction)
    {
        float X1, X2, Y1, Y2;
        if (a.x > b.x)
        {
            X1 = a.x; Y1 = a.y;
            X2 = b.x; Y2 = b.y;
        }
        else
        {
            X2 = a.x; Y2 = a.y;
            X1 = b.x; Y1 = b.y;
        }

        direction = new Vector3(X1 - X2, Y1 - Y2).normalized;
        var angle = Vector3.Angle(Vector3.right, direction);
        if (direction.y < 0)
            angle = -angle;

        return Quaternion.Euler(0, 0, angle);
    }

    private void GenLines()
    {
        foreach (var edge in network.Edges)
        {
            var v1 = new Vector2(edge.First.X, edge.First.Y);
            var v2 = new Vector2(edge.Second.X, edge.Second.Y);

            Vector3 direction;
            var rotation = ComputeRotation(v1, v2, out direction);
            var position = new Vector3
            (
                edge.First.X + (edge.Second.X - edge.First.X) / 2,
                edge.First.Y + (edge.Second.Y - edge.First.Y) / 2,
                0f
            );

            var lineObject = Instantiate(line, position, rotation) as GameObject;

            var lineController = lineObject.GetComponent<LineController>();
            lineController.Init(edge, position, rotation, direction);
            lineController.AddBus();

            // 12 = Magic not to overlap stations.
            var distance = edge.First.Distance(edge.Second) - 12;
            var currentScale = lineObject.transform.localScale;
            var newScale = new Vector3(distance, currentScale.y, currentScale.z);
            lineObject.transform.localScale = newScale;

            var lineRenderer = lineObject.GetComponent<LineRenderer>();
            lineRenderer.SetPosition(0, new Vector3(edge.First.X, edge.First.Y, 0f));
            lineRenderer.SetPosition(1, new Vector3(edge.Second.X, edge.Second.Y, 0f));

            edge.Controller = lineController;
        }
    }

    private List<StationController> GenStations()
    {
        var stations = new List<StationController>();
        int stationCount = simulationValues.StationsCount;
        
        float xMin = -150f * stationCount / 25;
        float xMax = 150f * stationCount / 25;
        float yMin = -60f * stationCount / 25;
        float yMax = 60f * stationCount / 25;


        for (int i = 0; i < stationCount; ++i)
        {
            bool tooClose = false;
            Quaternion rotation;
            Vector3 position;

            do
            {
                rotation = Quaternion.identity;
                position = new Vector3
                (
                    Random.Range(xMin, xMax),
                    Random.Range(yMin, yMax),
                    -2f
                );

                for (int k = 0; k < stations.Count; ++k)
                {
                    var otherX = stations[k].X;
                    var otherY = stations[k].Y;
                    var distance = Mathf.Sqrt(Mathf.Pow(position.x - otherX, 2) + Mathf.Pow(position.y - otherY, 2));
                    if (distance < 25)
                    {
                        tooClose = true;
                        break;
                    }
                    else
                        tooClose = false;
                }

            }
            while (tooClose);

            var gameObject = Instantiate(station, position, rotation) as GameObject;
            var controller = gameObject.GetComponent<StationController>();
            controller.Init();
            stations.Add(controller);
        }

        return stations;
    }

    private static bool ccw(Vector2 A, Vector2 B, Vector2 C)
    {
        return (C.y - A.y) * (B.x - A.x) > (B.y - A.y) * (C.x - A.x);
    }
}
