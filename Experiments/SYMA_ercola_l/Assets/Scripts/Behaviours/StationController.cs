﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class StationController : MonoBehaviour, IMesurable<StationController, float>
{
    public float X { get; private set; }
    public float Y { get; private set; }
    public int Serial { get; private set; }

    public StationAgent Agent { get { return stationAgent; } }

    private static int count = 0;
    private StationAgent stationAgent = new StationAgent();

    public void Init()
    {
        Serial = count++;
        X = gameObject.transform.position.x;
        Y = gameObject.transform.position.y;
    }

    public void AddLine(int line)
    {
        stationAgent.AddLine(line);
    }

    public void HandleBus(BusAgent bus, int from)
    {
        Agent.HandleBus(bus, Serial, from);
        UpdateCount();
    }

    public void AddTraveler(TravelerAgent traveler)
    {
        stationAgent.AddTraveler(traveler);
        UpdateCount();
    }

    public void RemoveTraveler()
    {
        Agent.RemoveTraveler();
        UpdateCount();
    }

    public float Distance(StationController other)
    {
        return Mathf.Sqrt(Mathf.Pow(X - other.X, 2) + Mathf.Pow(Y - other.Y, 2));
    }

    public void SelectedColors()
    {
        var sprite = gameObject.GetComponent<SpriteRenderer>();
        sprite.color = new Color
        (
            229f / 255f,
            124f / 255f,
            67f / 255f
        );
    }

    public void NormalColors()
    {
        var sprite = gameObject.GetComponent<SpriteRenderer>();
        if (Agent.HasMonitor)
        {
            Color c = new Color
            (
                73 / 255f,
                172 / 255f,
                221 / 255f
            );

            sprite.color = c;
        }
        else
            sprite.color = Color.white;
    }

    public void UpdateCount()
    {
        if (!UIController.Instance.ShowStationSerials)
        {
            int count = stationAgent.Count;
            var textMesh = gameObject.GetComponentInChildren<TextMesh>();
            textMesh.text = count.ToString();
        }

        var ui = UIController.Instance;
        if (ui.CurrentStation == this)
            ui.UpdateTravelersCount();
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            UIController.Instance.ShowStation(this);
    }
}