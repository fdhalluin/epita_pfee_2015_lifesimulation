﻿using UnityEngine;
using System.Collections.Generic;

public class BusController : MonoBehaviour
{
    public bool Ok
    {
        get { return busAgent.Ok; }
    }
    public float Speed
    {
        get { return busAgent.Speed; }
        set { busAgent.SetSpeed(value); }
    }

    private Vector3 direction;
    private Edge<StationController> line;
    private BusAgent busAgent = new BusAgent();

    public void Init(Edge<StationController> line, Vector3 dir, float speed)
    {
        this.line = line;
        direction = dir;
        this.Speed = speed;
    }

    public void ToggleStatus()
    {
        busAgent.ToggleStatus();
    }

    public void Destroy()
    {
        busAgent.Destroy();
        DestroyImmediate(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Station")
        {
            var controller = other.gameObject.GetComponent<StationController>();
            if (controller.Equals(line.First) || controller.Equals(line.Second))
            {
                int otherSerial = line.First.Serial;
                if (controller.Equals(line.First))
                    otherSerial = line.Second.Serial;
                controller.HandleBus(busAgent, otherSerial);

                busAgent.UTurn();
            }
        }
    }

    private void Update()
    {
        var pos = gameObject.transform.position;
        gameObject.transform.position = pos + direction * Speed * Time.deltaTime;
    }
}
