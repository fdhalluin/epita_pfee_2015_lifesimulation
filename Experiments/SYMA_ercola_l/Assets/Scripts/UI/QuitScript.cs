﻿using UnityEngine;
using System.Collections;

public class QuitScript : MonoBehaviour
{
    public void QuitApplication()
    {
        Application.Quit();
    }
}
