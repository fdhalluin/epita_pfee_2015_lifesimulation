﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;

public class Logs : MonoBehaviour
{
    public static Logs Instance { get; private set; }

    private Text textArea;

    public Logs()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    public void PostMessage(string header, string str)
    {
        StringBuilder sb = new StringBuilder(textArea.text);
        sb.Append(header);
        sb.AppendLine(str);
        textArea.text = sb.ToString();
    }

    private void Update()
    {
        if (textArea.text.Length < 5000)
            return;

        textArea.text = textArea.text.Remove(0, textArea.text.Length - 2000);
    }

    private void Start()
    {
        textArea = gameObject.GetComponent<Text>(); 
    }
}
