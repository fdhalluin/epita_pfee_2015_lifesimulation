﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

enum State
{
    Line,
    Station,
}

public class UIController : MonoBehaviour
{
    public GameObject busField;
    public GameObject travelerField;

    public GameObject lineUI;
    public GameObject stationUI;
    public GameObject travelersUI;
    public GameObject busViewport;
    public GameObject travelersViewport;

    public float initialBusOffset;
    public float busOffsets;

    public static UIController Instance { get; private set; }
    public StationController CurrentStation { get; private set; }
    public LineController CurrentLine { get; private set; }
    public bool ShowStationSerials { get; set; }
    public object SavedCurrent { get; private set; }

    private State savedState;
    private List<GameObject> statuses;
    private Dictionary<TravelerAgent, TravelerFieldScript> travelerFields;

    public void ShowStation(StationController station)
    {
        SwitchMenu();

        stationUI.SetActive(true);
        CurrentStation = station;
        UpdateTravelersCount();

        var gameObject = GameObject.Find("Station Name");
        var text = gameObject.GetComponent<Text>();
        text.text = string.Format("Station {0}", station.Serial);

        var toggle = stationUI.GetComponentInChildren<Toggle>();
        toggle.isOn = CurrentStation.Agent.HasMonitor;

        CurrentStation.SelectedColors();
    }

    public void ShowLine(LineController line)
    {
        SwitchMenu();

        lineUI.SetActive(true);
        CurrentLine = line;
        UpdateBusesDisplay();
        UpdateBusesStatus();
        UpdateBusesSpeed();

        var gameObject = GameObject.Find("Line Name");
        var text = gameObject.GetComponent<Text>();

        var s1 = line.Edge.First.Serial;
        var s2 = line.Edge.Second.Serial;

        if (s1 > s2)
        {
            var tmp = s1;
            s1 = s2;
            s2 = tmp;
        }

        text.text = string.Format("From {0} To {1}", s1, s2);

        CurrentLine.SelectedColors();
    }

    public void CreateTravelersFields()
    {
        travelerFields = new Dictionary<TravelerAgent, TravelerFieldScript>();
        foreach (var traveler in AgentManager.Instance.Travelers.Take(35))
        {
            var position = Vector3.zero;
            var rotation = Quaternion.identity;

            var gameObject = Instantiate(travelerField, position, rotation) as GameObject;
            gameObject.transform.SetParent(travelersViewport.transform);
            var controller = gameObject.GetComponent<TravelerFieldScript>();
            controller.Init(traveler);
            travelerFields.Add(traveler, controller);

            var toogles = gameObject.GetComponentsInChildren<Toggle>();
            toogles[0].isOn = traveler.Smartphone;
            toogles[1].isOn = traveler.Tracked;
        }
    }

    public void ToggleTravelersUI(bool value)
    {
        if (value)
        {
            if (CurrentLine == null)
            {
                SavedCurrent = CurrentStation;
                CurrentStation = null;
                savedState = State.Station;
                stationUI.SetActive(false);
            }
            else
            {
                SavedCurrent = CurrentLine;
                CurrentLine = null;
                savedState = State.Line;
                lineUI.SetActive(false);
            }

            travelersUI.SetActive(true);
            CreateTravelersFields();
        }
        else
        {
            travelersUI.SetActive(false);
            if (savedState == State.Line)
            {
                CurrentLine = SavedCurrent as LineController;
                lineUI.SetActive(true);
            }
            else
            {
                CurrentStation = SavedCurrent as StationController;
                stationUI.SetActive(true);
            }

            foreach (var field in travelerFields.Values)
                DestroyImmediate(field.gameObject);
            travelerFields.Clear();
        }
    }

    public void UpdateTraveler(TravelerAgent traveler)
    {
        if (CurrentLine != null || CurrentStation != null)
            return;

        if (travelerFields.ContainsKey(traveler))
            travelerFields[traveler].UpdateDisplay();
    }

    public void UpdateTravelersCount()
    {
        var count = CurrentStation.Agent.Count;

        var gameObject = GameObject.Find("Travelers Number");
        var text = gameObject.GetComponent<Text>();
        text.text = count.ToString();
    }

    public void UpdateBusesSpeed()
    {
        var gameObject = GameObject.Find("Speed Label");
        var text = string.Format("{0} km/h", 3 * CurrentLine.Speed);
        gameObject.GetComponent<Text>().text = text;

        lineUI.GetComponentInChildren<Slider>().value = CurrentLine.Speed;
    }

    public void UpdateBusesDisplay()
    {
        var count = CurrentLine.Buses.Count;

        var gameObject = GameObject.Find("Buses Number");
        var text = gameObject.GetComponent<Text>();
        text.text = count.ToString();

        if (statuses.Count > CurrentLine.Buses.Count)
        {
            var toRemove = new List<int>();
            for (int i = CurrentLine.Buses.Count; i < statuses.Count; ++i)
                toRemove.Add(i);

            foreach (var idx in toRemove)
                DestroyImmediate(statuses[idx]);

            statuses.RemoveRange(CurrentLine.Buses.Count, statuses.Count - CurrentLine.Buses.Count);
        }
        else if (CurrentLine.Buses.Count > statuses.Count)
        {
            for (int i = statuses.Count; i < CurrentLine.Buses.Count; ++i)
                AddBusStatus(CurrentLine.Buses[i]);
        }

    }


    public void UpdateBusStatus(BusController bus)
    {
        int i = 0;
        for (; i < statuses.Count; ++i)
        {
            var script = statuses[i].GetComponent<ModifyBusStatus>();
            if (script.Bus == bus)
                break;
        }

        UpdateBusStatus(i, bus);
    }


    public UIController()
    {
        CurrentStation = null;
        ShowStationSerials = false;
        statuses = new List<GameObject>();

        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    private void UpdateBusesStatus()
    {
        for (int i = 0; i < statuses.Count; ++i)
        {
            var bus = statuses[i].GetComponent<ModifyBusStatus>().Bus;
            UpdateBusStatus(i, bus);
        }
    }

    private void UpdateBusStatus(int i, BusController bus)
    {
        var label = statuses[i].GetComponentInChildren<Text>();

        if (bus.Ok)
        {
            label.text = "OK";
            label.color = Color.green;
        }
        else
        {
            label.text = "KO";
            label.color = Color.red;
        }
    }

    private void AddBusStatus(BusController bus)
    {
        var position = Vector3.zero;
        var rotation = Quaternion.identity;

        var gameObject = Instantiate(busField, position, rotation) as GameObject;

        var rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.SetParent(busViewport.transform);
        
        var script = gameObject.GetComponent<ModifyBusStatus>();
        script.Bus = bus;

        statuses.Add(gameObject);
        UpdateBusStatus(statuses.Count - 1, bus);
    }

    private void SwitchMenu()
    {
        if (CurrentStation != null)
            CurrentStation.NormalColors();

        if (savedState == State.Station)
        {
            var station = SavedCurrent as StationController;
            if (station != null)
                station.NormalColors();
        }

        if (CurrentLine != null)
            CurrentLine.NormalColors();

        if (savedState == State.Line)
        {
            var line = SavedCurrent as LineController;
            if (line != null)
                line.NormalColors();
        }

        foreach (var status in statuses)
            DestroyImmediate(status);

        statuses.Clear();

        var travelerToggle = GameObject.Find("Show Travelers")
                                       .GetComponent<Toggle>();
        travelerToggle.isOn = false;

        CurrentLine = null;
        CurrentStation = null;

        lineUI.SetActive(false);
        stationUI.SetActive(false);
        travelersUI.SetActive(false);
    }
}
