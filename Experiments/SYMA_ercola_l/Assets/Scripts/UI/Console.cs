﻿using UnityEngine;
using System.Collections;

public class Console : MonoBehaviour
{
    public float speed;

    public void ToggleConsole()
    {
        speed = -speed;
    }

    private void Update()
    {
        var rectTransform = gameObject.GetComponent<RectTransform>();
        if (rectTransform.anchoredPosition.y != 200 || rectTransform.anchoredPosition.y != 0)
        {
            var positionY = Mathf.Clamp(rectTransform.anchoredPosition.y + speed * Time.deltaTime, 0, 200);
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, positionY);
        }
    }
}
