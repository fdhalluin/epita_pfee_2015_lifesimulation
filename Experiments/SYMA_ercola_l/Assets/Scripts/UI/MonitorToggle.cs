﻿using UnityEngine;
using System.Collections;

public class MonitorToggle : MonoBehaviour
{
    public void ToggleMonitor(bool value)
    {
        UIController.Instance.CurrentStation.Agent.HasMonitor = value;
    }

    public void ToggleEverywhere(bool value)
    {
        var stations = GameObject.FindGameObjectsWithTag("Station");
        foreach (var station in stations)
        {
            var controller = station.GetComponent<StationController>();
            controller.Agent.HasMonitor = value;
            if (controller != UIController.Instance.CurrentStation
                && UIController.Instance.SavedCurrent != controller)
                controller.NormalColors();
        }
    }
}
