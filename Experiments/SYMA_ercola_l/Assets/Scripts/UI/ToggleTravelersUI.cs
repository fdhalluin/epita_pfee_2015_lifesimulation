﻿using UnityEngine;

public class ToggleTravelersUI : MonoBehaviour
{
    public void Toggle(bool value)
    {
        UIController.Instance.ToggleTravelersUI(value);
    }
}
