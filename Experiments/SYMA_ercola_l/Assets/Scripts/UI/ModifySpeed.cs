﻿using UnityEngine;
using System.Collections;

public class ModifySpeed : MonoBehaviour
{
    public void ChangeSpeed(float value)
    {
        UIController.Instance.CurrentLine.SetSpeed(value);
        AgentManager.Instance.ShouldRecompute = true;
        UIController.Instance.UpdateBusesSpeed();
    }
}
