﻿using UnityEngine;
using System.Collections;

public class ModifyBuses : MonoBehaviour
{
    public void MoreBuses()
    {
        var line = UIController.Instance.CurrentLine;
        line.AddBus();
        AgentManager.Instance.ShouldRecompute = true;
    }

    public void LessBuses()
    {
        var line = UIController.Instance.CurrentLine;
        line.RemoveBus();
        AgentManager.Instance.ShouldRecompute = true;
    }
}
