﻿using UnityEngine;
using UnityEngine.UI;

public class TravelerFieldScript: MonoBehaviour
{
    private TravelerAgent traveler;

    public void Init(TravelerAgent traveler)
    {
        this.traveler = traveler;

        var texts = gameObject.GetComponentsInChildren<Text>();
        texts[0].text = traveler.Serial.ToString();

        UpdateDisplay();
    }

    public void UpdateDisplay()
    {
        var path = string.Format("{0} ➙ {1} ... {2}", traveler.Position,
                                                       traveler.Next,
                                                       traveler.Destination);

        var texts = gameObject.GetComponentsInChildren<Text>();
        texts[1].text = path;
    }

    public void ToggleSmartphone(bool value)
    {
        traveler.Smartphone = value;
    }

    public void ToggleTracked(bool value)
    {
        traveler.Tracked = value;
    }
}
