﻿using UnityEngine;
using UnityEngine.UI;
using System;

using Random = UnityEngine.Random;

public class StartSubmit : MonoBehaviour
{
    private Text seedInput;
    private Toggle seedToggle;
    private Text stationsInput;
    private Text travelersInput;
    private SimulationValues values;

    public void Submit()
    {
        try
        {
            if (seedToggle.isOn)
                Random.seed = int.Parse(seedInput.text);
        }
        catch(FormatException)
        { }
                       
        int stations;
        int travelers;

        values.StationsCount = int.TryParse(stationsInput.text, out stations) ? stations : 25;
        values.TravelersCount = int.TryParse(travelersInput.text, out travelers) ? travelers : 125;

        Application.LoadLevel("Main");
    }

    private void Awake()
    {
        var simulation = GameObject.Find("Simulation Values");
        values = simulation.GetComponent<SimulationValues>();

        var seed = GameObject.Find("Seed Fields/Seed/Text");
        seedInput = seed.GetComponent<Text>();

        var toggle = GameObject.Find("Seed Fields/Toggle Seed");
        seedToggle = toggle.GetComponent<Toggle>();

        var stations = GameObject.Find("Stations/Text");
        stationsInput = stations.GetComponent<Text>();

        var travelers = GameObject.Find("Travelers/Text");
        travelersInput = travelers.GetComponent<Text>();
    }
}
