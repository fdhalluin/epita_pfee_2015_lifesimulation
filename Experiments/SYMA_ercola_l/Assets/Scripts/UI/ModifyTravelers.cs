﻿using UnityEngine;
using System.Collections;

public class ModifyTravelers : MonoBehaviour
{
    public void MoreTravelers()
    {
        var station = UIController.Instance.CurrentStation;
        var traveler = new TravelerAgent(station.Serial);
        station.AddTraveler(traveler);
    }

    public void LessTravelers()
    {
        var station = UIController.Instance.CurrentStation;
        station.RemoveTraveler();
    }
}
