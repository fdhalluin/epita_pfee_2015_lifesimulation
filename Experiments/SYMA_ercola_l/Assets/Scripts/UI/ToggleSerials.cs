﻿using UnityEngine;
using System.Collections;

public class ToggleSerials : MonoBehaviour
{
    public void ToggleStationsSerial(bool value)
    {
        UIController.Instance.ShowStationSerials = value;
        var stations = GameObject.FindGameObjectsWithTag("Station");

        if (value)
            foreach (var station in stations)
            {
                var controller = station.GetComponent<StationController>();
                var text = station.GetComponentInChildren<TextMesh>();
                text.text = controller.Serial.ToString();
            }
        else
            foreach (var station in stations)
            {
                var controller = station.GetComponent<StationController>();
                controller.UpdateCount();
            }
    }
}
