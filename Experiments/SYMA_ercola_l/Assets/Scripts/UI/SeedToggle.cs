﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SeedToggle : MonoBehaviour
{
    private InputField seedInput;

    public void ToggleSeed(bool value)
    {
        seedInput.interactable = value;
    }

    private void Awake()
    {
        var gameObject = GameObject.Find("Seed Fields/Seed");
        seedInput = gameObject.GetComponent<InputField>();
    }
}
