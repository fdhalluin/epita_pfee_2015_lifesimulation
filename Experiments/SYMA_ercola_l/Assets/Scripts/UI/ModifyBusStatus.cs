﻿using UnityEngine;

public class ModifyBusStatus : MonoBehaviour
{
    public BusController Bus { get; set; }

    public void ToggleStatus()
    {
        Bus.ToggleStatus();
        UIController.Instance.UpdateBusStatus(Bus);
    }
}