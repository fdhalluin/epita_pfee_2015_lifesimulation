﻿public interface IMesurable<T, U>
{
    U Distance(T other);
}
