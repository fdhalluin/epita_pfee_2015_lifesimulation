﻿using System;
using System.Collections.Generic;

public class Edge<T> : IEqualityComparer<Edge<T>>
{
    public T First { get; private set; }
    public T Second { get; private set; }

    // Sad for the overall design, but I need it here T_T.
    public LineController Controller { get; set; }

    public Edge(T first, T second)
    {
        First = first;
        Second = second;
    }

    public bool Equals(Edge<T> x, Edge<T> y)
    {
        return x.First.Equals(y.First) && x.Second.Equals(y.Second)
            || x.First.Equals(y.Second) && y.Second.Equals(x.First);
    }

    public override int GetHashCode()
    {
        return GetHashCode(this);
    }

    public int GetHashCode(Edge<T> obj)
    {
        unchecked
        {
            int hash = 17;
            hash = hash * 23 + obj.First.GetHashCode(); 
            hash = hash * 23 + obj.Second.GetHashCode();
            return hash;
        }
    }
}
