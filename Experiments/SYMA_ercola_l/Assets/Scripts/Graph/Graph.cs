﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Graph<T>
    where T: IMesurable<T, float>
{
    public HashSet<Edge<T>> Edges { get; private set; }
    public List<T> Vertices { get; private set; }

    private float[,] distances;

    public Graph(List<T> nodes)
    {
        Vertices = nodes;
        Edges = new HashSet<Edge<T>>();

        distances = new float[nodes.Count, nodes.Count];
        for (int i = 0; i < nodes.Count; ++i)
            for (int j = 0; j < nodes.Count; ++j)
                distances[i, j] = distances[j, i] = nodes[i].Distance(nodes[j]);

            GenMinimalGraph();
    }

    private void GenMinimalGraph()
    {
        int[] connections = new int[Vertices.Count];
        bool[,] connected = new bool[Vertices.Count, Vertices.Count];

        for (int i = 0; i < Vertices.Count; ++i)
            for (int j = 0; j < Vertices.Count; ++j)
                connected[i, j] = false;

        for (int i = 0; i < Vertices.Count; ++i)
        {
            T[] neighbours = new T[2];
            float[] neighboursDists = new float[] { float.MaxValue, float.MaxValue };

            for (int j = 0; j < Vertices.Count; ++j)
            {
                if (i == j || connected[i, j])
                    continue;

                if (connections[i] != 0 && connections[j] != 0)
                    continue;

                if (distances[i, j] < neighboursDists[0])
                {
                    neighboursDists[1] = neighboursDists[0];
                    neighbours[1] = neighbours[0];

                    neighboursDists[0] = distances[i, j];
                    neighbours[0] = Vertices[j];

                    connected[i, j] = connected[j, i] = true;
                }
                else if (distances[i, j] < neighboursDists[1])
                {
                    neighboursDists[1] = distances[i, j];
                    neighbours[1] = Vertices[j];

                    connected[i, j] = connected[j, i] = true;
                }
            }

            if (neighboursDists[1] == float.MaxValue && neighboursDists[0] == float.MaxValue)
                return;

            var edge = new Edge<T>(Vertices[i], neighbours[0]);
            Edges.Add(edge);

            if (connections[i] == 0 && neighboursDists[1] != float.MaxValue)
            {
                var secondEdge = new Edge<T>(Vertices[i], neighbours[1]);
                Edges.Add(secondEdge);
            }
        }
    }
}