using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Pawn : MonoBehaviour
{
	public Color highlight;
	public uint X;
	public uint Y;
	public float movingSpeed = 5.0f;
	
	private FalseMap falseMap;
	private Map realMap;
	private Renderer rend;
	private Collider col;
	private AudioSource knockSfx;

	private Color baseColor;
	private Tile goal;
	private bool waiting = false;
	private bool helping = false;


	// List of the positions to go to
	private List<Tile> intentions;

	// Use this for initialization
	void Start ()
	{
		rend = GetComponentInChildren<Renderer>();
		col = GetComponentInChildren<Collider>();
		knockSfx = GetComponent<AudioSource>();
		
		baseColor = rend.material.color;
	}
	
	public void SetMap(Map m)
	{
		//print("START OF FALSE MAP");
		realMap = m;
		
		falseMap = new FalseMap (realMap.GetTiles (), realMap.getMapSize());
	}

	public void Step()
	{
		if (intentions.Count == 0)
			return;

		Tile destination = intentions[0];
		if (destination.Walkable)
		{
			SetPosition (destination.X, destination.Y);
			if (destination is ButtonTile)
			{
				((ButtonTile)destination).Press ();

				falseMap.ButtonTyped((ButtonTile) destination);
				realMap.ButtonTyped(this, (ButtonTile) destination);
				//realMap.UpdatePathOfPawns ();
			}

			intentions.Remove(destination);
		}
		else if (realMap.IsCoopActivated() && falseMap.IsTileWalkable (destination.X, destination.Y) && (realMap.IsForgottenPawn(this) || !waiting))
		{
			knockSfx.Play();
			//print ("PLEASE OPEN THE DOOR!!");

			int res = realMap.CallForHelp(this, (DoorTile) destination); //CALL ON REAL MAP FOR PAWNS

			int totalTime = intentions.Count + res;

			falseMap.GetTiles()[destination.X, destination.Y].Walkable = false;
			ArrayList detour = falseMap.AStar(this.X, this.Y, goal.X, goal.Y);

			ButtonTile buttonD = ((DoorTile) destination).button;

			ArrayList detour2 = falseMap.AStar(this.X, this.Y, buttonD.X, buttonD.Y);
			falseMap.GetTiles()[destination.X, destination.Y].Walkable = true;
			ArrayList detour2Bis = falseMap.AStar(buttonD.X, buttonD.Y, destination.X, destination.Y);

			int TimeForDetour = 10000;
			int TimeForDetour2 = 10001;

			if (detour != null)
				TimeForDetour = detour.Count - 1;

			if (detour2 != null)
				TimeForDetour2 = detour2.Count - 1 + detour2Bis.Count - 1 + intentions.Count - 1;


			if (detour == null && detour2 == null && res == -1)
			{
				print("I CANNOT MOVE!!");
			}
			else if (res == -1 || totalTime > TimeForDetour || totalTime > TimeForDetour2) //I WILL NOT WAIT
			{
				falseMap.GetTiles()[destination.X, destination.Y].Walkable = false;
				realMap.NotNeedHelpAnyMore(this, res);

				if (res != -1)
				{
					if ((totalTime > TimeForDetour2 && TimeForDetour2 > TimeForDetour) || (TimeForDetour2 > totalTime && totalTime > TimeForDetour))
					{
						CheckPath();
						//print("FUCK THIS SHIT, I'M GOING BY MYSELF in " + intentions.Count + " STEPS INSTEAD OF " + totalTime);
					}
					else if ((totalTime > TimeForDetour && TimeForDetour > TimeForDetour2) || (TimeForDetour > totalTime && totalTime > TimeForDetour2))
					{
						realMap.PawnHelpingHimself(this);
						SetGoal(((DoorTile) destination).button);
						//print("I'LL FIND THIS BUTTON MYSELF!!!");
					}
				}
				else
				{
					if (TimeForDetour2 > TimeForDetour)
					{
						CheckPath();
						//print("FUCK THIS SHIT, I'M GOING BY MYSELF in " + intentions.Count + " STEPS INSTEAD OF " + totalTime);
					}
					else
					{
						realMap.PawnHelpingHimself(this);
						SetGoal(((DoorTile) destination).button);
						//print("I'LL FIND THIS BUTTON MYSELF!!!");
					}
				}

			}
			else
			{
				//print("OK, I'LL WAIT!!");
				waiting = true;
			}
		}

	}

	public void SetPosition(uint x, uint y)
	{
		X = x;
		Y = y;
	}

	void Update ()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity) && col == hitInfo.collider)
		{
			// The mouse is over the tile
			rend.material.color = highlight;
			
			if (Input.GetMouseButtonDown(0))
			{
				realMap.OnPawnClicked(this);
			}
		}
		else
		{
			rend.material.color = baseColor;
		}

		// Moving the visual to its actual position
		if (X != transform.position.x || Y != transform.position.z)
		{
			transform.position = Vector3.Lerp(transform.position, new Vector3(X, 0, Y), movingSpeed * Time.deltaTime);
		}
	}

	public void CheckPath()
	{
		ArrayList path;
		if (realMap.IsCoopActivated())
			path = falseMap.AStar(X, Y, goal.X, goal.Y);
		else
			path = realMap.AStar(X, Y, goal.X, goal.Y);

		intentions = new List<Tile>();
		
		if (path == null)
			return;
		
		path.RemoveAt(0);

		if (realMap.IsCoopActivated ())
		{
			foreach (FalseTile tile in path) {
				intentions.Add (realMap.GetTiles () [tile.X, tile.Y]);
			}
		}
		else
		{
			foreach (Tile tile in path) {
				intentions.Add (tile);
			}
		}
			
	}

	public bool HasArrived()
	{
		if (goal == null)
			return true;

		return X == goal.X && Y == goal.Y;
	}

	public void SetGoal(Tile g)
	{
		goal = g;
		CheckPath ();

	}

	public Tile GetGoal()
	{
		return goal;
	}

	public Tile GetNext()
	{
		if (intentions.Count == 0)
			return null;
		return intentions [0];
	}

	public void HelpingDone()
	{
		helping = false;
	}

	public void NowHelping()
	{
		helping = true;
	}

	public void WaitingDone()
	{
		waiting = false;
	}

	public FalseMap GetFalseMap()
	{
		return falseMap;
	}

	public void UpdateFalseMap(Map map)
	{
		falseMap.updateMap (map);
	}
}
