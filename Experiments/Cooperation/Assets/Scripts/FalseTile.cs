﻿using UnityEngine;
using System.Collections;

public class FalseTile
{
	public uint X;
	public uint Y;

	public ESelection type;
	public bool Walkable; // Can the player walk on this tile?


	public FalseTile(uint x, uint y, ESelection ty, bool w)
	{
		X = x;
		Y = y;
		type = ty;
		Walkable = w;
	}
}
