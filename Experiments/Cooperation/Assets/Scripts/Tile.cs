﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
	public Color highlight;
	public uint X;
	public uint Y;

	public ESelection type;
	public bool Walkable; // Can the player walk on this tile?

	private Map map;
	private Renderer[] rend;
	private Collider col;

	protected Color[] baseColor;

	public void Awake ()
	{
		rend = GetComponentsInChildren<Renderer>();
		col = GetComponentInChildren<Collider>();

		baseColor = new Color[rend.Length];
		for (int i = 0; i < rend.Length; i++)
		{
			baseColor[i] = rend[i].material.color;
		}
	}

	public void SetMap(Map m)
	{
		map = m;
	}

	public void SetBaseColor(Color c)
	{
		for (int i = 0; i < baseColor.Length; i++)
		{
			baseColor[i] = c;
		}
	}
	
	void Update ()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity) && col == hitInfo.collider)
		{
			// The mouse is over the tile
			foreach (Renderer r in rend)
			{
				r.material.color = highlight;
			}

			if (Input.GetMouseButtonDown(0))
			{
				map.OnTileClicked(this);
			}
		}
		else
		{
			for (int i = 0; i < rend.Length; i++)
			{
				rend[i].material.color = baseColor[i];
			}
		}
	}
}
