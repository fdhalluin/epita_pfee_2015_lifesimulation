﻿using UnityEngine;
using System.Collections;

public class ButtonTile : Tile
{
	public DoorTile door;
	public Color color;

	private LineRenderer line;

	void Awake()
	{
		base.Awake();
		line = GetComponent<LineRenderer>();
		line.SetPosition(0, new Vector3(transform.position.x, 0.2f, transform.position.z));
		line.SetPosition(1, new Vector3(transform.position.x, 0.2f, transform.position.z));
		line.enabled = false;
	}

	void OnDestroy()
	{
		if (door != null)
			door.SetButton(null);
	}

	public void SetColor(Color col)
	{
		color = col;
		
		baseColor[0] = col;
		baseColor[1] = col;

		//GetComponentInChildren<MeshRenderer>().material.color = color;
		line.SetColors(col, col);
		line.material.color = col;
	}

	public void LinkToDoor(DoorTile d)
	{
		d.SetButton(this);
		door = d;

		Vector3 doorPosition = d.transform.position;

		line.SetPosition(1, new Vector3(doorPosition.x, 0.2f, doorPosition.z));

		door.SetBaseColor(color);
	}

	public void SetLineDisplay(bool b)
	{
		line.enabled = b;
	}

	public void Disconect()
	{
		door = null;
		line.SetPosition(1, new Vector3(transform.position.x, 0.2f, transform.position.z));
	}

	public void Press()
	{
		if (door != null)
		{
			door.Open();
		}
	}
}
