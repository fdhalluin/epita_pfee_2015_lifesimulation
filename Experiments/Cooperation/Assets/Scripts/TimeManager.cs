﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Singleton for managing each step
public class TimeManager : MonoBehaviour 
{
	// Singleton
	private static TimeManager _instance;
	public static TimeManager instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<TimeManager>();
			return _instance;
		}
	}

	public Text stepText;
	public Map map;
	public LaunchButton launchButton;

	private AudioSource goalSfx;
	private bool isRunning = false;

	private uint stepNumber = 0;
	public uint GetStepNumber()
	{
		return stepNumber;
	}

	void Start()
	{
		goalSfx = GetComponent<AudioSource>();
	}

	void FixedUpdate()
	{
		stepText.text= "Step : " + stepNumber;
	}
	
	// Simulate one step
	public void Step()
	{
		if (! map.EveryoneHasArrived())
		{
			map.Step();
			stepNumber++;
		}
		else
		{
			// Every pawn has arrived to its goal
			if (isRunning)
			{
				launchButton.OnClick();
				goalSfx.Play();
			}
		}
	}

	public void ToggleLaunch()
	{
		isRunning = ! isRunning;

		if (isRunning)
			InvokeRepeating("Step", 0.5f, 0.5f);
		else
			CancelInvoke("Step");
	}

	public void ResetTime()
	{
		stepNumber = 0;
	}
}
