﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;


public enum ESelection
{
	Empty, // 0
	Wall,  // 1
	Goal,  // 2
	Pawn,  // 3
	Button,// 4
	Door,  // 5
	Connect// 6
}

public class Map : MonoBehaviour
{
	public GameObject emptyTile;
	public GameObject wallTile;
	public GameObject goalTile;
	public GameObject doorTile;
	public GameObject buttonTile;
	public GameObject pawnGO;
	public GameObject tilesContainer;
	public GameObject interfacePanel;

	private uint mapSize = 10;
	private Tile[,] tiles;
	private List<Pawn> pawns;
	private List<Pawn> pawnsInNeeds;
	private List<Pawn> pawnsCurrentlyHelping;
	private bool coopActivated;

	private List<ButtonTile> buttons;
	private ESelection selectedType; // Selected in the interface

	private ButtonTile connectFrom = null;

	private uint selectedSize = 10;
	private Tile goal = null;

	void Start ()
	{
		//print("START OF REAL MAP");
		pawns = new List<Pawn> ();
		pawnsInNeeds = new List<Pawn> ();
		pawnsCurrentlyHelping = new List<Pawn> ();
		coopActivated = true;

		buttons = new List<ButtonTile> ();

		GenerateMap (mapSize);

		MakeBasicMap ();
		selectedType = ESelection.Empty;
	}

	void Update ()
	{
	
	}
	
	private void InstantiateTile(GameObject prefab, uint x, uint y)
	{
		foreach(Pawn p in pawns)
		{
			if (p.X == x && p.Y == y)
			{
				pawns.Remove(p);
				Destroy(p.gameObject);
				break;
			}
		}

		Tile newTile = ((GameObject)Instantiate(prefab, new Vector3(x, 0, y), Quaternion.identity)).GetComponent<Tile>();
		newTile.X = x;
		newTile.Y = y;
		newTile.transform.parent = tilesContainer.transform;
		newTile.SetMap(this);

		if (prefab == buttonTile)
		{
			Color buttonColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 0.5f);
			((ButtonTile)newTile).SetColor(buttonColor);
			buttons.Add((ButtonTile)newTile);
		}

		tiles[x,y] = newTile;
	
	}

	private void InstantiatePawn(uint x, uint y)
	{
		// Only one pawn per tile
		foreach(Pawn p in pawns)
		{
			if (p.X == x && p.Y == y)
			{
				pawns.Remove(p);
				Destroy(p.gameObject);
				return;
			}
		}

		Pawn newPawn = ((GameObject)Instantiate(pawnGO, new Vector3(x, 0, y), Quaternion.identity)).GetComponent<Pawn>();
		newPawn.X = x;
		newPawn.Y = y;
		newPawn.transform.parent = transform;
		newPawn.SetMap(this);
		Color pawnColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
		newPawn.GetComponentInChildren<MeshRenderer>().material.color = pawnColor;
		TrailRenderer trail = newPawn.GetComponentInChildren<TrailRenderer>();
		trail.material.color = pawnColor;
		float TrailOffsetX = Random.Range(-0.25f, 0.25f);
		float TrailOffsetY = Random.Range(-0.25f, 0.25f);
		trail.transform.localPosition = new Vector3(TrailOffsetX, 0.15f, TrailOffsetY);

		foreach (Tile t in tiles)
		{
			if (t.type == ESelection.Goal)
			{
				newPawn.SetGoal(t);
				break;
			}
		}
		
		pawns.Add(newPawn);
	}

	public void GenerateMap(uint size)
	{
		mapSize = size;
		tiles = new Tile[size,size];

		for (uint x = 0; x < size; ++x)
		{
			for (uint y = 0; y < size; ++y)
			{
				InstantiateTile(emptyTile, x, y);
			}
		}
	}

	public void RegenerateMap()
	{
		foreach (Tile t in tiles)
			Destroy(t.gameObject);

		foreach (Pawn p in pawns)
			Destroy(p.gameObject);
		pawns.Clear();
		buttons.Clear();

		GenerateMap(selectedSize);

		TimeManager.instance.ResetTime();
	}

	// From an input field (that's why we use a string)
	public void SetSelectedSize(string s)
	{
		selectedSize = uint.Parse(s);
	}

	// Execute each pawn's AI
	public void Step()
	{
		foreach (Pawn p in pawns)
		{
			p.Step();
		}
	}

	
	// returns true if every pawn has arrived to its goal
	public bool EveryoneHasArrived()
	{
		bool everyoneHasArrived = true;
		
		foreach (Pawn p in pawns)
		{
			everyoneHasArrived = everyoneHasArrived && p.HasArrived();
		}
		
		return everyoneHasArrived;
	}

	private void MakeBasicMap()
	{
		selectedType = ESelection.Wall;
		for (uint x = 1; x < 10; ++x)
			OnTileClicked(tiles[x, 4]);

		selectedType = ESelection.Goal;
		OnTileClicked(tiles[5, 9]);

		selectedType = ESelection.Empty;
		OnTileClicked(tiles[5, 4]);
		
		selectedType = ESelection.Door;
		OnTileClicked(tiles[5, 4]);

		selectedType = ESelection.Button;
		OnTileClicked(tiles[3, 6]);

		DoorTile door = (DoorTile)tiles[5, 4];
		ButtonTile button = (ButtonTile)tiles[3, 6];
		button.LinkToDoor(door);

		selectedType = ESelection.Pawn;
		OnTileClicked(tiles[5, 3]);
		OnTileClicked(tiles[3, 7]);

		selectedType = ESelection.Empty;
	}

	public void OnTileClicked(Tile tile)
	{
		if (tile.Walkable && selectedType == ESelection.Pawn)
		{
			InstantiatePawn(tile.X, tile.Y);
			return;
		}

		if (selectedType != ESelection.Empty && tile.type == selectedType)
		{
			// If the tile already contains the selected type, set it as empty
			InstantiateTile(emptyTile, tile.X, tile.Y);
		}
		else
		{
			switch(selectedType)
			{
			case ESelection.Wall:
				InstantiateTile(wallTile, tile.X, tile.Y);
				break;
			case ESelection.Goal:
				if (goal != null)
				{
					// Clear current goal
					InstantiateTile(emptyTile, goal.X, goal.Y);
					Destroy(goal.gameObject);
				}
				InstantiateTile(goalTile, tile.X, tile.Y);
				goal = tiles[tile.X, tile.Y];
				break;
			case ESelection.Door:
				InstantiateTile(doorTile, tile.X, tile.Y);
				break;
			case ESelection.Button:
				InstantiateTile(buttonTile, tile.X, tile.Y);
				break;
			case ESelection.Connect:
				if (tile is ButtonTile)
				{
					connectFrom = (ButtonTile)tile;
				}
				else if (tile is DoorTile && connectFrom != null)
				{
					connectFrom.LinkToDoor((DoorTile)tile);
				}
				UpdatePathPawns ();
				return;
			default:
				// Default on empty tile
				InstantiateTile(emptyTile, tile.X, tile.Y);
				break;
			}
		}

		// Update all the pawns' intentions when the map is modified
		UpdatePathPawns ();

		if (tile is ButtonTile)
		{
			buttons.Remove((ButtonTile)tile);
		}
		Destroy(tile.gameObject);
	}


	private void UpdatePathPawns()
	{
		pawnsInNeeds.Clear ();
		pawnsCurrentlyHelping.Clear ();

		foreach(Pawn pawn in pawns)
		{
			pawn.UpdateFalseMap(this);
			pawn.HelpingDone();
			pawn.WaitingDone();
			pawn.SetGoal(goal);
		}
	}

	public int CallForHelp(Pawn PawnInNeed, DoorTile door)
	{
		if (!pawnsInNeeds.Contains(PawnInNeed))
			pawnsInNeeds.Add (PawnInNeed);
		ButtonTile button = door.button;

		int min = (int) (mapSize * mapSize);
		Pawn Savior = null;


		foreach (Pawn pawn in pawns)
		{
			if (pawnsInNeeds.Contains(pawn) || pawnsCurrentlyHelping.Contains(pawn))
				continue;

			pawn.GetFalseMap().GetTiles()[door.X, door.Y].Walkable = false;
			ArrayList tempPath = pawn.GetFalseMap().AStar(pawn.X, pawn.Y, button.X, button.Y);
			if (tempPath == null)
				continue;

			int temp = tempPath.Count - 1;

			if (temp < min)
			{
				Savior = pawn;
				min = temp;
			}
		}

		if (Savior != null)
		{
			Savior.SetGoal(button);
			Savior.NowHelping();
			//print("OK, I WILL COME in " + min + " STEPS!!");
			pawnsCurrentlyHelping.Add(Savior);

			return min;
		}

		return -1;
	}

	public void NotNeedHelpAnyMore(Pawn p, int res)
	{
		pawnsInNeeds.Remove (p);
		
		if (pawnsCurrentlyHelping.Count == 0 || res == -1)
			return;

		Pawn Savior =  pawnsCurrentlyHelping[pawnsCurrentlyHelping.Count - 1];
		pawnsCurrentlyHelping.RemoveAt (pawnsCurrentlyHelping.Count - 1);

		Savior.SetGoal (goal);
		Savior.HelpingDone ();

		//print ("HELPING: " + pawnsCurrentlyHelping.Count);
		//print("HELP NEEDED: " + pawnsInNeeds.Count);
	}

	public void PawnHelpingHimself(Pawn p)
	{
		if (!pawnsCurrentlyHelping.Contains(p))
			pawnsCurrentlyHelping.Add (p);
		p.NowHelping ();
	}

	public bool IsForgottenPawn(Pawn p)
	{
		//print ("HELPING: " + pawnsCurrentlyHelping.Count);
		//print("HELP NEEDED: " + pawnsInNeeds.Count);

		if (pawnsInNeeds.Contains (p) && pawnsCurrentlyHelping.Count == 0)
		{
			p.WaitingDone();
			return true;
		}
		return false;
	}


	public void ButtonTyped(Pawn pawn, ButtonTile button)
	{
		if (pawnsCurrentlyHelping.Contains (pawn) && pawn.GetGoal () == button)
		{
			pawnsCurrentlyHelping.Remove (pawn);
			pawn.SetGoal(goal);
			pawn.HelpingDone();
		}

		Tile door = button.door;
		ArrayList pawnsToRemove = new ArrayList ();


		foreach (Pawn p in pawnsInNeeds)
		{
			if (p.GetNext() == door)
				pawnsToRemove.Add(p);
		}
		
		foreach (Pawn p in pawnsToRemove)
		{
			pawnsInNeeds.Remove(p);
			p.WaitingDone();
			//print("YOUHOU, I CAN MOVE");
		}

		//print ("HELPING: " + pawnsCurrentlyHelping.Count);
		//print("HELP NEEDED: " + pawnsInNeeds.Count);
	}

	private float HeuristicDistance(Tile start, Tile goal)
	{
		float X1 = start.X;
		float X2 = goal.X;

		float Y1 = start.Y;
		float Y2 = goal.Y;

		return Mathf.Sqrt (Mathf.Pow (X2 - X1, 2) + Mathf.Pow (Y2 - Y1, 2));
	}

	private float RealDistance(Tile current, Tile Next)
	{
		if (!Next.Walkable || !current.Walkable)
			return 10000;
		return 1;
	}



	public ArrayList AStar(uint StartX, uint StartY, uint GoalX, uint GoalY)
	{
		Tile start = tiles[StartX, StartY];
		Tile goal = tiles [GoalX, GoalY];

		ArrayList closedset = new ArrayList();    // The set of nodes already evaluated.
		ArrayList openset = new ArrayList();
		openset.Add(start);    // The set of tentative nodes to be evaluated, initially containing the start node

		Dictionary<Tile, Tile> came_from = new Dictionary<Tile, Tile> ();    // The map of navigated nodes.
		
		Dictionary<Tile, float> g_score = new Dictionary<Tile, float> ();
		Dictionary<Tile, float> f_score = new Dictionary<Tile, float> ();

		g_score [start] = 0;   // Cost from start along best known path.
		// Estimated total cost from start to goal through y.

		f_score[start] = g_score[start] + HeuristicDistance(start, goal);

		while (openset.Count != 0)
		{
			Tile current = (Tile) openset[0];  //the node in openset having the lowest f_score[] value
			foreach(Tile t in openset)
			{
				if (f_score[t] < f_score[current])
					current = t;
			}

			if (current == goal)
				return reconstruct_path(came_from, goal);

			openset.Remove(current);
			closedset.Add(current);

			ArrayList neighbours = new ArrayList();

			if (current.X >= 1)
				neighbours.Add(tiles[current.X - 1, current.Y]);

			if (current.Y >= 1)
				neighbours.Add(tiles[current.X, current.Y - 1]);

			if (current.Y + 1 < mapSize)
				neighbours.Add(tiles[current.X, current.Y + 1]);

			if (current.X + 1 < mapSize)
				neighbours.Add(tiles[current.X + 1, current.Y]);


			foreach(Tile neighbour in neighbours)
			{
				if (closedset.Contains(neighbour))
					continue;
				float tentative_g_score = g_score[current] + RealDistance(current, neighbour);
			
				if (!openset.Contains(neighbour) || tentative_g_score < g_score[neighbour])
				{
					came_from[neighbour] = current;
					g_score[neighbour] = tentative_g_score;
					f_score[neighbour] = g_score[neighbour] + HeuristicDistance(neighbour, goal);
					if (!openset.Contains(neighbour) && neighbour.Walkable)
						openset.Add(neighbour);
				}
			}
		}

		return null;
	}

	private ArrayList reconstruct_path(Dictionary<Tile, Tile> came_from, Tile current)
	{
		ArrayList total_path = new ArrayList();
		total_path.Add(current);
		while (came_from.ContainsKey(current))
		{
			current = came_from[current];
			total_path.Add(current);
		}
		total_path.Reverse();
		return total_path;
	}

	public bool IsTileWalkable(uint x, uint y)
	{
		return tiles [x, y].Walkable;
	}

	public void OnPawnClicked(Pawn tile)
	{
		if (selectedType == ESelection.Pawn)
		{
			InstantiatePawn(tile.X, tile.Y);
			return;
		}
	}

	public void SetConnectMode(bool b)
	{
		foreach (ButtonTile button in buttons)
		{
			connectFrom = null;
			button.SetLineDisplay(b);
		}
	}

	// Interface callbacks
	public void SetSelectedType(int type)
	{
		ESelection selection = (ESelection)type;

		if (selectedType == selection)
		{
			selectedType = ESelection.Empty;
			SetConnectMode(false);
		}
		else
		{
			SetConnectMode(selection == ESelection.Connect);

			foreach (InterfaceButton b in interfacePanel.GetComponentsInChildren<InterfaceButton>())
			{
				if (b.Selected())
				{
					b.OnClick();
				}
			}

			selectedType = selection;
		}
	}

	public Tile[,] GetTiles()
	{
		return tiles;
	}

	public uint getMapSize()
	{
		return mapSize;
	}

	public bool IsCoopActivated()
	{
		return coopActivated;
	}

	public void ActivateCoop()
	{
		coopActivated = !coopActivated;
		UpdatePathPawns ();
	}
}
