﻿using UnityEngine;
using System.Collections;

public class DoorTile : Tile
{
	public ButtonTile button;
	public GameObject cubeVisual;

	private AudioSource sfx;

	void Start()
	{
		sfx = GetComponent<AudioSource>();
	}

	void OnDestroy()
	{
		if (button != null)
			button.Disconect();
	}

	public void SetButton(ButtonTile b)
	{
		if (button != null)
			button.Disconect();

		button = b;
	}

	public void Open()
	{
		if (Walkable == true)
			return;

		Walkable = true;

		if (cubeVisual != null)
			cubeVisual.SetActive(false);

		sfx.Play();
	}
}
