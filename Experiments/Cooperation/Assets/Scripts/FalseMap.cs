using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class FalseMap
{

	private uint mapSize = 10;
	private FalseTile[,] tiles;


	public FalseMap (Tile[,] otiles, uint size)
	{
		mapSize = size;

		tiles = new FalseTile[mapSize, mapSize];

		foreach (Tile t in otiles) {
			tiles[t.X, t.Y] = new FalseTile(t.X, t.Y, t.type, t.Walkable);
			if (t.type == ESelection.Door && ((DoorTile)t).button != null)
				tiles[t.X, t.Y].Walkable = true;
		}
	}


	private float HeuristicDistance(FalseTile start, FalseTile goal)
	{
		float X1 = start.X;
		float X2 = goal.X;

		float Y1 = start.Y;
		float Y2 = goal.Y;

		return Mathf.Sqrt (Mathf.Pow (X2 - X1, 2) + Mathf.Pow (Y2 - Y1, 2));
	}

	private float RealDistance(FalseTile current, FalseTile Next)
	{

		if (!Next.Walkable || !current.Walkable)
			return 10000;
		return 1;
	}



	public ArrayList AStar(uint StartX, uint StartY, uint GoalX, uint GoalY)
	{
		FalseTile start = tiles[StartX, StartY];
		FalseTile goal = tiles [GoalX, GoalY];

		ArrayList closedset = new ArrayList();    // The set of nodes already evaluated.
		ArrayList openset = new ArrayList();
		openset.Add(start);    // The set of tentative nodes to be evaluated, initially containing the start node

		Dictionary<FalseTile, FalseTile> came_from = new Dictionary<FalseTile, FalseTile> ();    // The map of navigated nodes.
		
		Dictionary<FalseTile, float> g_score = new Dictionary<FalseTile, float> ();
		Dictionary<FalseTile, float> f_score = new Dictionary<FalseTile, float> ();

		g_score [start] = 0;   // Cost from start along best known path.
		// Estimated total cost from start to goal through y.

		f_score[start] = g_score[start] + HeuristicDistance(start, goal);

		while (openset.Count != 0)
		{
			FalseTile current = (FalseTile) openset[0];  //the node in openset having the lowest f_score[] value
			foreach(FalseTile t in openset)
			{
				if (f_score[t] < f_score[current])
					current = t;
			}

			if (current == goal)
				return reconstruct_path(came_from, goal);

			openset.Remove(current);
			closedset.Add(current);

			ArrayList neighbours = new ArrayList();

			if (current.X >= 1)
				neighbours.Add(tiles[current.X - 1, current.Y]);

			if (current.Y >= 1)
				neighbours.Add(tiles[current.X, current.Y - 1]);

			if (current.Y + 1 < mapSize)
				neighbours.Add(tiles[current.X, current.Y + 1]);

			if (current.X + 1 < mapSize)
				neighbours.Add(tiles[current.X + 1, current.Y]);


			foreach(FalseTile neighbour in neighbours)
			{
				if (closedset.Contains(neighbour))
					continue;
				float tentative_g_score = g_score[current] + RealDistance(current, neighbour);
			
				if (!openset.Contains(neighbour) || tentative_g_score < g_score[neighbour])
				{
					came_from[neighbour] = current;
					g_score[neighbour] = tentative_g_score;
					f_score[neighbour] = g_score[neighbour] + HeuristicDistance(neighbour, goal);
					if (!openset.Contains(neighbour) && neighbour.Walkable)
						openset.Add(neighbour);
				}
			}
		}

		return null;
	}

	private ArrayList reconstruct_path(Dictionary<FalseTile, FalseTile> came_from, FalseTile current)
	{
		ArrayList total_path = new ArrayList();
		total_path.Add(current);
		while (came_from.ContainsKey(current))
		{
			current = came_from[current];
			total_path.Add(current);
		}
		total_path.Reverse();
		return total_path;
	}


	public void ButtonTyped(ButtonTile button)
	{
		Tile door = button.door;
		tiles [door.X, door.Y].Walkable = true;
	}

	public void updateMap(Map map)
	{
		foreach (Tile t in map.GetTiles()) {
			tiles[t.X, t.Y] = new FalseTile(t.X, t.Y, t.type, t.Walkable);
			if (t.type == ESelection.Door && ((DoorTile)t).button != null)
				tiles[t.X, t.Y].Walkable = true;
		}
	}

	public bool IsTileWalkable(uint x, uint y)
	{
		return tiles [x, y].Walkable;
	}

	public FalseTile[,] GetTiles()
	{
		return tiles;
	}
}
