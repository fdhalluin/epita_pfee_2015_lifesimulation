﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LaunchButton : MonoBehaviour
{
	private bool launched = false;
	private Button button;
	private Text text;

	void Start()
	{
		button = GetComponent<Button>();
		text = GetComponentInChildren<Text>();
	}

	public void OnClick()
	{
		launched = !launched;

		if (launched)
		{
			text.text = "PAUSE";
			ColorBlock colors = new ColorBlock();
			colors.normalColor = new Color(0.8f, 0, 0);
			colors.highlightedColor = new Color(0.8f, 0, 0);
			colors.pressedColor = new Color(0.5f, 0, 0);
			colors.colorMultiplier = 1.0f;
			button.colors = colors;
		}
		else
		{
			text.text = "LAUNCH";
			ColorBlock colors = new ColorBlock();
			colors.normalColor = new Color(0, 0.8f, 0);
			colors.highlightedColor = new Color(0, 0.8f, 0);
			colors.pressedColor = new Color(0, 0.5f, 0);
			colors.colorMultiplier = 1.0f;
			button.colors = colors;
		}

		TimeManager.instance.ToggleLaunch();
	}
}
