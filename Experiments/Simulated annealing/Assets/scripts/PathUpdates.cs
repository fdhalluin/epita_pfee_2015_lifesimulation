﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathUpdates : MonoBehaviour {

    public GameObject cityPrefab;
    public GameObject edgePrefab;
    public GameObject cityContainer;
    public GameObject edgeContainer;
    public int numberOfCities;
    public double temperature;
    public double cooldownRate;
    public double cooldownK;

    public double distance;

    private List<GameObject> parcours = new List<GameObject>();
    private List<GameObject> edges = new List<GameObject>();

    void Start()
    {
        for (int i = 0; i < numberOfCities; ++i)
        {
            // A city
            GameObject copy = Instantiate(cityPrefab);
            copy.transform.parent = cityContainer.transform;
            copy.name = "City of " + i;
            copy.transform.position = new Vector3(Random.Range(-4, 4), Random.Range(-4, 4), cityPrefab.transform.position.z);
            parcours.Add(copy);

            // An edge
            copy = Instantiate(edgePrefab);
            copy.SetActive(false);
            copy.transform.parent = edgeContainer.transform;
            edges.Add(copy);
        }

        //moveEdge(parcours[9], parcours[7], edges[0]);

        updateEdges();
        activateEdges();
    }

	// Update is called once per frame
	void Update ()
    {
        if (temperature > -100f)
        {
            iterate();
            updateEdges();
            distance = computeLength();
            cool();
        }
	}

    private void activateEdges()
    {
        foreach (GameObject o in edges)
            o.SetActive(true);
    }

    private void updateEdges()
    {
        for (int i = 0; i < parcours.Count; ++i)
            moveEdge(parcours[i], parcours[(i + 1) % parcours.Count], edges[i]);
    }

    private void moveEdge(GameObject acity, GameObject bcity, GameObject edge)
    {
        // Safety swap
        //if (acity.transform.position.x < bcity.transform.position.x)
        //{
        //    GameObject tmp = acity;
        //    acity = bcity;
        //    bcity = tmp;
        //}


        //float xdelta = acity.transform.position.x - bcity.transform.position.x;
        //float ydelta = acity.transform.position.y - bcity.transform.position.y;

        float xdelta = bcity.transform.position.x - acity.transform.position.x;
        float ydelta = bcity.transform.position.y - acity.transform.position.y;
        float dist = Mathf.Sqrt(xdelta * xdelta + ydelta * ydelta);

        //float angle = Mathf.Acos(ydelta / dist) * 57.2957795f;
        float angle = Mathf.Atan2(ydelta, xdelta) * 57.2957795f + 90f;
        edge.transform.localEulerAngles = new Vector3(0, 0, angle);
        edge.transform.localScale = new Vector3(edge.transform.localScale.x, dist, edge.transform.localScale.z);
        edge.transform.position = new Vector3(
                                                (acity.transform.position.x + bcity.transform.position.x) / 2,
                                                (acity.transform.position.y + bcity.transform.position.y) / 2,
                                                 edge.transform.position.z
                                             );
    }

    // Update temperature
    private void cool()
    {
        temperature = temperature * cooldownRate - cooldownK;
    }

    // Exchange two cities if algorithm deems it necessary
    // return index of exchange or null if nothing happened
    private Pair<int, int> iterate()
    {
        if (parcours.Count <= 1)
            return null;

        /* Select two cities to exchange */
        int aindex = UnityEngine.Random.Range(0, parcours.Count - 1);
        int bindex = UnityEngine.Random.Range(0, parcours.Count - 1);
        while (aindex == bindex)
            bindex = UnityEngine.Random.Range(0, parcours.Count - 1);

        /* Compute ancient length to target neighbors */
        GameObject aleft = parcours[(aindex + parcours.Count - 1) % parcours.Count];
        GameObject aright = parcours[(aindex + 1) % parcours.Count];
        GameObject bleft = parcours[(bindex + parcours.Count - 1) % parcours.Count];
        GameObject bright = parcours[(bindex + 1) % parcours.Count];

        double distaOld = distancePoints(parcours[aindex], aleft) +
                          distancePoints(parcours[aindex], aright);
        double distbOld = distancePoints(parcours[bindex], bleft) +
                          distancePoints(parcours[bindex], bright);

        /* Compute new length to target neighbors */
        double distaNew = distancePoints(parcours[aindex], bleft) +
                          distancePoints(parcours[aindex], bright);
        double distbNew = distancePoints(parcours[bindex], aleft) +
                          distancePoints(parcours[bindex], aright);

        /* Compute delta */
        double delta = distaNew + distbNew - distaOld - distbOld;

        /* Decide if it's enough for a swap, then swap */
        if (delta < (temperature / 1000))
        {
            GameObject tmp = parcours[aindex];
            parcours[aindex] = parcours[bindex];
            parcours[bindex] = tmp;

            return new Pair<int,int>(aindex, bindex);
        }
        else
        {
            // no swap
            return null;
        }
    }

    private double computeLength()
    {
        double d = 0;

        for (int i = 1; i < parcours.Count; ++i)
            d += distancePoints(parcours[i], parcours[i - 1]);

        if (parcours.Count > 1)
            d += distancePoints(parcours[0], parcours[parcours.Count - 1]);

        return d;
    }

    private float distancePoints(GameObject a, GameObject b)
    {
        float xd = a.transform.position.x - b.transform.position.x;
        float yd = a.transform.position.y - b.transform.position.y;
        return xd * xd + yd * yd;
    }
}
