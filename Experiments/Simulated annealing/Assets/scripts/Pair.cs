﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

 class Pair<T, U>
 {
     public T left;
     public U right;

     public Pair(T left, U right)
     {
         this.left = left;
         this.right = right;
     }
 }
